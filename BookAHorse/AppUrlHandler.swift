//
//  AppUrlHandler.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/24/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AppUrlHandler: HKBaseUrlHandler {
    
    override func handleAsset(_ id: String, query: [URLQueryItem]?) -> Bool {
        if let rootViewController = (UIApplication.shared.delegate as? HKBaseAppDelegate)?.window?.rootViewController {
            if let controller = AppController.getViewControllerById(AppController.Asset, viewController: rootViewController) as? HKAssetViewProtocol {
                
                controller.loadAsset(id)
                
                let navi = UINavigationController(rootViewController: controller as! UIViewController)
                
                rootViewController.present(navi, animated: true, completion: nil)
                
                return true
            }
        }
        return false
    }

}
