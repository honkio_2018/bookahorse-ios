//
//  HorsesMapViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import MapKit
import HonkioAppTemplate

class HorsesMapViewController: HKBaseAssetsMapViewController {
    
    @IBOutlet weak var appleMapsView: MKMapView!

    private var bottomSheetViewController: BottomSheetViewController?
    
    override func buildMapViewProtocol() -> HKMapViewProtocol {
        let mapView = HKAppleMapsView(self.appleMapsView)
        if let location = HonkioApi.deviceLocation() {
            mapView.showsUserLocation = true
            mapView.moveToLocation(location.coordinate, animated: false)
        }
        return mapView
    }
    
    @IBAction func currentLocationAction(_ sender: AnyObject) {
        moveToUserLocation(animated: true)
    }
    
    override open func annotationDidSelect(_ annotation: HKMapViewAnnotation?) {
        if annotation != nil {
            if let asset = annotation?.item as? HKAsset {
                addBottomSheetView(asset)
            }
        }
        else {
            if let bottomSheetVC = self.bottomSheetViewController, !self.childViewControllers.isEmpty {
                bottomSheetVC.willMove(toParentViewController: nil)
                bottomSheetVC.view.removeFromSuperview()
                bottomSheetVC.removeFromParentViewController()
                bottomSheetViewController = nil
            }
        }
    }

    func addBottomSheetView(_ asset: HKAsset) {
        bottomSheetViewController = BottomSheetViewController()
        if let bottomSheetVC = bottomSheetViewController {
            bottomSheetVC.assets = asset
            
            self.addChildViewController(bottomSheetVC)
            self.view.addSubview(bottomSheetVC.view)
            bottomSheetVC.didMove(toParentViewController: self)
            
            let height = view.frame.height
            let width  = view.frame.width
            bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        }
    }
}
