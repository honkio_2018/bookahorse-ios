//
//  MapViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/27/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import MapKit
import HonkioAppTemplate


class MapViewController: HKBaseMapViewController {
    
    @IBOutlet weak var searchPanel: UIView!
    @IBOutlet weak var searchProgress: UIView!
    @IBOutlet weak var searchText: AutoCompleteTextField!
    @IBOutlet weak var currentLocationButton: UIView!
    @IBOutlet weak var markerLocationButton: UIView!
    @IBOutlet weak var appleMapsView: MKMapView!
    
    fileprivate var dataTask: URLSessionDataTask?
    fileprivate var delayedText: String?
    
    var location: CLLocation?
    
    var readOnly: Bool = true
    var descMessage : String?
    var userCountry: String = "FI"
    
    var completeAction: ((_ viewController: MapViewController, _ place: HKAddress?, _ location: CLLocation?) -> Void)?
    
    fileprivate var doneButton: UIBarButtonItem?
    
    fileprivate var isViewSetup = false
    fileprivate var isUserLocationShown = false
    fileprivate let geocoder: GeocoderProtocol = GoogleGeocoder()
    
    override func buildMapViewProtocol() -> HKMapViewProtocol {
        return HKAppleMapsView(self.appleMapsView)
    }
    
    @IBAction func mapPinLocationAction(_ sender: AnyObject) {
        centerToMapPinLocation()
    }
    
    @IBAction func currentLocationAction(_ sender: AnyObject) {
        moveToUserLocation(animated: true)
    }
    
    @IBAction func findLocationAction(_ sender: AnyObject) {
        self.geocoder.cancel()
        self.searchProgress.isHidden = false
        self.geocoder.fromLocationName(self.searchText.text ?? "") {[unowned self] (place, location) in
            self.removeAllAnnotations()
            self.searchProgress.isHidden = true
            
            if place != nil{
                self.location = location
                
                let annotation = HKMapViewAnnotation()
                annotation.coordinate = self.location!.coordinate
                annotation.title = place?.address1
                self.addAnnotation(annotation)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let borderColor = UIColor(netHex: AppColors.appOrange).cgColor
        self.currentLocationButton.layer.borderColor = borderColor
        self.currentLocationButton.layer.borderWidth = 1
        self.markerLocationButton.layer.borderColor = borderColor
        self.markerLocationButton.layer.borderWidth = 1
        
        self.searchText.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        self.searchText.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)!
        self.searchText.autoCompleteCellHeight = 35.0
        self.searchText.maximumAutoCompleteCount = 20
        self.searchText.hidesWhenSelected = true
        self.searchText.hidesWhenEmpty = true
        self.searchText.enableAttributedText = true
        self.searchText.autoCompleteAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.black,
            NSAttributedStringKey.font : UIFont(name: "HelveticaNeue-Bold", size: 12.0)!
        ]
        
        self.searchText.onTextChange = {[weak self] text in
            if !text.isEmpty{
                if self?.dataTask != nil {
                    self?.delayedText = text
                }
                else {
                    self?.fetchAutocompletePlaces(text)
                }
            }
        }
        
        self.searchText.onSelect = {_,_ in
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isViewSetup {
            
            if self == self.navigationController?.viewControllers[0] {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(MapViewController.cancelAction))
            }
            
            self.searchProgress.isHidden = true
            
            if !readOnly {
                
                self.showsUserLocation = true
                self.currentLocationButton.isHidden = false
                self.searchPanel.isHidden = false
                self.doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(MapViewController.doneAction))
                
                self.searchText.delegate = self
            }
            else {
                self.currentLocationButton.isHidden = true
                self.searchPanel.isHidden = true
            }
            
            if location != nil {
                let annotation = HKMapViewAnnotation()
                annotation.coordinate = location!.coordinate
                self.addAnnotation(annotation)
            }
            else if !self.isUserLocationShown {
                self.moveToUserLocation(animated: false)
            }
            
            isViewSetup = true
        }
    }
    
    func centerToUserLocationOnce() {
        if !isUserLocationShown {
            if location == nil {
                super.moveToUserLocation(animated: true)
            }
            isUserLocationShown = true
        }
    }
    
    override func addAnnotation(_ annotation: HKMapViewAnnotation) {
        moveToLocation(annotation.coordinate, animated: true)
        
        super.addAnnotation(annotation)
        
        // Show Done button
        self.navigationItem.rightBarButtonItem = self.doneButton
    }
    
    func centerToMapPinLocation() {
        if self.location != nil {
            moveToLocation(self.location!.coordinate, animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.findLocationAction(textField)
        return true
    }
    
    @IBAction func doneAction() {
        if self.location != nil {
            
            BusyIndicator.shared.showProgressView(self.view)
            
            self.geocoder.fromLocation(self.location!, callback: {[unowned self] (place, location) in
                
                if self.completeAction != nil {
                    self.completeAction?(self, place, self.location)
                }
                else {
                    self.cancelAction()
                }
                
                BusyIndicator.shared.hideProgressView()
            })
        }
        else {
            if self.descMessage != nil {
                CustomAlertView(okTitle: nil, okMessage: self.descMessage!, okAction: nil).show()
            }
        }
    }
    
    @IBAction func cancelAction() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func mapDidTap(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        if !readOnly {
            self.removeAllAnnotations()
            
            self.location = CLLocation(latitude: latitude, longitude: longitude)
            
            self.geocoder.fromLocation(self.location!) {[unowned self] (address, location) in
                self.searchText.text = (address?.postalCode ?? "") + " " + (address?.address1 ?? "")
            }
            
            let annotation = HKMapViewAnnotation()
            annotation.coordinate = self.location!.coordinate
            self.addAnnotation(annotation)
        }
    }
    
    fileprivate func fetchAutocompletePlaces(_ keyword:String?) {
        if keyword != nil && (keyword?.count ?? 0) >= 2 {
            // TODO fetch results in prefered language
            let googleMapsKey = "AIzaSyAqNq30DT13uPY50dS77tq7oLAMrPNK9I4"
            let location = String(format: "%.6f,%.6f", self.appleMapsView.camera.centerCoordinate.latitude, self.appleMapsView.camera.centerCoordinate.longitude)
            let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?" +
            "types=geocode&sensor=false&key=\(googleMapsKey)&input=\(keyword!)&location=\(location)&radius=50000&&components=country:\(userCountry)"
            let s = (CharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
            s.addCharacters(in: "+&")
            
            guard let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: s as CharacterSet) else {
                return
            }
            
            guard let url = URL(string: encodedString) else {
                return
            }
            
            let request = URLRequest(url: url)
            self.dataTask = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                if let data = data {
                    
                    do{
                        let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                        
                        var locations: [String]? = nil
                        
                        if result?["status"] as? String  == "OK" {
                            if let predictions = result?["predictions"] as? NSArray{
                                locations = [String]()
                                for dict in predictions as! [NSDictionary]{
                                    locations?.append(dict["description"] as! String)
                                }
                            }
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.searchText.autoCompleteStrings = locations
                        })
                    }
                    catch let error {
                        print("Error: \(error.localizedDescription)")
                    }
                    if self.delayedText != nil {
                        self.fetchAutocompletePlaces(self.delayedText)
                        self.delayedText = nil
                    }
                }
                self.dataTask = nil
            })
            self.dataTask?.resume()
        }
    }
}
