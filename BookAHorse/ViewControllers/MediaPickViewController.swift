//
//  MediaPickViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MediaPickViewController: HKBaseMediaPickViewController {
    
    @IBOutlet weak var labelReason: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
