//
//  MainViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/24/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MainViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if HonkioApi.isConnected() {
            var theme = AppSettings.theme
            
            // Theme correction
            switch (theme) {
            case ROLE_RIDER:
                if !AppApi.isRegisteredAsRider() {
                    theme = AppApi.isRegisteredAsOwner() ? ROLE_OWNER : ROLE_NONE
                }
                break
            case ROLE_OWNER:
                if !AppApi.isRegisteredAsOwner() {
                    theme = AppApi.isRegisteredAsRider() ? ROLE_RIDER : ROLE_NONE
                }
                break
            default:
                theme = AppApi.isRegisteredAsRider() ? ROLE_RIDER : (AppApi.isRegisteredAsOwner() ? ROLE_OWNER : ROLE_NONE)
                break
            }
            
            AppSettings.theme = theme
            
            switch (theme) {
            case ROLE_RIDER:
//                AppColors.applyRiderTheme()
                self.viewControllers = [
                    AppController.instance.getViewControllerByIdentifier("MainRiderViewController", viewController: self)!
                ]
                break
            case ROLE_OWNER:
//                AppColors.applyOwnerTheme()
                self.viewControllers = [
                    AppController.instance.getViewControllerByIdentifier("MainOwnerViewController", viewController: self)!
                ]
                break
            default:
//                AppColors.applyCommonTheme()
                self.viewControllers = [
                    AppController.instance.getViewControllerByIdentifier("RoleRegistrationViewController", viewController: self)!
                ]
                break
            }
        }
        else {
//            AppColors.applyCommonTheme()
            self.viewControllers = [
                AppController.instance.getViewControllerByIdentifier("MainGuestViewController", viewController: self)!
            ]
        }
    }
}
