//
//  HorseEventsListFragment.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseEventsListViewController: HKBaseSplitScheduleTableViewController {
    
    @IBOutlet weak var labelEmpty: UILabel!
    
    static let TIME_FORMAT = "HH:mm"
    
    public var asset: HKAsset! {
        didSet {
            self.filter.objectId = self.asset?.assetId
        }
    }
    
    public var products: [BookedProduct]! {
        didSet {
            
            self.filter.products = self.products
            
            var duration = 0
            for book in self.products {
                duration += book.product.duration * Int(book.count)
            }
            
            self.filter.step = duration
        }
    }
    
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        dateFormatter.setLocalizedDateFormatFromTemplate(HorseEventsListViewController.TIME_FORMAT)
        
        var dateComp = DateComponents()
        dateComp.month = 3
        
        self.filter.startDate = Date()
        self.filter.endDate = Calendar.current.date(byAdding: dateComp, to: self.filter.startDate)!
        self.filter.round = SPLIT_SCHEDULE_ROUND_MIN
        self.filter.step = 1800000 // (30 minutes)
        self.filter.objectType = HK_OBJECT_TYPE_ASSET
        
        // add right button on navigation bar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(HorseEventsListViewController.actionMenu))
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items = [HKTableViewItem]()
        let itemBinder = buildCellBunder()
        let headerBinder = buildHeaderBunder()
        
        let newGroupDateFormatter = DateFormatter()
        newGroupDateFormatter.dateStyle = .short
        newGroupDateFormatter.timeStyle = .none
        newGroupDateFormatter.setLocalizedDateFormatFromTemplate("MMMMd")
        
        var group: String! = nil
        if list.count > 0 {
            group = newGroupDateFormatter.string(from: ((list[0] as? Event)?.start)!)
            items.append(("EventsSectionTitleCell", group, headerBinder))
        }
        
        for listItem in list as! [Event?] {
            if let item = listItem {
                let newGroupDate = newGroupDateFormatter.string(from: item.start)
                if !newGroupDate.elementsEqual(group)  {
                    items.append(("EventsSectionTitleCell", newGroupDate, headerBinder))
                    group = newGroupDate
                }
                items.append(("EventItemCell", listItem, itemBinder))
            }
        }
        return items
    }
    
    override open func buildCellBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { [unowned self] (viewController, cell, item) in
            
            let eventCell = cell as! EventItemCell
            let event = item as! Event
            
            eventCell.labelTime.text = String(format: "%@ - %@",
                                                self.dateFormatter.string(from: event.start),
                                                self.dateFormatter.string(from: event.end))
        }
    }
    
    override open func loadFinished(_ queryCount : Int, querySkip : Int) {
        super.loadFinished(queryCount, querySkip: querySkip)
        self.labelEmpty.isHidden = self.lazyItemsCount() > 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let event = self.lazyItem(indexPath) as? Event {
            if let controller = AppController.instance.getViewControllerByIdentifier("PostOrderViewController", viewController: self) as? PostOrderViewController {
                controller.asset = self.asset
                controller.products = self.products
                controller.event = event
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    open func buildHeaderBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let titleCell = cell as! EventsSectionTitleCell
            
            titleCell.labelTitle.text = item as? String
        }
    }
    
    @objc func actionMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        self.present(alertController, animated: true)
    }
}
