//
//  ShopViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 31/05/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class ShopViewController: HKBaseShopViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = Str.screen_owner_details_title
        
        // add right button on navigation bar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(ShopViewController.actionMenu))
    }
    
    override func buildViewController(_ shopInfo: ShopInfo) -> UIViewController {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: OwnerDetailsViewController.self),
                                                                                 viewController: self) as? OwnerDetailsViewController {
            controller.shopInfo = shopInfo
            return controller
        }
        return super.buildViewController(shopInfo)
    }
    
    @IBAction func actionMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))

        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        if self.contentItem?.shop.identityId == AppApi.getOwnerShop()?.identityId {
            alertController.addAction(UIAlertAction(title: Str.menu_owner_details_edit, style: .default) {[unowned self] action in
                if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: OwnerRegistrationViewController.self),
                                                                                         viewController: self) as? OwnerRegistrationViewController {
                    self.present(controller, animated: true, completion: nil)
                }
            })
        }
        
        self.present(alertController, animated: true)
    }
    
    override func shopLoadingError(_ error: ApiError) {
        let failAction: (()->Void) = { [unowned self] in
            self.navigationController?.popViewController(animated: true)
        }
        
        if !AppErrors.handleError(self, error: error, fallAction: failAction) {
            CustomAlertView(apiError: error, okAction: failAction).show()
        }
    }
    
}
