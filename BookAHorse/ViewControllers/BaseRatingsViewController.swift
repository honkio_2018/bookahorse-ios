//
//  BaseRatingsViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/4/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

open class BaseRatingsViewController: HKBaseOrderCommentsTableViewController {
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    let dateComponentsFormatter = DateComponentsFormatter()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        dateComponentsFormatter.allowedUnits = [.year,.month,.weekOfMonth,.day,.hour,.minute]
        dateComponentsFormatter.maximumUnitCount = 1
        dateComponentsFormatter.unitsStyle = .full
    }

    open override func cellIdentifier(_ item: UserComment) -> String {
        return "OrderCommentCell"
    }
    
    open override func buildCellBunder() -> HKBaseTableViewController.HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let rating = item as! UserComment
            let ratingCell = cell as! OrderCommentCell
            
            if let ratingTime = rating.time, let ratingTimeAgo = self.dateComponentsFormatter.string(from: ratingTime, to: Date()) {
                ratingCell.labelDate.text = "\(ratingTimeAgo) \("screen_notifications_title_ago".localized)"
            }
            
            ratingCell.labelComment.text = "\"\((rating.text?.isEmpty ?? true) ? "..." : rating.text!)\""
            ratingCell.labelName.text = "\(rating.voter.userFirstName ?? "-") \(rating.voter.userLastName ?? "-")"
            ratingCell.showRating(Int(rating.mark))
        }
    }
    
    open override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
