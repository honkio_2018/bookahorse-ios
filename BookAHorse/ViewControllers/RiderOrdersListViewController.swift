//
//  RiderOrdersListViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/13/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderOrdersListViewController: OrdersListViewController {
    
    enum RiderFilter: String {
        case current = "current"
        case history = "history"
    }
    
    private static let RiderFilterKey = "BookHorse_RiderFilter"
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var emptyLabel: UILabel!
    
    private var riderFilter: RiderFilter = RiderFilter.init(rawValue: StoreData.load(fromStore: RiderFilterKey) as? String ?? RiderFilter.current.rawValue)! {
        didSet {
            
            StoreData.save(toStore: self.riderFilter.rawValue, forKey: RiderOrdersListViewController.RiderFilterKey)
            
            self.refresh()
            self.updateFilterLabels(self.riderFilter)
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.riderFilter = .current
        self.tableView.tableFooterView = UIView()
        
        btnFilter.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        
        self.updateFilterLabels(self.riderFilter)
    }
    
    @IBAction func filterAction(_ sender: Any?) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_orders_list_filter_current, style: .default) {
            [unowned self] action in
            self.riderFilter = .current
        })
        alertController.addAction(UIAlertAction(title: Str.screen_orders_list_filter_history, style: .default) {
            [unowned self] action in
            self.riderFilter = .history
        })
        
        self.present(alertController, animated: true)
    }
    
    //MARK: - HONKIO
    override open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        if AppApi.riderRole()?.isChild ?? true {
            filter.thirdPerson = HonkioApi.activeUser()?.userId
        } else {
            filter.owner = HonkioApi.activeUser()?.userId
        }
        
        filter.childMerchants = true
        filter.model = Order.Model.model.rawValue
        filter.queryUnreadMessages = true
        
        switch self.riderFilter {
        case .current:
            filter.status = [
                Order.Status.booked.rawValue,
                Order.Status.bookRequest.rawValue
            ].joined(separator: "|")
            break
        case .history:
            filter.status = [
                Order.Status.canceldByRider.rawValue,
                Order.Status.canceledByOwner.rawValue,
                Order.Status.rideCompleted.rawValue
            ].joined(separator: "|")
            break
        }
    }
    
    private func updateFilterLabels(_ filter: RiderFilter) {
        switch filter {
        case .current:
            self.btnFilter.setTitle(Str.screen_orders_list_filter_current, for: .normal)
            break
        case .history:
            self.btnFilter.setTitle(Str.screen_orders_list_filter_history, for: .normal)
            break
        }
    }
    
    override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
