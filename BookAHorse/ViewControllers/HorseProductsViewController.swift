//
//  HorseSheduleViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/6/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseProductsViewController: HKBaseScheduleProductsTableViewController, HKAssetViewProtocol, HorseDetailsCellDelegate, HKLoginViewLoginDelegate, HKOrderSetVCStrategyDelegate {
    
    private static let LCO_TAG_INTEREST = "LCO_TAG_INTEREST"
    
    public var asset: HKAsset? {
        didSet {
            if self.shop?.identityId != self.asset?.horseShop {
                self.shop = nil
            }
            self.updateHeader()
            
            if self.shop == nil && self.asset?.horseShop != nil {
                HonkioApi.shopFind(byId: self.asset!.horseShop!, flags: 0) {[weak self] (response) in
                    if let list = (response.result as? ShopFind)?.list {
                        if list.count > 0 {
                            self?.shop = list[0]
                        }
                    }
                }
            }
        }
    }
    
    private var isOrderExists: Bool? {
        didSet {
            self.updateHeader()
            self.tableView.reloadData()
        }
    }
    
    private var shop: Shop? {
        didSet {
            if self.shop != nil {
                self.checkOrders(self.shop!)
            }
        }
    }
    
    func loadAsset(_ assetId: String) {
        self.filter.objectId = assetId
        HonkioApi.userGetAsset(assetId, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                self?.asset = response.result as? HKAsset
            }
        }
    }
    
    private let headerSection = HKTableViewSection()
    
    let horseDetailsCell = "HorseDetailsCell"

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    
    override func buildSource(_ lazySection: HKTableViewSection) {
        self.itemsSource.removeAll()
        self.itemsSource.append(self.headerSection)
        self.itemsSource.append(lazySection)
        self.updateHeader()
    }
    
    @IBAction func actionInterest(_ sender: Any?) {
        let order = Order()!
        
        order.userOwner = HonkioApi.activeUser()?.userId
        order.asset = self.asset?.assetId
        order.model = Order.Model.modelInterest.rawValue
        order.title = self.asset?.horseName
        order.shopId = self.asset?.horseShop
        order.status = Order.Model.modelInterest.rawValue
        
        let strategy = HKOrderSetVCStrategy(HorseProductsViewController.LCO_TAG_INTEREST, parent: self)
        strategy.removeOnCompletion = true
        strategy.isAccountRequired = false
        strategy.isPinRequired = true
        
        strategy.userSetOrder(order, shop: self.shopIdentity, flags: 0)
    }
    
    func orderWillSet(_ vcStrategy: HKSimpleOrderSetVCStrategy) {
        BusyIndicator.shared.showProgressView(self.view)
    }
    
    func orderDidSet(_ vcStrategy: HKSimpleOrderSetVCStrategy, order : Order) {
        if HorseProductsViewController.LCO_TAG_INTEREST == vcStrategy.tag {
        CustomAlertView(okTitle: Str.screen_horse_product_list_dialog_interest_published_title, okMessage: Str.screen_horse_product_list_dialog_interest_published_message, okAction: nil).show()
        }
    }
    
    func onOrderSetError(_ vcStrategy: HKSimpleOrderSetVCStrategy, error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
    
    override func cellIdentifier(_ item: Product) -> String {
        return "ProductItemCell"
    }
    
    override func setupFilter(_ filter: ScheduleFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        
        self.filter.objectId = self.asset?.assetId ?? self.filter.objectId
        self.filter.objectType = HK_OBJECT_TYPE_ASSET
        self.filter.startDate = Date()
        
        var dateComp = DateComponents()
        dateComp.month = 3
        self.filter.endDate = Calendar.current.date(byAdding: dateComp, to: self.filter.startDate)!
    }
    
    override func loadFinished() {
        super.loadFinished()
        
        if self.lazyItemsCount() == 0 {
            self.lazySection.items.append(("HorseDetailsEmptyCell", nil, { (viewController, cell, item) in
                let emptyCell = cell as! HorseDetailsEmptyCell
                
                emptyCell.btnInterest.removeTarget(viewController, action: #selector(HorseProductsViewController.actionInterest(_:)), for: .touchUpInside)
                emptyCell.btnInterest.addTarget(viewController, action: #selector(HorseProductsViewController.actionInterest(_:)), for: .touchUpInside)
            }))
            self.tableView.reloadData()
        }
    }
    
    override func buildCellBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let productCell = cell as! ProductItemCell
            let product = item as! Product
            
            productCell.labelName.text = product.name
            productCell.labelDesc.text = product.desc
            productCell.labelAmount.text = String(format: "%.2f %@", arguments: [product.amount, product.currency ?? "null"])
        }
    }
    
    func buildHeaderCellBunder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let detailsCell = cell as! HorseDetailsCell
            let horse = item as! (asset: HKAsset, shop: Shop?, isOrderExists: Bool?)
            
            detailsCell.labelHorseName.text = horse.asset.name
            
            if horse.isOrderExists == true || horse.asset.horseOwner == HonkioApi.activeUser()?.userId {
                detailsCell.labelOwnerName.text = horse.shop?.name ?? "..."
                detailsCell.labelHorseLocation.text = horse.asset.address?.fullAddress() ?? "-"
            }
            else if horse.isOrderExists == false {
                detailsCell.labelOwnerName.text = Str.screen_order_details_hidden_data
                detailsCell.labelHorseLocation.text = horse.asset.address?.shortAddress() ?? "-"
            }
            else if horse.isOrderExists == nil {
                detailsCell.labelOwnerName.text = "..."
                detailsCell.labelHorseLocation.text = horse.asset.address?.shortAddress() ?? "-"
            }
            
//            let horseImageUrl = AppApi.MEDIA_URL_ASSET + horse.asset.assetId
            if let horseImageUrl = horse.asset.horsePhoto {
                detailsCell.imgHorse.imageFromURL(link: horseImageUrl,
                                                     errorImage: UIImage(named: "ic_placeholder_horse"),
                                                     contentMode: nil)
            } else {
                detailsCell.imgHorse.image = UIImage(named: "ic_placeholder_horse")
            }
            
            if let ownerId = horse.asset.horseShop {
                let ownerImageUrl = AppApi.MEDIA_URL_SHOP_PHOTO + ownerId
                detailsCell.imgOwner.imageFromURL(link: ownerImageUrl, errorImage: UIImage(named: "ic_placeholder_owner"), contentMode: nil)
            }
            else {
                detailsCell.imgOwner.image = UIImage(named: "ic_placeholder_owner")
            }
            
            detailsCell.delegate = viewController as? HorseDetailsCellDelegate
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if self.isLasySection(self.itemsSource[indexPath.section]) {
            if !HonkioApi.isConnected() {
                let alertController = UIAlertController(title: Str.screen_horses_list_dialog_not_logged_in_title,
                                                        message: Str.screen_horses_list_dialog_not_logged_in_message,
                                                        preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: Str.screen_horses_list_dialog_not_logged_in_cancel, style: .cancel)
                alertController.addAction(cancelAction)
                let loginAction = UIAlertAction(title: "Login", style: .default) { [unowned self](action: UIAlertAction) in
                    print("Login pressed");
                    if let viewController = AppController.instance.getViewControllerById(AppController.Login, viewController: self) as? HKLoginViewProtocol {
                        viewController.doLogin = false
                        viewController.loginDelegate = self
                        self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
                    }
                }
                alertController.addAction(loginAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else if let product = self.lazyItem(indexPath) as? Product, HonkioApi.isConnected() {
                if HonkioApi.activeUser()?.userId == asset?.horseOwner {
                    CustomAlertView(okTitle: Str.screen_horse_schedule_dialog_own_horse_title,
                                    okMessage: Str.screen_horse_schedule_dialog_own_horse_message,
                                    okAction: nil).show()
                } else {
                    if !HorseMatchingHelper.isHorseSuitableForRider(AppApi.apiInstance.riderRole!, self.asset!) {
                        CustomAlertView(okTitle: Str.screen_horse_schedule_dialog_horse_not_suitable_title,
                                        okMessage: Str.screen_horse_schedule_dialog_horse_not_suitable_message,
                                        okAction: nil).show()
                    } else {
                        if self.shop != nil, let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseEventsListViewController.self), viewController: self) as? HorseEventsListViewController {
                            controller.shopIdentity = self.shop
                            controller.products =  [BookedProduct(product: product, andCount: 1)]
                            controller.asset = self.asset
                            self.navigationController?.pushViewController(controller, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    private func updateHeader() {
        self.headerSection.items.removeAll()
        if self.asset == nil {
            self.headerSection.items.append(("ProgressCell", nil, nil))
        }
        else {
            self.headerSection.items.append((horseDetailsCell, (asset: self.asset, shop: self.shop, isOrderExists: self.isOrderExists), self.buildHeaderCellBunder()))
        }
    }

    func horseImageClick(_ cell: HorseDetailsCell) {
        if let asset = self.asset {
            if let controller = AppController.getViewControllerById(AppController.Asset, viewController: self) as? HKAssetViewProtocol {
                controller.asset = asset
                self.navigationController?.pushViewController(controller as! UIViewController, animated: true)
            }
        }
    }
    
    func horseAddressClick(_ cell: HorseDetailsCell) {
        if let asset = self.asset {
            if let controller = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {
                controller.location = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    func ownerImageClick(_ cell: HorseDetailsCell) {
        if let asset = self.asset {
            if let controller = AppController.getViewControllerById(AppController.Shop, viewController: self) as? HKBaseShopViewController {
                if let identity = self.shop {
                    controller.load(byIdentyty: identity)
                }
                else {
                    controller.load(byId: asset.horseShop!)
                }
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    // MARK: - HKLoginViewLoginDelegate
    
    func loginFinished(_ view: HKLoginViewProtocol, _ isLogedin: Bool) {
        if isLogedin {
            // Restart main screen if logedin
            if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
                
                if let navigationVC = viewController as? UINavigationController {
                    if let thisVC = AppController.instance.getViewControllerByIdentifier(String(describing: HorseProductsViewController.self), viewController: self) as? HKAssetViewProtocol {
                        thisVC.asset = self.asset
                        navigationVC.pushViewController(thisVC as! UIViewController, animated: false)
                    }
                }
                
                let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                appDelegate.changeRootViewController(viewController)
            }
        }
        else if let delegate = view as? HKLoginViewLoginDelegate {
            delegate.loginFinished(view, isLogedin)
        }
    }
    
    private func checkOrders(_ shop: Shop) {
        if HonkioApi.isConnected() && AppSettings.theme == ROLE_RIDER {
            let filter = OrderFilter()
            if let riderRole = AppApi.apiInstance.riderRole, riderRole.isChild {
                filter.thirdPerson = HonkioApi.activeUser()?.userId
            } else {
                filter.owner = HonkioApi.activeUser()?.userId
            }
            filter.isQueryOnlyCount = true
            filter.model = Order.Model.model.rawValue
            filter.status = [Order.Status.booked.rawValue, Order.Status.rideCompleted.rawValue].joined(separator: "|")
            HonkioApi.userGetOrders(filter, shop: shop, flags: 0) { [weak self] (response) in
                self?.isOrderExists = response.isStatusAccept() && ((response.result as? OrdersList)?.totalCount ?? 0) > 0
            }
        }
        else {
            self.isOrderExists = false
        }
    }
}
