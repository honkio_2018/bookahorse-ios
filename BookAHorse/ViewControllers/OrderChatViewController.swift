//
//  OrderChatViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 17/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

/**
 The view controller for managing chat messages.
 */
class OrderChatViewController: HKBaseOrderChatViewController {

    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var messageTextBottomConstraint: NSLayoutConstraint!
    @IBOutlet open var progressView: UIActivityIndicatorView!
    
    private var filter: ChatMessageFilter?
    
    var isHorseOwnerChat: Bool {
        return (order.model == Order.Model.model.rawValue) &&
            AppApi.apiInstance.ownerRole != nil &&
            AppApi.apiInstance.ownerRole?.ownerShopId == order.shopId &&
            AppSettings.theme == ROLE_OWNER
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 76
        self.tableView.allowsSelection = false
        
        self.messageTextField.returnKeyType = .send
        // delegate set in storyboard
//        self.messageTextField.delegate = self
        self.title = order?.title
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(OrderChatViewController.keyboardWillShow),
                                               name: NSNotification.Name.UIKeyboardWillShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(OrderChatViewController.keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override open func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }
    }
    
    open override func buildSource(_ lazySection : HKTableViewSection) {
        itemsSource.removeAll()
        itemsSource.append(lazySection)
    }

    @IBAction func actionSend() {
        if let messageTxt = self.messageTextField.text {
            let trimMessageText = messageTxt.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
            
            if !trimMessageText.isEmpty {
                let message = ChatMessage()
                
                message?.orderId = self.order.orderId
                message?.orderThirdPerson = self.order.thirdPerson
                message?.senderId = HonkioApi.activeUser()?.userId
                message?.text = trimMessageText
                
                // getting sender name
                if self.order.userOwner == message?.senderId || self.order.thirdPerson == message?.senderId {
                    message?.senderRole = AppApi.ROLE_RIDER
                    message?.senderFirstName = HonkioApi.activeUser()?.firstname
                    message?.senderLastName = HonkioApi.activeUser()?.lastname
                }
                else {
                    message?.senderRole = AppApi.ROLE_OWNER
                    message?.senderFirstName = nil
                    message?.senderLastName = nil
                    
                    let shop = AppApi.getOwnerShop()
                    
                    message?.shopId = shop?.identityId
                    message?.shopName = shop?.getProperty(MERCHANT_SHOP_FIELD_NAME) as? String
                }
                
                self.sendMessage(message!)
            }
        }
        self.messageTextField.text = ""
    }
    
    @objc open func keyboardWillShow(_ notification:Notification) {
        let info = (notification as NSNotification).userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.messageTextBottomConstraint.constant = keyboardFrame.size.height
        })
    }
    
    @objc open func keyboardWillHide(_ notification:Notification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.messageTextBottomConstraint.constant = 0
        })
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.messageTextField {
            self.actionSend()
        }
        return true
    }
    
    open override func textViewDidBeginEditing(_ textView: UITextView) {
        // Do nothing
    }
    
    open override func textViewDidEndEditing(_ textView: UITextView) {
        // Do nothing
    }
    
    open override func textFieldDidBeginEditing(_ textField: UITextField) {
        // Do nothing
    }
    
    open override func textFieldDidEndEditing(_ textField: UITextField) {
        // Do nothing
    }
    
    open override func cellIdentifier(_ item: ChatMessage) -> String {
        if isMyMessage(item) { // is user sender
            return "ChatMessageCellRight"
        }
        return "ChatMessageCellLeft"
    }
    
    var dateTimeFormatter : DateFormatter!
    func dateTimeFormater() -> DateFormatter {
        if dateTimeFormatter == nil {
            dateTimeFormatter = DateFormatter()
            dateTimeFormatter.dateStyle = .short
            dateTimeFormatter.timeStyle = .short
        }
        return dateTimeFormatter
    }
    
    func formatDateTime(_ date: Date) -> String {
        let formater = dateTimeFormater()
        return formater.string(from: date)
    }
    
    
    open override func sendMessage(_ message: ChatMessage) {
        message.time = Date()
        self.addMessageToList(message)
        
        if !isHorseOwnerChat {
            weak var weakSelf = self;
            HonkioApi.userSend(message, flags: 0) { (response) in
                if let strongSelf = weakSelf {
                    if response.isStatusAccept() {
                        // mark message as sent
                    }
                    else {
                        let _ = AppErrors.handleError(strongSelf, response: response)
                    }
                }
            }
        } else {
            weak var weakSelf = self;
            HonkioMerchantApi.merchantSend(message, flags: 0) { (response) in
                if let strongSelf = weakSelf {
                    if response.isStatusAccept() {
                        // mark message as sent
                    }
                    else {
                        let _ = AppErrors.handleError(strongSelf, response: response)
                    }
                }
            }
        }
    }
    
    /**
     Loads the list of ChatMessages.
     - parameter responseHandler: The handler to be used for managing the response.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if self.filter == nil {
            self.filter = ChatMessageFilter()
            self.filter?.orderId = self.order.orderId
        }
        self.setupFilter(filter!, count: count, skip: skip)
        
        if isHorseOwnerChat {
            HonkioMerchantApi.merchantGetChatMessages(filter, flags: 0 , handler: responseHandler)
        } else {
            HonkioApi.userGetChatMessages(filter, flags: 0 , handler: responseHandler)
        }
        
        if (skip == 0) {
            self.progressView.isHidden = false
            self.progressView.startAnimating()
        }
        else {
            self.progressView.isHidden = true
        }
    }
    
    open override func loadFinished() {
        super.loadFinished()
        self.progressView.stopAnimating()
    }
    
    fileprivate func addMessageToList(_ message: ChatMessage) {
        self.lazySection.items.append((cellIdentifier(message), message, buildCellBunder()))
        self.tableView.reloadData()
        
        //        let newCell = self.tableView(self.tableView, cellForRowAt: self.lasyIndexPath(self.lazySection.items.count - 1))
        //        newCell.layoutIfNeeded()
        //        let verticalContentOffset = self.tableView.contentOffset.y + newCell.frame.height
        //        self.tableView.setContentOffset(CGPoint(x: 0, y: verticalContentOffset), animated: true)
        
        self.tableView.scrollToRow(at: self.lasyIndexPath(self.lazySection.items.count - 1), at: .bottom, animated: false)
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open override func buildCellBunder() -> HKTableViewItemInitializer? {
        return {[unowned self] (viewController, cell, item) in
            let chatCell = cell as? BaseChatMessageCell
            let chatMessage = item as? ChatMessage
            let parent = viewController as? OrderChatViewController
            
            chatCell?.backgroundColor = UIColor.clear
            chatCell?.labelDate.text = parent?.formatDateTime(chatMessage!.time)
            chatCell?.labelMessage.text = "\(chatMessage?.senderName() ?? ""): \(chatMessage?.text ?? "null")"
            
            if self.isMyMessage(chatMessage) { // is user sender
                if chatMessage?.senderRole == AppApi.ROLE_RIDER {
                    chatCell?.viewBackground.image = UIImage(named: (chatMessage?.senderId == self.order.userOwner) ? "bg_chat_msg_box_orderer_right" : "bg_chat_msg_box_rider_right")
                }
                else {
                    chatCell?.viewBackground.image = UIImage(named: "bg_chat_msg_box_employer_right")
                }
            }
            else {
                if chatMessage?.senderRole == AppApi.ROLE_RIDER {
                    chatCell?.viewBackground.image = UIImage(named: (chatMessage?.senderId == self.order.userOwner) ? "bg_chat_msg_box_orderer_left" : "bg_chat_msg_box_rider_left")
                }
                else {
                    chatCell?.viewBackground.image = UIImage(named: "bg_chat_msg_box_employer_left")
                }
            }
        }
    }
    
    private func isMyMessage(_ item: ChatMessage?) -> Bool {
        return item?.senderId == HonkioApi.activeUser()?.userId ||
            (item?.senderId == nil && item?.shopId == AppApi.getOwnerShop()?.identityId)
        
    }
}
