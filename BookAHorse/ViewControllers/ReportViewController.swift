//
//  ReportViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 3/1/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

fileprivate typealias ReportItem = (transaction: Transaction, order: Order?)

class ReportViewController: HKBaseLazyTableViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelEmpty: UILabel!
    
    private var month: Int = 1
    private var year: Int = 2019
    private var calendar: Calendar = Calendar.current
    
    private var components: Set<Calendar.Component> = [Calendar.Component.month, Calendar.Component.year]
    private var dateFormater: DateFormatter = DateFormatter()
    private var itemDateFormater: DateFormatter = DateFormatter()
    
    private let filter = TransactionFilter()
    
    fileprivate(set) open var footerSection : HKTableViewSection!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: 32,left: 0,bottom: 0,right: 0)
        
        dateFormater.dateFormat = "MMMM YYYY"
        itemDateFormater.dateFormat = "dd/MM"
        
        var dateComponents = calendar.dateComponents(self.components, from: Date())
        
        self.month = dateComponents.month ?? 1
        self.year = dateComponents.year ?? 2019
        
        self.updateDateLabel()
        self.updateFilterDates()
        
        self.filter.status = RESPONSE_STATUS_ACCEPT
        self.filter.type = "order_payments"
        self.filter.isQueryOrderDetails = true
    }
    
    @IBAction func actionNextMonth(_ sender: Any?) {
        var month = self.month + 1
        var year = self.year
        if month > 12 {
            month = 1
            year = year + 1
        }
        
        self.month = month
        self.year = year
        
        self.updateDateLabel()
        self.updateFilterDates()
        self.refresh()
    }
    
    @IBAction func actionPreviousMonth(_ sender: Any?) {
        var month = self.month - 1
        var year = self.year
        if month < 1 {
            month = 12
            year = year - 1
        }
        
        self.month = month
        self.year = year
        
        self.updateDateLabel()
        self.updateFilterDates()
        self.refresh()
    }
    
    @IBAction func actionSendReport(_ sender: Any?) {
        CustomAlertView(title: Str.screen_earnings_list_report_dialog_title, message: Str.screen_earnings_list_report_dialog_desc, cancelButtonTitle: "dlg_common_button_ok".localized) { [unowned self] in
            BusyIndicator.shared.showProgressView(self.view)
            let params: [AnyHashable : Any] = [
                "user_role_id" : AppApi.ownerRole()?.objectId ?? "",
                "start_date" : ApiDateFormatter.serverString(from: self.filter.startDate),
                "end_date" : ApiDateFormatter.serverString(from: self.filter.endDate)
            ]
            HonkioMerchantApi.merchantReportRun(AppApi.REPORT_USER_SEND_REPORT, email: HonkioApi.activeUser()?.login, params: params, flags: 0, handler: {[weak self] (response) in
                BusyIndicator.shared.hideProgressView()
                if response.isStatusAccept() {
                    CustomAlertView(okTitle: nil, okMessage: Str.screen_earnings_list_report_send_message, okAction: nil).show()
                }
                else if let strongSelf = self {
                    let error = response.anyError()
                    if !AppErrors.handleError(strongSelf, error: error) {
                        CustomAlertView(apiError: error, okAction: nil).show()
                    }
                }
            })
        }.addButtonWithTitle("dlg_common_button_cancel".localized, action: {
                // Do nothing
        }).show()
    }
    
    override func buildSource(_ lazySection: HKTableViewSection) {
        self.footerSection = HKTableViewSection()
        
        self.itemsSource.append(lazySection)
        self.itemsSource.append(self.footerSection)
    }
    
    override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.filter.queryCount = Int32(count)
        self.filter.querySkip = Int32(skip)
        HonkioMerchantApi.merchantTransactionsList(self.filter, shop:AppApi.getOwnerShop(), flags: 0, handler: responseHandler)
    }
    
    override func toList(_ response: Response) -> [Any?] {
        let transactions = response.result as! Transactions
        
        var list: [ReportItem] = []
        
        for transaction in transactions.list {
            if transaction.orderId != nil {
                list.append((transaction: transaction, order: transactions.orders?[transaction.orderId!]))
            }
        }
        return list
    }
    
    override func mapItem(_ item: Any?) -> (HKTableViewItem) {
        return (identifierOrNibName: "ReportItemTableCell", item: item, initializer: { (viewController, cell, item) in
            let reportCell = cell as? ReportItemTableCell
            let reportItem = item as! ReportItem
            
            let date = (viewController as? ReportViewController)?.itemDateFormater.string(from: reportItem.transaction.time)
            
            reportCell?.setBold(false)
            reportCell?.labelTitle.text = String(format: "%@ %@", date ?? "", reportItem.order?.title ?? "-")
            reportCell?.labelAmount.text = String(format: "%.2f", reportItem.transaction.amount)
            reportCell?.labelCurrency.text = reportItem.transaction.currency
        })
    }
    
    override func loadFinished() {
        self.footerSection.items.removeAll()
        if !self.isCanBeUpdated {
            
            if self.lazyList.count > 0 {
                self.labelEmpty.isHidden = true
                
                var amountTotal: Double = 0.0
                var amountFee: Double = 0.0
                var amountVat: Double = 0.0
                var currency: String? = nil
                
                for item in self.lazyList {
                    if let reportItem = item as? ReportItem {
                        for product in reportItem.transaction.products {
                            if AppApi.FEE_PRODUCT_ID == product.productId {
                                amountFee += product.amount_no_vat * Double(product.count)
                                amountVat += product.amount_vat * Double(product.count)
                            }
                            else {
                                amountTotal += product.amount * Double(product.count)
                            }
                        }
                        
                        if currency == nil {
                            currency = reportItem.transaction.currency
                        }
                    }
                }
                
                let binder: HKTableViewItemInitializer = { (viewController, cell, item) in
                    let reportCell = cell as? ReportItemTableCell
                    let closure = item as? (title: String?, amount: Double?, currency: String?)
                    
                    reportCell?.setBold(true)
                    reportCell?.labelTitle.text = closure?.title
                    reportCell?.labelAmount.text = String(format: "%.2f", closure?.amount ?? 0.0)
                    reportCell?.labelCurrency.text = closure?.currency
                }
                
                self.footerSection.items.append(("ReportSeparatorTableCell", nil, nil))
                
                self.footerSection.items.append((
                    identifierOrNibName: "ReportItemTableCell",
                    item: (title: Str.screen_earnings_list_total, amount: amountTotal, currency: currency),
                    initializer: binder))
                
                self.footerSection.items.append(("ReportSeparatorTableCell", nil, nil))
                
                self.footerSection.items.append((
                    identifierOrNibName: "ReportItemTableCell",
                    item: (title: Str.screen_earnings_list_fee, amount: -amountFee, currency: currency),
                    initializer: binder))
                
                self.footerSection.items.append((
                    identifierOrNibName: "ReportItemTableCell",
                    item: (title: Str.screen_earnings_list_vat, amount: -amountVat, currency: currency),
                    initializer: binder))
                
                self.footerSection.items.append(("ReportSeparatorTableCell", nil, nil))
                
                self.footerSection.items.append((
                    identifierOrNibName: "ReportItemTableCell",
                    item: (title: Str.screen_earnings_list_earnings, amount: amountTotal - amountFee - amountVat, currency: currency),
                    initializer: binder))
            }
            else {
                self.labelEmpty.isHidden = false
            }
        }
        super.loadFinished()
    }
    
    private func updateDateLabel() {
        self.labelDate.text = self.dateString(self.month, self.year)
    }
    
    private func dateString(_ month: Int, _ year: Int) -> String {
        var dateComponents = calendar.dateComponents(self.components, from: Date())
        dateComponents.setValue(self.month, for: Calendar.Component.month)
        dateComponents.setValue(self.year, for: Calendar.Component.year)
        
        return dateFormater.string(from: calendar.date(from: dateComponents)!)
    }
    
    private func updateFilterDates() {
        var dateComponents = self.calendar.dateComponents(self.components, from: Date())
        dateComponents.setValue(self.month, for: Calendar.Component.month)
        dateComponents.setValue(self.year, for: Calendar.Component.year)
        
        dateComponents.setValue(1, for: Calendar.Component.day)
        dateComponents.setValue(0, for: Calendar.Component.hour)
        dateComponents.setValue(0, for: Calendar.Component.minute)
        dateComponents.setValue(0, for: Calendar.Component.second)
        dateComponents.setValue(0, for: Calendar.Component.nanosecond)
        self.filter.startDate = self.calendar.date(from: dateComponents)!
        
        var nextMont = self.month + 1
        var nextYear = self.year
        if nextMont > 12 {
            nextMont = 1
            nextYear = nextYear + 1
        }
        
        dateComponents.setValue(nextMont, for: Calendar.Component.month)
        dateComponents.setValue(nextYear, for: Calendar.Component.year)
        
        self.filter.endDate = self.calendar.date(from: dateComponents)?.addingTimeInterval(-1)
    }
}
