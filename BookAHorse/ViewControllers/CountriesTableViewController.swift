//
//  CountriesTableViewController.swift
//
//  Created by Shurygin Denis on 9/20/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

open class CountriesTableViewController: HKWizardViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet open var tableView: UITableView!
    
    public var completionAction: ((CountryInfo) -> Void)!
    
    public var list: [CountryInfo] = HKCountries.List
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 64
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName:"CountryViewCell", bundle: nil), forCellReuseIdentifier: "CountryViewCell")
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(CountriesTableViewController.cancelAction))
    }
    
    @objc open func cancelAction() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource

    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.list.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CountryViewCell") as? CountryViewCell {
            let iso = self.list[indexPath.row].iso
            let countryName = self.list[indexPath.row].name
            
            let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: iso!])
            cell.titleLabel.text = NSLocale(localeIdentifier: Locale.current.identifier).displayName(forKey: NSLocale.Key.identifier, value: identifier) ?? countryName
            return cell
        }
        return UITableViewCell()
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = self.localizedCountry(self.list[indexPath.row])
        if self.completionAction != nil {
            self.completionAction(country)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func localizedCountry(_ country: CountryInfo) -> CountryInfo {
        let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue : country.iso])
        if let name = NSLocale(localeIdentifier: Locale.current.identifier).displayName(forKey: NSLocale.Key.identifier, value: identifier) {
            let localizedCountry = CountryInfo()
            localizedCountry.iso = country.iso
            localizedCountry.name = name
            localizedCountry.nameTranslated = name
            
            return localizedCountry
        }
        
        return country
    }
}
