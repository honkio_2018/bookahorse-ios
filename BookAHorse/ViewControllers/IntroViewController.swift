//
//  CSIntroVC.swift
//  happs
//
//  Created by Shurygin Denis on 8/11/16.
//  Copyright © 2016 SoftDev. All rights reserved.
//

import UIKit
import AVKit
import HonkioAppTemplate

open class IntroViewController: HKBaseViewController, IntroLastPageViewDelegate {
    
    public static let INTRO_DID_SHOWN = "IntroViewController.INTRO_DID_SHOWN"
    
    fileprivate static let BUTTON_CORNER_RADIUS : CGFloat = 5;
    
    fileprivate fileprivate(set) lazy var wpTitleView : UIView = self.makeTitleView()
    fileprivate fileprivate(set) lazy var pagerView : UIScrollView = self.makePagerView()
    fileprivate fileprivate(set) lazy var pageControlView : UIPageControl = self.makePageControlView()
    fileprivate fileprivate(set) lazy var nextButton : UIView = self.makeNextButton()
    
    fileprivate var lastPageView : IntroLastPageView? {
        get {
            return pages[pages.count - 1] as? IntroLastPageView
        }
    }
    
    fileprivate fileprivate(set) lazy var pages : [UIView] = [
        self.makePageView("intro_state_1_image", title: "screen_intro_title_1".localized, text: "screen_intro_desc_1".localized),
        self.makePageView("intro_state_2_image", title: "screen_intro_title_2".localized, text: "screen_intro_desc_2".localized),
        self.makePageView("intro_state_3_image", title: "screen_intro_title_3".localized, text: "screen_intro_desc_3".localized),
        self.makeLastPageView()
    ]
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        setupConstraints()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    public func actionShowVideo() {
        let viewController = AVPlayerViewController()
        viewController.player = AVPlayer(url: URL(string: Str.screen_intro_video_link)!)
        viewController.title = Str.app_name
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func actionNext() {
        if pageControlView.currentPage < pages.count - 1 {
            pagerView.setContentOffset(CGPoint(x: pagerView.contentOffset.x + pagerView.frame.width, y: pagerView.contentOffset.y), animated: true)
        }
        else {
            StoreData.save(toStore: true, forKey: IntroViewController.INTRO_DID_SHOWN)
            
            if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
                
                let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                appDelegate.changeRootViewController(viewController)
            }
        }
    }
    
    func setupSubviews() {
        
        self.view.addSubview(wpTitleView)
        self.view.addSubview(pagerView)
        self.view.addSubview(pageControlView)
        self.view.addSubview(nextButton)
        
        for page in pages {
            self.pagerView.addSubview(page)
        }
        
        pageControlView.numberOfPages = self.pages.count
        pageControlView.currentPage = 0
        
        pagerView.delegate = self
        lastPageView?.delegate = self
    }
    
    open func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = getScrolPosition(scrollView)
        pageControlView.currentPage = position
    }
    
    func setupConstraints() {
        
        var views : [String : AnyObject] = [
            "wpTitleView" : wpTitleView,
            "pagerView" : pagerView,
            "pageControlView" : pageControlView,
            "nextButton" : nextButton
        ]
        
        var formatArray : [String] = [
            "V:|-32-[wpTitleView(47)]-8-[pagerView]-0-|",
            "V:[pageControlView]-8-[nextButton(40)]-0-|",
            "H:|-0-[pagerView]-0-|",
            "H:|-0-[pageControlView]-0-|",
            "H:|-0-[nextButton]-0-|"
        ]
        
        var constraints :[NSLayoutConstraint] = NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        NSLayoutConstraint(item: wpTitleView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        
        //============================
        // Pages
        
        views = [:]
        formatArray = [String(format: "H:|-0-[page_%i]", 0), String(format: "H:[page_%i]-0-|", pages.count - 1)]
        
        for i in 0...pages.count-1 {
            views.updateValue(pages[i], forKey: String(format: "page_%i", i))
            formatArray.append(String(format: "V:|-0-[page_%i]-0-|", i))
            
            if i != 0 {
                formatArray.append(String(format: "H:[page_%i]-0-[page_%i]", i-1, i))
            }
            
            NSLayoutConstraint(item: pages[i], attribute: .width, relatedBy: .equal, toItem: pagerView, attribute: .width, multiplier: 1, constant: 0).isActive = true
            NSLayoutConstraint(item: pages[i], attribute: .height, relatedBy: .equal, toItem: pagerView, attribute: .height, multiplier: 1, constant: 0).isActive = true
        }
        
        constraints = NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
    }
    
    fileprivate func getScrolPosition(_ scrollView: UIScrollView) -> Int {
        return Int((scrollView.contentOffset.x + scrollView.bounds.width / 2) / scrollView.bounds.width)
    }
    
    fileprivate func scrolToNextPosition(_ scrollView: UIScrollView) -> Bool {
        let position = getScrolPosition(scrollView)
        if position == Int(scrollView.contentSize.width / scrollView.bounds.width) - 1 {
            // Is last position
            return false
        }
        scrollView.scrollRectToVisible(CGRect(x: CGFloat(position + 1) * scrollView.bounds.width, y: 0, width: scrollView.bounds.width, height: scrollView.bounds.height), animated: true)
        return true
    }
    
    fileprivate func makePagerView() -> UIScrollView {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false;

        view.isPagingEnabled = true
        view.showsHorizontalScrollIndicator = false
        view.bounces = false
        
        return view
    }
    
    fileprivate func makePageControlView() -> UIPageControl {
        let view = UIPageControl()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.currentPageIndicatorTintColor = UIColor(netHex: AppColors.appOrange)
        view.pageIndicatorTintColor = UIColor(netHex: AppColors.appGray)
        
        return view
    }
    
    fileprivate func makePageView(_ image: String, title: String, text: String) -> UIView {
        let view = IntroPageView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        
        view.logoImageView.image = UIImage(named: image)
        view.titleView.text = title
        view.textView.text = text
        
        return view
    }
    
    fileprivate func makeLastPageView() -> IntroLastPageView {
        let view = IntroLastPageView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        
        return view
    }
    
    fileprivate func makeTitleView() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        
        let logoView = UIImageView()
        logoView.translatesAutoresizingMaskIntoConstraints = false
        
        logoView.image = UIImage(named: "logo_nav")
        logoView.contentMode = .scaleAspectFit
        
        let titleView = UILabel()
        titleView.translatesAutoresizingMaskIntoConstraints = false
        
        titleView.numberOfLines = 1
        titleView.textAlignment = .center
        titleView.font = UIFont(name: "OpenSans", size: CGFloat(22))
        titleView.textColor = UIColor.black
        titleView.text = "app_name".localized
        
        view.addSubview(logoView)
        view.addSubview(titleView)
        
        let views : [String : AnyObject] = [
            "logoView" : logoView,
            "titleView" : titleView
            ]
        
        let formatArray : [String] = [
            "V:|-0-[logoView]-0-|",
            "V:|-0-[titleView]-0-|",
            "H:|-0-[logoView]-16-[titleView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] = NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return view
    }
    
    fileprivate func makeNextButton() -> UIView {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false;
        button.setTitle("screen_intro_button_next".localized, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(netHex: AppColors.appOrange)
        button.addTarget(self, action: #selector(IntroViewController.actionNext), for: .touchUpInside)
        return button
    }
    
}
