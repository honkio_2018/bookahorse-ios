//
//  LoginViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 13/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

class LoginViewController: HKOAuthLoginViewController {
    
    override func serverDataDidLoad(doLogin: Bool) {
        HonkioApi.assetStructureList([AppApi.HORSE_STRUCTURE], shop: nil, flags: MSG_FLAG_NO_WARNING, handler: nil)
        HonkioApi.userStructureDescriptionList([AppApi.ROLE_RIDER, AppApi.ROLE_OWNER], shop: nil, flags: MSG_FLAG_NO_WARNING, handler: nil)
        
        super.serverDataDidLoad(doLogin: doLogin)
    }
    
    override func callLoginFinished(_ isLoggedin: Bool) {
        guard isLoggedin else {
            super.callLoginFinished(isLoggedin)
            return
        }
        
        HonkioApi.userAccessList(HonkioApi.mainShopInfo()?.merchant.merchantId,
                                 flags: MSG_FLAG_NO_WARNING,
                                 handler: nil)
        
        loadRoles()
    }

    func callSuperLoginFinished() {
        super.callLoginFinished(true)
    }
    
    func loadRoles() {
        AppApi.userResetRoles()
        HonkioApi.userGetRoles(nil, flags: 0) { [unowned self] (response) in
            guard response.isStatusAccept(),
                let roles = response.result as? UserRolesList
            else {
                //response.anyError()
                self.callSuperLoginFinished()
                return
            }
            
            AppApi.apiInstance.onRolesGet(roles: roles.list)
            self.loadMerchantIfNeeded();
            //self.callSuperLoginFinished()
        }
    }
    
    func loadMerchantIfNeeded() {
        guard let owner = AppApi.ownerRole()
        else {
            callSuperLoginFinished()
            return
        }
        
        if !(owner.ownerMerchantId?.isEmpty ?? true) {
            if AppApi.getOwnerMerchant() == nil {
                HonkioMerchantApi.merchantGet(owner.ownerMerchantId!, flags: 0) {
                    [unowned self] (response) in
                    guard response.isStatusAccept()
                        else {
                            self.callSuperLoginFinished()
                            return
                    }
                    AppApi.apiInstance.onMerchantGet(merchant: response.result as! Merchant)
                    self.loadShopIfNeeded()
                }
            } else {
                self.loadShopIfNeeded()
                // Refresh data in background
                HonkioMerchantApi.merchantGet(owner.ownerMerchantId!,
                                              flags: MSG_FLAG_LOW_PRIORITY | MSG_FLAG_NO_WARNING,
                                              handler: nil)
            }
            return
        }
        callSuperLoginFinished()
    }
    
    func loadShopIfNeeded() {
        guard let owner = AppApi.ownerRole()
            else {
                callSuperLoginFinished()
                return
        }
        if !(owner.ownerShopId?.isEmpty ?? true) {
            if AppApi.apiInstance.getOwnerShop() == nil {
                loadShopById(shopId: owner.ownerShopId!)
            } else {
                callSuperLoginFinished()
                loadShopInBackground(shopId: owner.ownerShopId!)
            }
            return
        }
        callSuperLoginFinished();
    }
    
    func loadShopById(shopId: String) {
        HonkioApi.shopFind(byId: shopId, flags: 0) { [unowned self] (response) in
            guard response.isStatusAccept()
                else {
                    //response.anyError()
                    self.callSuperLoginFinished()
                    return
            }
            
            let shopList = response.result as! ShopFind
            for shop in shopList.list {
                if shopId == shop.identityId {
                    self.loadShopByIdentity(shopIdentity: shop)
                    return
                }
            }
            self.callSuperLoginFinished()
            return
        }
    }
    
    func loadShopByIdentity(shopIdentity: Identity) {
        HonkioMerchantApi.merchantShopGet(shopIdentity, flags: 0) { (response) in
            if response.isStatusAccept() {
                let merchantShopInfo = response.result as! MerchantShopInfo
                AppApi.apiInstance.onShopGet(shop:  merchantShopInfo.shop)
            }
            self.callSuperLoginFinished()
        }
    }
    
    func loadShopInBackground(shopId: String) {
        HonkioApi.shopFind(byId: shopId, flags: MSG_FLAG_LOW_PRIORITY) { (response) in
            guard response.isStatusAccept()
                else {
                    self.callSuperLoginFinished()
                    return
            }
            
            let shopFind = response.result as! ShopFind
            if !shopFind.list.isEmpty {
                HonkioApi.shopGetInfo(shopFind.list.first!,
                                      flags: MSG_FLAG_LOW_PRIORITY | MSG_FLAG_NO_WARNING,
                                      handler: nil)
            }
        }
    }
}
