//
//  RiderRegistration1ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/25/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration1ViewController: HKWizardViewController {
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPhone: UITextField!
    
    private var isInit = false
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.textName.text = role.riderName
                self.textEmail.text = role.email
                self.textPhone.text = role.phone
            }
            isInit = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            
            role.riderName = self.textName.text
            role.email = self.textEmail.text
            role.phone = self.textPhone.text
            
        }
        
        if isValid() {
            self.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        var isValid = true
        
        isValid = validatorNotEmpty.validate(self.textName) && isValid
        isValid = validatorNotEmpty.validate(self.textEmail) && isValid
        isValid = validatorNotEmpty.validate(self.textPhone) && isValid
        
        return isValid
    }
}
