//
//  OwnerRegistration1ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/31/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OwnerRegistration1ViewController: HKWizardViewController {
    
    @IBOutlet var textCompanyId: UITextField!
    @IBOutlet var textCompanyName: UITextField!
    @IBOutlet var textEmail: UITextField!
    @IBOutlet var textAddress: UITextField!
    @IBOutlet var textPhone: UITextField!
    @IBOutlet var textPresentation: CustomUITextView!
    
    private var isInit = false
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.textCompanyId.text = role.ownerCompanyId
                self.textCompanyName.text = role.ownerCompanyName
                self.textEmail.text = role.email
                self.textAddress.text = role.ownerAddress
                self.textPhone.text = role.phone
                self.textPresentation.text = role.presentation
            }
            isInit = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            
            role.ownerCompanyId = self.textCompanyId.text
            role.ownerCompanyName = self.textCompanyName.text
            role.email = self.textEmail.text
            role.ownerAddress = self.textAddress.text
            role.phone = self.textPhone.text
            role.presentation = self.textPresentation.text
            
        }
        
        if isValid() {
            self.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        var isValid = true
        
        isValid = validatorNotEmpty.validate(self.textCompanyId) && isValid
        isValid = validatorNotEmpty.validate(self.textCompanyName) && isValid
        isValid = validatorNotEmpty.validate(self.textEmail) && isValid
        isValid = validatorNotEmpty.validate(self.textPhone) && isValid
        
        return isValid
    }
}
