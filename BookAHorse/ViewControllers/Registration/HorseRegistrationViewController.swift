//
//  HorseRegistrationViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

public protocol HorseRegistrationDelegate {
    func horseDidCreated(_ asset: HKAsset)
}

class HorseRegistrationViewController: HKWizardNavigationController {
    
    var horse: HKAsset? {
        set {
            self.wizardObject = newValue
        }
        get {
            return self.wizardObject as? HKAsset
        }
    }
    
    open var registrationDelegate: HorseRegistrationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.horse == nil {
            let horseAsset = HKAsset()!
            horseAsset.isVisible = true
            horseAsset.merchantId = AppApi.getOwnerMerchant()?.merchantId
            horseAsset.stricture = AppApi.getHorseStructure()
            horseAsset.horseOwner = HonkioApi.activeUser()?.userId
            horseAsset.horseShop = AppApi.getOwnerShop()?.identityId
            self.horse = horseAsset
        }
        
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: String(describing: HorseRegistration1ViewController.self)))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: String(describing: HorseRegistration2ViewController.self)))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: String(describing: HorseRegistration3ViewController.self)))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: String(describing: HorseRegistration4ViewController.self)))
    }
    
    override func wizardComplete() {
        BusyIndicator.shared.showProgressView(self.view)
        
        HonkioMerchantApi.merchantAssetSet((self.wizardObject as! HKAsset), shop: AppApi.getOwnerShop(), flags: 0) { [unowned self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if let horse = response.result as? HKAsset {
                    self.dismiss(animated: true) { [unowned self] in
                        guard self.horse?.assetId == nil else { return }
                        // call delegate only for new horses, going on horses list screen
                        self.registrationDelegate?.horseDidCreated(horse)
                    }
                } else {
                    assertionFailure("new created horse asset not returned in response")
                }
            }
            else {
                let error = response.anyError()
                if !AppErrors.handleError(self, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
//        viewController.title = "\(self.viewControllers.count + 1)/\(self.wizardViewControllers.count)"
        viewController.title = Str.screen_registration_horse_title
        super.pushViewController(viewController, animated: animated)
    }
}
