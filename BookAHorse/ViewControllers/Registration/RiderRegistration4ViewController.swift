//
//  RiderRegistration4ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/28/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration4ViewController: HKBaseTableViewController {
    
    @IBOutlet open var tableView: UITableView!
    
    open var userRole: UserRole? {
        get { return wizardNavigationController?.wizardObject as? UserRole }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }
    
    private let checkableItemCell = "CheckableItemCell"
    private let sectionTitleCell = "SectionTitleCell"
    private let sectionTitleNoDescCell = "SectionTitleNoDescCell"
    
    
    private var competenceLevel: [String] = []
    private lazy var competenceLevelDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.competenceLevel.contains(property.value) {
                self.competenceLevel.append(property.value)
            }
            else if !isOn {
                if let index = self.competenceLevel.index(of: property.value) {
                    self.competenceLevel.remove(at: index)
                }
            }
        }
    }
    
    private var competition: [String] = []
    private lazy var competitionDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.competition.contains(property.value) {
                self.competition.append(property.value)
            }
            else if !isOn {
                if let index = self.competition.index(of: property.value) {
                    self.competition.remove(at: index)
                }
            }
        }
    }
    
    private var horseType: [String] = []
    private lazy var horseTypeDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.horseType.contains(property.value) {
                self.horseType.append(property.value)
            }
            else if !isOn {
                if let index = self.horseType.index(of: property.value) {
                    self.horseType.remove(at: index)
                }
            }
        }
    }
    
    private var temperamentMin: Int = 0
    private var temperamentMax: Int = 1
    private lazy var temperamentDelegate = { (_ object: AnyObject?, _ minValue: Double, _ maxValue: Double) -> Void in
        self.temperamentMin = Int(minValue)
        self.temperamentMax = Int(maxValue)
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            role.riderCompetenceLevel = self.competenceLevel
            role.riderCompetition = competition
            role.riderHorseType = self.horseType
            role.riderHorseTemperamentMin = self.temperamentMin
            role.riderHorseTemperamentMax = self.temperamentMax
        }
        
        if isValid() {
            self.wizardNavigationController?.nextViewController()
        }
    }

    override func viewDidLoad() {
        self.competenceLevel = self.userRole?.riderCompetenceLevel ?? []
        self.competition = self.userRole?.riderCompetition ?? []
        self.horseType = self.userRole?.riderHorseType ?? []
        self.temperamentMin = self.userRole?.riderHorseTemperamentMin ?? 0
        self.temperamentMax = self.userRole?.riderHorseTemperamentMax ?? 1
        
        self.buildItemsSource()
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 69
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    private func buildItemsSource() {
        
        self.itemsSource.removeAll()
        let riderStructure = AppApi.getRiderStructure()!
        let sectionTitleBinder = SectionTitleCell.buildBinder()
        
        // competeceLevel
        var section = HKTableViewSection()
        section.items.append((self.sectionTitleCell, (title: "screen_registration_rider_group".localized, desc: "screen_registration_empty_list_title".localized), sectionTitleBinder))
        
        let completeceLevelType = (riderStructure.getProperty(UserRole.Fields.competenceLevel.rawValue) as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        var binder = buildCompetenceLevelBinder()
        for value in completeceLevelType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        self.itemsSource.append(section)
        
        // Competition
        section = HKTableViewSection()
        section.items.append((self.sectionTitleNoDescCell, (title: "screen_registration_rider_competition_field".localized, desc: ""), sectionTitleBinder))
        
        let completionType = (riderStructure.getProperty(UserRole.Fields.competition.rawValue) as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        binder = buildCompetitionBinder()
        for value in completionType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        self.itemsSource.append(section)
        
        // Horse type
        section = HKTableViewSection()
        section.items.append((self.sectionTitleCell, (title: "screen_registration_rider_horse_type".localized, desc: "screen_registration_empty_list_title".localized), sectionTitleBinder))
        
        let horseType = (riderStructure.getProperty(UserRole.Fields.horseType.rawValue) as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        binder = buildHorseTypeBinder()
        for value in horseType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        self.itemsSource.append(section)
        
        // Horse temperament
        section = HKTableViewSection()

        let horseTemperamentMax = (riderStructure.getProperty(UserRole.Fields.temperamentMax.rawValue) as! HKStructureNumericProperty)
        let horseTemperamentMin = (riderStructure.getProperty(UserRole.Fields.temperamentMin.rawValue) as! HKStructureNumericProperty)
        
        section.items.append(("HorseDetailsTemperamentRangeCell", (min: CGFloat(horseTemperamentMin.min), max: CGFloat(horseTemperamentMax.max)), buildTemperamentBinder()))
        
        self.itemsSource.append(section)
    }
    
    private func buildCompetenceLevelBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! RiderRegistration4ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.competenceLevel.contains(property.value)
            checkableCell.switchDelegate = parent.competenceLevelDelegate
        }
    }
    
    private func buildCompetitionBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! RiderRegistration4ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.competition.contains(property.value)
            checkableCell.switchDelegate = parent.competitionDelegate
        }
    }
    
    private func buildHorseTypeBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! RiderRegistration4ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.horseType.contains(property.value)
            checkableCell.switchDelegate = parent.horseTypeDelegate
        }
    }
    
    private func buildTemperamentBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let range = item as! (min: CGFloat, max: CGFloat)
            let rangeCell = cell as! DetailsNumericRangeSliderCell
            let parent = viewController as! RiderRegistration4ViewController
            
            rangeCell.detailNameLabel.text = Str.screen_registration_rider_temperament
            rangeCell.valueRangeSlider.setMinMaxValue(range.min, maxValue: range.max)
            
            rangeCell.valueRangeSlider.setValue(CGFloat(parent.temperamentMin), right: CGFloat(parent.temperamentMax))
            rangeCell.slideDelegate = parent.temperamentDelegate
        }
    }
    
    private func isValid() ->Bool {
        if self.competenceLevel.count == 0 {
            CustomAlertView(okTitle: "screen_registration_rider_group".localized, okMessage: "screen_registration_empty_list_title".localized, okAction: nil).show()
            return false
        }
        if self.horseType.count == 0 {
            CustomAlertView(okTitle: "screen_registration_rider_horse_type".localized, okMessage: "screen_registration_empty_list_title".localized, okAction: nil).show()
            return false
        }
        return true
    }
}
