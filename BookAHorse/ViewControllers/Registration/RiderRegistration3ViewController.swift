//
//  RiderRegistration3ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/25/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration3ViewController: HKWizardViewController {
    
    @IBOutlet weak var btnBirthdate: UIButton!
    @IBOutlet weak var sliderWeight: UISlider!
    @IBOutlet weak var sliderHeight: UISlider!
    @IBOutlet weak var sliderTimesPerWeek: UISlider!
    @IBOutlet weak var sliderTimesPerYear: UISlider!
    @IBOutlet weak var sliderYearsTotal: UISlider!
    
    @IBOutlet weak var labelWeight: UILabel!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var labelTimesPerWeek: UILabel!
    @IBOutlet weak var labelTimesPerYear: UILabel!
    @IBOutlet weak var labelYearsTotal: UILabel!
    
    private var isInit = false
    
    private var birthDate: Date? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let weightType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.weight.rawValue) as? HKStructureNumericProperty {
                self.sliderWeight.maximumValue = Float(weightType.min)
                self.sliderWeight.maximumValue = Float(weightType.max)
            }
            if let lengthType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.length.rawValue) as? HKStructureNumericProperty {
                self.sliderHeight.maximumValue = Float(lengthType.min)
                self.sliderHeight.maximumValue = Float(lengthType.max)
            }
            if let rideFrequencyWeekType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.rideFrequencyWeek.rawValue) as? HKStructureNumericProperty {
                self.sliderTimesPerWeek.maximumValue = Float(rideFrequencyWeekType.min)
                self.sliderTimesPerWeek.maximumValue = Float(rideFrequencyWeekType.max)
            }
            if let rideFrequencyYearType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.rideFrequencyYear.rawValue) as? HKStructureNumericProperty {
                self.sliderTimesPerYear.maximumValue = Float(rideFrequencyYearType.min)
                self.sliderTimesPerYear.maximumValue = Float(rideFrequencyYearType.max)
            }
            if let yearsTotalType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.yearsTotal.rawValue) as? HKStructureNumericProperty {
                self.sliderYearsTotal.maximumValue = Float(yearsTotalType.min)
                self.sliderYearsTotal.maximumValue = Float(yearsTotalType.max)
            }
            
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.sliderWeight.value = Float(role.riderWeight ?? Int(self.sliderWeight.minimumValue))
                self.sliderHeight.value = Float(role.riderLength ?? Int(self.sliderHeight.minimumValue))
                self.sliderTimesPerWeek.value = Float(role.riderRideFrequencyWeek ?? Int(self.sliderTimesPerWeek.minimumValue))
                self.sliderTimesPerYear.value = Float(role.riderRideFrequencyYear ?? Int(self.sliderTimesPerYear.minimumValue))
                self.sliderYearsTotal.value = Float(role.riderYearsTotal ?? Int(self.sliderYearsTotal.minimumValue))
                self.birthDate = role.riderBirthDate
            }
            isInit = true
        }
        self.updateViews()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func actionSelectBirthDay() {
        DatePickerDialog().show("screen_registration_rider_year_of_birth".localized, doneButtonTitle: "dlg_common_button_ok".localized, cancelButtonTitle: "dlg_common_button_cancel".localized, defaultDate: Date(), minimumDate: nil, maximumDate: Date(), datePickerMode: .date) {[unowned self] (date) in
            self.birthDate = date
            self.updateViews()
        }
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            
            role.riderWeight = Int(self.sliderWeight.value)
            role.riderLength = Int(self.sliderHeight.value)
            role.riderRideFrequencyWeek = Int(self.sliderTimesPerWeek.value)
            role.riderRideFrequencyYear = Int(self.sliderTimesPerYear.value)
            role.riderYearsTotal = Int(self.sliderYearsTotal.value)
            role.riderBirthDate = self.birthDate
            
        }
        
        if isValid() {
            self.nextViewController()
        }
    }
    
    @IBAction func onSlide(sender: AnyObject?) {
        if sender === self.sliderWeight {
            self.labelWeight.text = String(Int(self.sliderWeight.value))
        }
        else if sender === self.sliderHeight {
            self.labelHeight.text = String(Int(self.sliderHeight.value))
        }
        else if sender === self.sliderTimesPerWeek {
            self.labelTimesPerWeek.text = String(Int(self.sliderTimesPerWeek.value))
        }
        else if sender === self.sliderTimesPerYear {
            self.labelTimesPerYear.text = String(Int(self.sliderTimesPerYear.value))
        }
        else if sender === self.sliderYearsTotal {
            self.labelYearsTotal.text = String(Int(self.sliderYearsTotal.value))
        }
    }
    
    private func updateViews() {
        if self.birthDate == nil {
            self.btnBirthdate.localizedTitle = "screen_registration_rider_year_of_birth_hint"
        }
        else {
            self.btnBirthdate.setTitle(ApiDateFormatter.string(from: self.birthDate!, dateStyle: .medium, time: .none), for: .normal)
        }
        
        self.onSlide(sender: self.sliderWeight)
        self.onSlide(sender: self.sliderHeight)
        self.onSlide(sender: self.sliderTimesPerWeek)
        self.onSlide(sender: self.sliderTimesPerYear)
        self.onSlide(sender: self.sliderYearsTotal)
    }
    
    private func isValid() -> Bool {
        if self.birthDate == nil {
            CustomAlertView(okTitle: nil, okMessage: "screen_registration_rider_year_of_birth_hint".localized, okAction: nil).show()
            return false
        }
        return true
    }
}
