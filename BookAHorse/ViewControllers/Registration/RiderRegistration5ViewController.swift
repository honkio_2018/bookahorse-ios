//
//  RiderRegistration5ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration5ViewController: HKWizardViewController {

    @IBOutlet weak var btnLessonType: UIButton!
    @IBOutlet weak var btnTypeOfRiding: UIButton!
    
    private var isInit = false
    
    private var lessonType: String?
    private var typeOfRiding: String?
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            role.riderLessonType = self.lessonType
            role.riderTypeOfRiding = self.typeOfRiding
        }
        
        if isValid() {
            self.wizardNavigationController?.nextViewController()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.lessonType = role.riderLessonType
                self.typeOfRiding = role.riderTypeOfRiding
            }
            isInit = true
        }
        
        self.updateViews()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func actionSelectLessonType() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        if let lessonTypeType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.lessonType.rawValue) as? HKStructureEnumProperty {
            for value in lessonTypeType.values {
                alertController.addAction(UIAlertAction(title: value.name, style: .default) {
                    [unowned self] action in
                    self.lessonType = value.value
                    self.updateViews()
                })
            }
        }
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func actionSelectTypeOfRiding() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        if let typeOfRidingType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.typeOfRiding.rawValue) as? HKStructureEnumProperty {
            for value in typeOfRidingType.values {
                alertController.addAction(UIAlertAction(title: value.name, style: .default) {
                    [unowned self] action in
                    self.typeOfRiding = value.value
                    self.updateViews()
                })
            }
        }
        
        self.present(alertController, animated: true)
    }
    
    private func updateViews() {
        if let lessonTypeType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.lessonType.rawValue) as? HKStructureEnumProperty {
            self.btnTypeOfRiding.isUserInteractionEnabled = true
            if let value = lessonTypeType.find(self.lessonType) {
                self.btnLessonType.setTitle(value.name, for: .normal)
            }
            else {
                self.lessonType = lessonTypeType.values[0].value
                self.btnLessonType.setTitle(lessonTypeType.values[0].name, for: .normal)
            }
        }
        else {
            self.btnLessonType.isUserInteractionEnabled = false
            self.btnLessonType.setTitle("ERROR!", for: .normal)
        }
        if let typeOfRidingType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.typeOfRiding.rawValue) as? HKStructureEnumProperty {
            self.btnTypeOfRiding.isUserInteractionEnabled = true
            if let value = typeOfRidingType.find(self.typeOfRiding) {
                self.btnTypeOfRiding.setTitle(value.name, for: .normal)
            }
            else {
                self.typeOfRiding = typeOfRidingType.values[0].value
                self.btnTypeOfRiding.setTitle(typeOfRidingType.values[0].name, for: .normal)
            }
        }
        else {
            self.btnTypeOfRiding.isUserInteractionEnabled = false
            self.btnTypeOfRiding.setTitle("ERROR!", for: .normal)
        }
    }
    
    private func isValid() -> Bool {
        return true
    }
}
