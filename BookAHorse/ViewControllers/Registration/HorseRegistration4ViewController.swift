//
//  HorseRegistration4ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseRegistration4ViewController: HKBaseTableViewController {
    
    @IBOutlet open var tableView: UITableView!
    @IBOutlet weak var btnLessonType: UIButton!
    @IBOutlet weak var btnTypeOfRiding: UIButton!
    @IBOutlet weak var otherFearsTextView: CustomUITextView!
    
    open var horse: HKAsset? {
        get { return wizardNavigationController?.wizardObject as? HKAsset }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }

    private let horseDetailsTemperamentCellIdentifier = "HorseDetailsTemperamentCell"
    private let checkableItemCell = "CheckableItemCell"
    private let sectionTitleNoDescCell = "SectionTitleNoDescCell"
    private let sectionTitleCell = "SectionTitleCell"
    private let textViewCell = "TextViewCell"
    private let detailsButtonCell = "DetailsButtonCell"
    private let actionButtonCell = "ActionButtonCell"
    
    private var horseAfraid: [String] = []
    private lazy var horseAfraidDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.horseAfraid.contains(property.value) {
                self.horseAfraid.append(property.value)
            }
            else if !isOn {
                if let index = self.horseAfraid.index(of: property.value) {
                    self.horseAfraid.remove(at: index)
                }
            }
        }
    }
    
    private var temperament: Int = 0
    private lazy var temperamentDelegate = { (_ object: AnyObject?, _ value: Float) -> Void in
        self.temperament = Int(value)
    }
    
    private var lessonType: String?
    private var typeOfRiding: String?
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 69
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.horseAfraid = self.horse?.horseAfraid ?? []
        self.temperament = self.horse?.horseTemperament ?? 0
        
        self.buildItemsSource()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    private func buildItemsSource() {
        self.itemsSource.removeAll()
        let sectionTitleBinder = SectionTitleCell.buildBinder()
        let section = HKTableViewSection()
        
        // Temperament of the Horse
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_temperament, desc: ""), sectionTitleBinder))
        
        section.items.append((horseDetailsTemperamentCellIdentifier, self.horse, { (viewController, cell, item) in
            let itemCell = cell as! HorseDetailsTemperamentCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration4ViewController
            
            let horseTemperament = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseTemperament.rawValue] as! HKStructureNumericProperty
            
            itemCell.temperametSlider.minimumValue = Float(horseTemperament.min)
            itemCell.temperametSlider.maximumValue = Float(horseTemperament.max)
            if let horseTemperament = horse.horseTemperament {
                itemCell.temperametSlider.value = Float(horseTemperament)
            } else {
                itemCell.temperametSlider.value = itemCell.temperametSlider.minimumValue
            }
            itemCell.slideDelegate = parent.temperamentDelegate
            itemCell.nameLabel.isHidden = true
        }))
        
        // What is the horse afraid of
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_afraid, desc: ""), sectionTitleBinder))
        
        let horseAfraidProperty = (AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseAfraid.rawValue] as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        let binder = buildHorseAfraidBinder()
        for value in horseAfraidProperty.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        // Other fears
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_other_fear, desc: ""), sectionTitleBinder))
        
        section.items.append((textViewCell, self.horse, { (viewController, cell, item) in
            let textCell = cell as! TextViewCell
            let horse = item as! HKAsset
            
            if let horseOtherFear = horse.horseOtherFear {
                textCell.textView.text = horseOtherFear
            }
            textCell.textView.localizedPlaceholder = Str.screen_registration_horse_other_fear_hint
            textCell.textView.delegate = viewController as! HorseRegistration4ViewController
        }))
        
        // Following lessons offered
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_lesson_type, desc: ""), sectionTitleBinder))
        
        section.items.append((actionButtonCell, self.horse, {(viewController, cell, item) in
            let buttonCell = cell as! ActionButtonCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration4ViewController
            
            if let lessonType = horse.lessonType {
                let lessonTypeName = AppApi.getHorseStructure()?
                    .findEnumPropertyValue(HKAsset.Fields.lessonType.rawValue, value: lessonType)?
                    .name ?? "-"
                buttonCell.actionButton.setTitle(lessonTypeName, for: .normal)
                self.lessonType = lessonType
            } else {
                let enumProperty = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.lessonType.rawValue] as! HKStructureEnumProperty
                let value = enumProperty.values.first
                buttonCell.actionButton.setTitle(value?.name, for: .normal)
                self.lessonType = value?.value
            }

            buttonCell.actionButton.addTarget(parent, action: #selector(parent.actionSelectLessonType), for: .touchDown)
            parent.btnLessonType = buttonCell.actionButton
        }))
        
        // Indoor and/or outdoor riding offered
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_type_of_riding, desc: ""), sectionTitleBinder))
        
        section.items.append((actionButtonCell, self.horse, {(viewController, cell, item) in
            let buttonCell = cell as! ActionButtonCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration4ViewController
            
            if let typeOfRiding = horse.typeOfRiding {
                let typeOfRidingName = AppApi.getHorseStructure()?
                    .findEnumPropertyValue(HKAsset.Fields.typeOfRiding.rawValue, value: typeOfRiding)?
                    .name ?? "-"
                buttonCell.actionButton.setTitle(typeOfRidingName, for: .normal)
                self.typeOfRiding = typeOfRiding
            } else {
                let enumProperty = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.typeOfRiding.rawValue] as! HKStructureEnumProperty
                let value = enumProperty.values.first
                buttonCell.actionButton.setTitle(value?.name, for: .normal)
                self.typeOfRiding = value?.value
            }
            buttonCell.actionButton.contentHorizontalAlignment = .left
            buttonCell.actionButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            buttonCell.actionButton.addTarget(parent, action: #selector(self.actionSelectTypeOfRiding), for: .touchDown)
            self.btnTypeOfRiding = buttonCell.actionButton
        }))
        
        self.itemsSource.append(section)
    }
    
    @IBAction func actionSelectLessonType() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        if let lessonTypeType = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.lessonType.rawValue] as? HKStructureEnumProperty {
            for value in lessonTypeType.values {
                alertController.addAction(UIAlertAction(title: value.name, style: .default) {
                    [unowned self] action in
                    self.lessonType = value.value
                    self.updateLessonTypeViews()
                })
            }
        }
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func actionSelectTypeOfRiding() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))

        if let typeOfRidingType = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.typeOfRiding.rawValue] as? HKStructureEnumProperty {
            for value in typeOfRidingType.values {
                alertController.addAction(UIAlertAction(title: value.name, style: .default) {
                    [unowned self] action in
                    self.typeOfRiding = value.value
                    self.updateTypeOfRidingViews()
                })
            }
        }

        self.present(alertController, animated: true)
    }
    
    private func updateLessonTypeViews() {
        if let lessonTypeType = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.lessonType.rawValue] as? HKStructureEnumProperty {
            self.btnTypeOfRiding.isUserInteractionEnabled = true
            if let value = lessonTypeType.find(self.lessonType) {
                self.btnLessonType.setTitle(value.name, for: .normal)
                self.lessonType = value.value
            }
            else {
                self.lessonType = lessonTypeType.values[0].value
                self.btnLessonType.setTitle(lessonTypeType.values[0].name, for: .normal)
            }
        }
        else {
            self.btnLessonType.isUserInteractionEnabled = false
            self.btnLessonType.setTitle("ERROR!", for: .normal)
        }
    }
    
    private func updateTypeOfRidingViews() {
        if let typeOfRidingType = AppApi.getHorseStructure()?.properties?[UserRole.Fields.typeOfRiding.rawValue] as? HKStructureEnumProperty {
            self.btnTypeOfRiding.isUserInteractionEnabled = true
            if let value = typeOfRidingType.find(self.typeOfRiding) {
                self.btnTypeOfRiding.setTitle(value.name, for: .normal)
                self.typeOfRiding = value.value
            }
            else {
                self.typeOfRiding = typeOfRidingType.values[0].value
                self.btnTypeOfRiding.setTitle(typeOfRidingType.values[0].name, for: .normal)
            }
        }
        else {
            self.btnTypeOfRiding.isUserInteractionEnabled = false
            self.btnTypeOfRiding.setTitle("ERROR!", for: .normal)
        }
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let horse = horse {
            horse.horseTemperament = Int(self.temperament)
            horse.horseAfraid = self.horseAfraid
            if let lessonType = self.lessonType, horse.lessonType != lessonType {
                horse.lessonType = lessonType
            }
            
            if let typeOfRiding = self.typeOfRiding,  horse.typeOfRiding != typeOfRiding {
                horse.typeOfRiding = self.typeOfRiding
            }
            
            if let otherFears = self.otherFearsTextView, horse.horseOtherFear != otherFears.text {
                horse.horseOtherFear = otherFears.text
            }
        }
        
        if isValid() {
            self.wizardNavigationController?.nextViewController()
        }
    }
    
    private func buildHorseAfraidBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! HorseRegistration4ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.horseAfraid.contains(property.value)
            checkableCell.switchDelegate = parent.horseAfraidDelegate
        }
    }
    
    private func isValid() -> Bool {
        return true
    }
    
    // MARK: - UITextViewDelegate protocol
    func textViewDidChange(_ textView: UITextView) {
        horse?.horseOtherFear = textView.text
    }
}
