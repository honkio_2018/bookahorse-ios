//
//  HorseRegistration2ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

class HorseRegistration2ViewController: HKWizardViewController {
    
    @IBOutlet weak var addresView: AddressView!

    private var isInit: Bool = false
    
    override func viewDidLoad() {
        self.addresView.parentViewController = self
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let horse = self.wizardNavigationController?.wizardObject as? HKAsset {
                self.addresView.setAddress(horse.address)
            }
            isInit = true
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        BusyIndicator.shared.showProgressView(self.view)
        self.addresView.syncAddress { [unowned self] in
            BusyIndicator.shared.hideProgressView()
            
            if let horse = self.wizardNavigationController?.wizardObject as? HKAsset {
                horse.address = self.addresView.getAddress()
                if let location = self.addresView.location {
                    horse.longitude = location.coordinate.longitude
                    horse.latitude = location.coordinate.latitude
                }
            }
            
            if self.isValid() {
                self.nextViewController()
            }
        }
    }
    
    private func isValid() -> Bool {
        return self.addresView.isValid()
    }
    
}
