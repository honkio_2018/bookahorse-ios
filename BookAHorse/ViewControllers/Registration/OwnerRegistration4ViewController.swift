//
//  OwnerRegistration4ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/31/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OwnerRegistration4ViewController: HKWizardViewController {
    
    @IBOutlet weak var textBankIban: UITextField!
    @IBOutlet weak var textBankSwift: UITextField!
    @IBOutlet weak var textVat: UITextField!
    @IBOutlet weak var textVatNumber: UITextField!
    
    private var isInit = false
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    private let validatorVat = HKTextValidatorSet(validators: [
            HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty()),
            HKTextValidator(errorMessage: "text_validator_invalid_amount".localized, rool: HKTextValidator.ruleDoubleNumber())
        ])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.textBankIban.text = role.ownerBankIban
                self.textBankSwift.text = role.ownerBankSwift
                self.textVat.text = role.ownerVat != nil ? String(role.ownerVat!) : ""
                self.textVatNumber.text = role.ownerVatNumber
            }
            isInit = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            
            role.ownerBankIban = self.textBankIban.text
            role.ownerBankSwift = self.textBankSwift.text
            role.ownerVat = Double(self.textVat.text ?? "0")
            role.ownerVatNumber = self.textVatNumber.text
            
        }
        
        if isValid() {
            dismissKeyboard()
            self.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        var isValid = true
        
        isValid = validatorNotEmpty.validate(self.textBankIban) && isValid
        
        return isValid
    }
}
