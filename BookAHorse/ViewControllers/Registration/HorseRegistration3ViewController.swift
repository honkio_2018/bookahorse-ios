//
//  HorseRegistration3ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

class HorseRegistration3ViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var horseDescriptionTextView: CustomUITextView!
    @IBOutlet weak var insuredSwitch: UISwitch!
    @IBOutlet weak var weightPicker: UIPickerView!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var agePicker: UIPickerView!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var heightPicker: UIPickerView!
    @IBOutlet weak var heightLabel: UILabel!
    
    enum rowIndexes: Int {
        case DescriptionRow
        case InsuredRow
        case MinMaxHeader
        case WeightRow
        case WeightPickerRow
        case AgeRow
        case AgePickerRow
        case HeightRow
        case HeightPickerRow

        static let count: Int = {
            var max: Int = 0
            while let _ = rowIndexes(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(17)),
                      NSAttributedStringKey.foregroundColor: UIColor(netHex: AppColors.appOrange)]
    
    let minMaxTextAttributes: [NSAttributedStringKey: Any] = [
        .foregroundColor : UIColor(netHex: AppColors.appOrange),
        .font : UIFont.boldSystemFont(ofSize: 17)
    ]

    
    open var horse: HKAsset? {
        get { return wizardNavigationController?.wizardObject as? HKAsset }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }
    
    private var isInit = false
    
    let horseStructure = AppApi.getHorseStructure()
    
    private var horseRiderWeight: [Int]!
    private var horseRiderAge: [Int]!
    private var horseRiderHeight: [Int]!
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized,
                                                  rool: HKTextValidator.ruleNotEmpty())
    
    private var horseRiderWeightMin: Int {
        // Weight Min
        if let horseRiderWeightMin = horseStructure?.properties?[HKAsset.Fields.horseRiderWeightMin.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderWeightMin.min)
        }
        return 0
    }
    
    private var horseRiderWeightMax: Int {
        // Weight Max
        if let horseRiderWeightMax = horseStructure?.properties?[HKAsset.Fields.horseRiderWeightMax.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderWeightMax.max)
        }
        return 0
    }
    
    private var horseRiderAgeMin: Int {
        // Age Min
        if let horseRiderAgeMin = horseStructure?.properties?[HKAsset.Fields.horseRiderAgeMin.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderAgeMin.min)
        }
        return 0
    }
    
    private var horseRiderAgeMax: Int {
        // Age Max
        if let horseRiderAgeMax = horseStructure?.properties?[HKAsset.Fields.horseRiderAgeMax.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderAgeMax.max)
        }
        return 0
    }
    
    private var horseRiderHeightMin: Int {
        // Height Min
        if let horseRiderHeightMin = horseStructure?.properties?[HKAsset.Fields.horseRiderLengthMin.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderHeightMin.min)
        }
        return 0
    }
    
    private var horseRiderHeightMax: Int {
        // Height Max
        if let horseRiderHeightMax = horseStructure?.properties?[HKAsset.Fields.horseRiderLengthMax.rawValue] as? HKStructureNumericProperty {
            return Int(horseRiderHeightMax.max)
        }
        return 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.tableView.tableFooterView = UIView()
        
        weightPicker.showsSelectionIndicator = true
        agePicker.showsSelectionIndicator = true
        heightPicker.showsSelectionIndicator = true
        
        self.horseRiderWeight = [Int](horseRiderWeightMin...horseRiderWeightMax)
        self.horseRiderAge = [Int](horseRiderAgeMin...horseRiderAgeMax)
        self.horseRiderHeight = [Int](horseRiderHeightMin...horseRiderHeightMax)
        
        let pickers = [weightPicker, agePicker, heightPicker]
        for picker in pickers {
            if let picker = picker {
                let minLabel = UILabel()
                minLabel.text = "screen_registration_owner_company_title_min".localized
//                minLabel.textColor = UIColor(netHex: AppColors.appOrange)
                minLabel.textAlignment = .center
                let maxLabel = UILabel()
                maxLabel.text = "screen_registration_owner_company_title_max".localized
//                maxLabel.textColor = UIColor(netHex: AppColors.appOrange)
                maxLabel.textAlignment = .center

                picker.setPickerLabels(labels: [0: minLabel, 1: maxLabel], containedView: picker.superview!)
            }
        }
    }
    
    private func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HKBaseViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let horse = self.horse {
                // weight min (row index)
                let weightRowMin = horse.horseRiderWeightMin == nil ? 0 : horse.horseRiderWeightMin! - horseRiderWeightMin
                weightPicker.selectRow(weightRowMin, inComponent: 0, animated: false)
                
                // weight max (row index)
                let weightRowMax = horse.horseRiderWeightMax == nil ? horseRiderWeightMax - horseRiderWeightMin : horse.horseRiderWeightMax! - horseRiderWeightMin
                weightPicker.selectRow(weightRowMax, inComponent: 1, animated: false)
                
                weightLabel.attributedText = formatMinMaxValue(horseRiderWeight[weightRowMin], horseRiderWeight[weightRowMax])

                // age min (row index)
                let ageRowMin = horse.horseRiderAgeMin == nil ? 0 : horse.horseRiderAgeMin! - horseRiderAgeMin
                agePicker.selectRow(ageRowMin, inComponent: 0, animated: false)
                
                // age max (row index)
                let ageRowMax = horse.horseRiderAgeMax == nil ? horseRiderAgeMax - horseRiderAgeMin : horse.horseRiderAgeMax! - horseRiderAgeMin
                agePicker.selectRow(ageRowMax, inComponent: 1, animated: false)
                
                ageLabel.attributedText = formatMinMaxValue(horseRiderAge[ageRowMin], horseRiderAge[ageRowMax])
                
                // height min (row index)
                let heightRowMin = horse.horseRiderLengthMin == nil ? 0 : horse.horseRiderLengthMin! - horseRiderHeightMin
                heightPicker.selectRow(heightRowMin, inComponent: 0, animated: false)
                
                // age max (row index)
                let heightRowMax = horse.horseRiderLengthMax == nil ? Int(abs(horseRiderHeightMax - horseRiderHeightMin)) : Int(UInt((horse.horseRiderLengthMax! - Int(horseRiderHeightMin)).magnitude))
                heightPicker.selectRow(heightRowMax, inComponent: 1, animated: false)
                
                heightLabel.attributedText = formatMinMaxValue(horseRiderHeight[heightRowMin], horseRiderHeight[heightRowMax])

                horseDescriptionTextView.text = horse.horseDescription ?? ""
                insuredSwitch.isOn = horse.horseInsured ?? false
            }
            else {
                weightPicker.selectRow(0, inComponent: 0, animated: false)
                weightPicker.selectRow(horseRiderWeightMax - horseRiderWeightMin, inComponent: 1, animated: false)
                agePicker.selectRow(0, inComponent: 0, animated: false)
                agePicker.selectRow(horseRiderAgeMax - horseRiderAgeMin, inComponent: 1, animated: false)
                heightPicker.selectRow(0, inComponent: 0, animated: false)
                heightPicker.selectRow(horseRiderHeightMax - horseRiderHeightMin, inComponent: 1, animated: false)
            }
            
            isInit = true
        }
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if let horse = horse {
            horse.horseInsured = insuredSwitch.isOn
            horse.horseDescription = horseDescriptionTextView.text
            // Minimum is initial value, selected row is offset (index) from initial value
            horse.horseRiderWeightMin = horseRiderWeight[weightPicker.selectedRow(inComponent: 0)]
            horse.horseRiderWeightMax = horseRiderWeight[weightPicker.selectedRow(inComponent: 1)]
            horse.horseRiderAgeMin = horseRiderAge[agePicker.selectedRow(inComponent: 0)]
            horse.horseRiderAgeMax = horseRiderAge[agePicker.selectedRow(inComponent: 1)]
            horse.horseRiderLengthMin = horseRiderHeight[heightPicker.selectedRow(inComponent: 0)]
            horse.horseRiderLengthMax = horseRiderHeight[heightPicker.selectedRow(inComponent: 1)]
        }
        
        if isValid() {
            self.wizardNavigationController?.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        guard let horseDescription = horseDescriptionTextView.text,
            !horseDescription.trimmingCharacters(in: .whitespaces).isEmpty else {
            CustomAlertView(okTitle: Str.screen_registration_horse_description,
                            okMessage: Str.screen_registration_horse_description_hint,
                            okAction: nil).show()
            return false
        }

        return true
    }
    
    func formatMinMaxValue(_ min: Int, _ max: Int) -> NSAttributedString {
        let resultString = NSMutableAttributedString(string: String(format: "%4u - %4u ", min, max), attributes: minMaxTextAttributes)
        
        return resultString
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowIndexes.count
    }
    
    func getPickerRowHeight(_ picker: UIPickerView) -> CGFloat {
        return picker.isHidden ? 0.0 : 216.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == rowIndexes.WeightPickerRow.rawValue {
            return getPickerRowHeight(weightPicker)
        } else if indexPath.row == rowIndexes.AgePickerRow.rawValue {
            return getPickerRowHeight(agePicker)
        } else if indexPath.row == rowIndexes.HeightPickerRow.rawValue {
            return getPickerRowHeight(heightPicker)
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == rowIndexes.WeightRow.rawValue {
            weightPicker.isHidden = !weightPicker.isHidden
            if weightPicker.isHidden {
                if weightLabel.text!.isEmpty {
                    let min = weightPicker.selectedRow(inComponent: 0)
                    let max = weightPicker.selectedRow(inComponent: 1)
                    weightLabel.attributedText = formatMinMaxValue(horseRiderWeight[min], horseRiderWeight[max])
                }
            } else {
                agePicker.isHidden = true
                heightPicker.isHidden = true
            }
        } else if indexPath.row == rowIndexes.AgeRow.rawValue {
            agePicker.isHidden = !agePicker.isHidden
            if agePicker.isHidden {
                if ageLabel.text!.isEmpty {
                    let min = agePicker.selectedRow(inComponent: 0)
                    let max = agePicker.selectedRow(inComponent: 1)
                    ageLabel.attributedText = formatMinMaxValue(horseRiderAge[min], horseRiderHeight[max])
                }
            } else {
                weightPicker.isHidden = true
                heightPicker.isHidden = true
            }
        } else if indexPath.row == rowIndexes.HeightRow.rawValue {
            heightPicker.isHidden = !heightPicker.isHidden
            if heightPicker.isHidden {
                if heightLabel.text!.isEmpty {
                    let min = heightPicker.selectedRow(inComponent: 0)
                    let max = heightPicker.selectedRow(inComponent: 1)
                    heightLabel.attributedText = formatMinMaxValue(horseRiderHeight[min], horseRiderHeight[max])
                }
            } else {
                weightPicker.isHidden = true
                agePicker.isHidden = true
            }
        }
        
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tableView.beginUpdates()
            // apple bug fix - some TV lines hide after animation
            self.tableView.deselectRow(at:indexPath, animated: true)
            self.tableView.endUpdates()
//        })
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView === weightPicker {
            return horseRiderWeight.count
        } else if pickerView === agePicker {
            return horseRiderAge.count
        } else if pickerView === heightPicker {
            return horseRiderHeight.count
        }
        
        return 0
    }
    
    // MARK: - UIPickerViewDataSource
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var min, max: Int
        
        if component == 0 {
            min = row
            max = pickerView.selectedRow(inComponent: 1)
        } else {
            min = pickerView.selectedRow(inComponent: 0)
            max = row
        }
        
        if min > max {
            pickerView.selectRow(min, inComponent: 1, animated: true)
            max = min
        }
        
        if pickerView === weightPicker {
            weightLabel.attributedText = formatMinMaxValue(horseRiderWeight[min], horseRiderWeight[max])
        } else if pickerView === agePicker {
            ageLabel.attributedText = formatMinMaxValue(horseRiderAge[min], horseRiderAge[max])
        } else if pickerView === heightPicker {
            heightLabel.attributedText = formatMinMaxValue(horseRiderHeight[min], horseRiderHeight[max])
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var value: Int = 0
        if pickerView === weightPicker {
            value = horseRiderWeight[row]
        } else if pickerView === agePicker {
            value = horseRiderAge[row]
        } else if pickerView === heightPicker {
            value = horseRiderHeight[row]
        }

        let rowTitle = NSAttributedString(string: "\(value)", attributes: attributes)
        return rowTitle
    }
}
