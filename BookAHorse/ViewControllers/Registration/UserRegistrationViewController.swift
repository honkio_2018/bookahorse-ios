//
//  UserRegistrationViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/4/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class UserRegistrationViewController: HKBaseViewController {

    @IBOutlet weak var horseOwnerButton: UIButton!
    @IBOutlet weak var riderButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        horseOwnerButton.layer.cornerRadius = 5
        riderButton.layer.cornerRadius = 5
    }
    
    @IBAction func actionSettings(_ sender:UIBarButtonItem!) {
        if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}
