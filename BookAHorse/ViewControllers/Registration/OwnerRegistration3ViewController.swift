//
//  OwnerRegistration2ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/31/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OwnerRegistration3ViewController: HKBaseTableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet open var tableView: UITableView!
    
    open var userRole: UserRole? {
        get { return wizardNavigationController?.wizardObject as? UserRole }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }
    
    private let checkableItemCell = "CheckableItemCell"
    private let sectionTitleCell = "SectionTitleCell"
    
    private let ownCompetenceLevelType = AppApi.getOwnerStructure()?.getProperty(UserRole.Fields.ownCompetenceLevel.rawValue) as? HKStructureEnumProperty
    private var ownCompetenceLevel: String? = nil
    private lazy var ownCompetenceLevelDelegate = {[unowned self] (_ cell: OwnerOwnCompetenceLevelCell, _ object: AnyObject?) -> Void in
        let alert = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        let pickerView = UIPickerView(frame: CGRect.init(x: 5, y: 20, width: UIScreen.main.bounds.width - 37, height: 180))
        pickerView.dataSource = self
        pickerView.delegate = self
        if self.ownCompetenceLevel != nil {
            pickerView.selectRow(self.ownCompetenceLevelType?.values?.index{$0.value == self.ownCompetenceLevel} ?? 0, inComponent: 0, animated: true)
        }
        alert.view.addSubview(pickerView)
        
        weak var weakButton = cell.labelButton
        let actionOk = UIAlertAction(title: "dlg_common_button_ok".localized, style: .cancel) { [unowned self] (action) in
            let selectedItem = self.ownCompetenceLevelType?.values[pickerView.selectedRow(inComponent: 0)]
            
            self.ownCompetenceLevel = selectedItem?.value
            weakButton?.text = selectedItem?.name
        }
        alert.addAction(actionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private var competenceLevel: [String] = []
    private lazy var competenceLevelDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.competenceLevel.contains(property.value) {
                self.competenceLevel.append(property.value)
            }
            else if !isOn {
                if let index = self.competenceLevel.index(of: property.value) {
                    self.competenceLevel.remove(at: index)
                }
            }
        }
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            role.ownerOwnCompetenceLevel = self.ownCompetenceLevel
            role.ownerCompetenceLevelGroups = self.competenceLevel
        }
        
        if isValid() {
            self.wizardNavigationController?.nextViewController()
        }
    }
    
    override func viewDidLoad() {
        self.ownCompetenceLevel = self.userRole?.ownerOwnCompetenceLevel
        self.competenceLevel = self.userRole?.riderCompetenceLevel ?? self.competenceLevel
        
        self.buildItemsSource()
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 69
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ownCompetenceLevelType?.values.count ?? 0
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ownCompetenceLevelType?.values[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as? UILabel
        
        if label == nil {
            label = UILabel()
            label?.font = UIFont.systemFont(ofSize: 20)
            label?.numberOfLines = 3
            label?.textAlignment = .center
            label?.lineBreakMode = .byWordWrapping
        }
        
        label?.text = self.pickerView(pickerView, titleForRow: row, forComponent: component)
        
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 90
    }
    
    private func buildItemsSource() {
        
        self.itemsSource.removeAll()
        let ownerStructure = AppApi.getOwnerStructure()!
        
        var section = HKTableViewSection()
        section.items.append(("OwnerOwnCompetenceLevelCell", nil, buildOwnCompetenceLevelBinder()))
        
        self.itemsSource.append(section)
        
        // competeceLevel
        section = HKTableViewSection()
        section.items.append((self.sectionTitleCell, (title: "screen_registration_owner_tuition_title".localized, desc: nil as String?), SectionTitleCell.buildBinder()))

        let competenceLevelType = (ownerStructure.getProperty(UserRole.Fields.competenceLevel.rawValue) as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty

        let binder = buildCompetenceLevelBinder()
        for value in competenceLevelType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }

        self.itemsSource.append(section)
    }
    
    private func buildOwnCompetenceLevelBinder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let competenceLevelCell = cell as! OwnerOwnCompetenceLevelCell
            let parent = viewController as! OwnerRegistration3ViewController
            
            let structure = AppApi.getOwnerStructure()?.getProperty(UserRole.Fields.ownCompetenceLevel.rawValue) as? HKStructureEnumProperty
            if let value = structure?.find(parent.ownCompetenceLevel) {
                competenceLevelCell.labelButton.text = value.name
            } else {
                if let value = structure?.values[0] {
                    parent.ownCompetenceLevel = value.value
                    competenceLevelCell.labelButton.text = value.name
                }
            }
            competenceLevelCell.buttonDelegate = parent.ownCompetenceLevelDelegate
        }
    }

    private func buildCompetenceLevelBinder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! OwnerRegistration3ViewController

            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.competenceLevel.contains(property.value)
            checkableCell.switchDelegate = parent.competenceLevelDelegate
        }
    }
    
    private func isValid() -> Bool {
        return true
    }
}
