//
//  RiderRegistration7ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration7ViewController: HKWizardViewController {

    @IBAction func nextAction(_ sender: AnyObject) {
        self.nextViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
}
