//
//  OwnerRegistrationViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/24/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OwnerRegistrationViewController: HKWizardNavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var role: UserRole?
        
        if let originalRole = AppApi.ownerRole() {
            role = UserRole(from: originalRole)
        }
        else {
            role = UserRole()
        }
        role!.ownerCompanyName = "\(HonkioApi.activeUser()?.firstname ?? "") \(HonkioApi.activeUser()?.lastname ?? "")"
        role!.email = HonkioApi.activeUser()?.login
        role!.phone = HonkioApi.activeUser()?.phone
        
        if let merchant = AppApi.getOwnerMerchant() {
            
            role!.ownerCompanyId =  merchant.businessId
            role!.ownerCompanyName =  merchant.name
            
            role!.ownerBankIban =  merchant.bankIban
            role!.ownerBankAccount =  merchant.bankName // TODO fix missunderstanding
            role!.ownerBankSwift =  merchant.bankSwift
            role!.ownerVat = merchant.vat
            role!.ownerVatNumber =  merchant.vatNumber
            
            role!.email =  merchant.supportEmail
            role!.phone =  merchant.supportPhone
        }
        
        if let shop = AppApi.getOwnerShop() {
            role!.ownerCompanyName = shop.getProperty(MERCHANT_SHOP_FIELD_NAME) as? String ?? role!.ownerCompanyName
            role!.presentation = shop.getProperty(MERCHANT_SHOP_FIELD_DESCRIPTION) as? String
            role!.ownerOwnCompetenceLevel = shop.getProperty(UserRole.Fields.strOwnCompetenceLevel.rawValue) as? String
            role!.ownerCompetenceLevelGroups = shop.getProperty(UserRole.Fields.listCompetenceLevel.rawValue) as? [String]
            role!.ownerAddress = shop.getProperty(MERCHANT_SHOP_FIELD_ADDRESS) as? String
        }
        
        self.wizardObject = role
        
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "OwnerRegistration1ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "OwnerRegistration3ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "OwnerRegistration4ViewController"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
//        viewController.title = "\(self.viewControllers.count + 1)/\(self.wizardViewControllers.count)"
        viewController.title = Str.screen_registration_owner_title
        super.pushViewController(viewController, animated: animated)
    }
    
    override func wizardComplete() {
        BusyIndicator.shared.showProgressView(self.view)
        
        if AppApi.ownerRole() == nil {
            let role = UserRole()
            role.userId = HonkioApi.activeUser()?.userId
            role.roleId = AppApi.ROLE_OWNER
            
            HonkioApi.userSetRole(role, flags: 0) {[weak self] (response) in
                if response.isStatusAccept() {
                    AppApi.onRoleGet(role: response.result as! UserRole)
                    self?.registerMerchantIfNeeded()
                }
                else {
                    self?.handleError(response.anyError())
                }
            }
        }
        else {
            self.registerMerchantIfNeeded()
        }
    }
    
    private func registerMerchantIfNeeded() {
        if let merchant = AppApi.getOwnerMerchant() {
            self.saveMerchant(merchant)
        }
        else {
            let role = self.wizardObject as! UserRole
            HonkioMerchantApi.userMerchantCreate(nil, name: role.ownerCompanyName, email: role.email, phone: role.phone, flags: 0) {[weak self] (response) in
                if response.isStatusAccept() {
                    let merchant = response.result as! Merchant
                    
                    HonkioApi.userAccessList(merchant.merchantId,
                                             flags: MSG_FLAG_NO_WARNING,
                                             handler: nil)
                    
                    self?.saveMerchantIntoUser(merchant)
                }
                else {
                    self?.handleError(response.anyError())
                }
            }
        }
    }
    
    private func saveMerchantIntoUser(_ merchant: Merchant) {
        let role = AppApi.ownerRole()!
        role.ownerMerchantId = merchant.merchantId
        
        HonkioApi.userSetRole(role, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                self?.saveMerchant(merchant)
            }
            else {
                self?.handleError(response.anyError())
            }
        }
    }
    
    private func saveMerchant(_ merchant: Merchant) {
        let role = self.wizardObject as! UserRole
        merchant.businessId = role.ownerCompanyId
        merchant.name = role.ownerCompanyName
        
        merchant.bankIban = role.ownerBankIban
        merchant.bankName = role.ownerBankAccount // TODO fix missunderstanding
        merchant.bankSwift = role.ownerBankSwift
        merchant.vat = role.ownerVat ?? 0.0
        merchant.vatNumber = role.ownerVatNumber
        
        merchant.supportEmail = role.email
        merchant.supportPhone = role.phone
        
        HonkioMerchantApi.merchantSet(merchant, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                AppApi.apiInstance.onMerchantGet(merchant: response.result as! Merchant)
                self?.loadShopIdentityIfNeeded()
            }
            else {
                self?.handleError(response.anyError())
            }
        }
    }
    
    private func loadShopIdentityIfNeeded() {
        if let shop = AppApi.getOwnerShop() {
            saveShop(shop)
        }
        else {
            let filter = ShopFilter()
            filter.merchantId = AppApi.ownerRole()?.ownerMerchantId
            HonkioMerchantApi.merchantShopsList(filter, flags: 0) {[weak self] (response) in
                if response.isStatusAccept() {
                    let list = (response.result as! MerchantShopsList).list

                    if list?.count ?? 0 > 0 {
                        self?.saveShopToRole(list![0])
                    }
                    else {
                        self?.saveShopToRole(MerchantShop(from: HonkioApi.mainShopIdentity()))
                    }
                }
                else {
                    self?.handleError(response.anyError())
                }
            }
        }
    }
    
    private func saveShopToRole(_ shop: MerchantShop) {
        let role = AppApi.ownerRole()!
        role.ownerShopId = shop.identityId
        AppApi.apiInstance.onShopGet(shop: shop)
        
        HonkioApi.userSetRole(role, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                AppApi.onRoleGet(role: response.result as! UserRole)
                self?.saveShop(shop)
            }
            else {
                self?.handleError(response.anyError())
            }
        }
    }
    
    private func saveShop(_ shop: MerchantShop) {
        if shop.settings == nil {
            shop.settings = [:]
        }
        
        shop.settings[MERCHANT_SHOP_FIELD_SERVICE_TYPE] = MERCHANT_SHOP_SERVICE_TYPE_ECOMMERCE

        if let role = self.wizardObject as? UserRole {
            shop.settings[MERCHANT_SHOP_FIELD_NAME] = role.ownerCompanyName
            shop.settings[MERCHANT_SHOP_FIELD_DESCRIPTION] = role.presentation
            shop.settings[UserRole.Fields.strOwnCompetenceLevel.rawValue] = role.ownerOwnCompetenceLevel
            shop.settings[UserRole.Fields.listCompetenceLevel.rawValue] = role.ownerCompetenceLevelGroups
            shop.settings[MERCHANT_SHOP_FIELD_ADDRESS] = role.ownerAddress
        }
        
        shop.setGeneralFields([
            MERCHANT_SHOP_FIELD_NAME, MERCHANT_SHOP_FIELD_ADDRESS, MERCHANT_SHOP_FIELD_LOGO_LARGE, MERCHANT_SHOP_FIELD_LOGO_SMALL,
            MERCHANT_SHOP_FIELD_MAX_DISTANCE, MERCHANT_SHOP_FIELD_SHOP_TYPE, MERCHANT_SHOP_FIELD_SERVICE_TYPE, MERCHANT_SHOP_FIELD_SHOP_MODE,
            MERCHANT_SHOP_FIELD_ACTIVE, MERCHANT_SHOP_FIELD_DAY_BREAK, MERCHANT_SHOP_FIELD_EMAIL, MERCHANT_SHOP_FIELD_LATITUDE, MERCHANT_SHOP_FIELD_LONGITUDE,
            MERCHANT_SHOP_FIELD_TAG_LIST, MERCHANT_SHOP_FIELD_DESCRIPTION, UserRole.Fields.strOwnCompetenceLevel.rawValue,
            UserRole.Fields.listCompetenceLevel.rawValue
        ])
        
        HonkioMerchantApi.merchantSetShop(shop, flags:0) {[weak self] (response) in
            if response.isStatusAccept() {
                AppApi.apiInstance.onShopGet(shop: (response.result as! MerchantShopInfo).shop)
                self?.completeRegistration()
            }
            else {
                self?.handleError(response.anyError())
            }
        }
    }
    
    private func completeRegistration() {
        BusyIndicator.shared.hideProgressView()
        
        AppSettings.theme = ROLE_OWNER
        
        let role = AppApi.ownerRole()!
        role.ownerRegistered = true
        HonkioApi.userSetRole(role, flags: MSG_FLAG_NO_WARNING | MSG_FLAG_LOW_PRIORITY, handler: nil)
        
        if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
            
            let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
            appDelegate.changeRootViewController(viewController)
        }
    }
    
    private func handleError(_ error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
        
        BusyIndicator.shared.hideProgressView()
    }
}
