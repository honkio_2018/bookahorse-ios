//
//  RiderRegistration6ViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration6ViewController: HKWizardViewController {

    @IBOutlet weak var switchStableOwner: UISwitch!

    @IBOutlet weak var textPresentation: CustomUITextView!
    @IBOutlet weak var textRefName: UITextField!
    @IBOutlet weak var textRefPhone: UITextField!
    @IBOutlet weak var textParentEmail: UITextField!
    
    @IBOutlet weak var horsesNumberField: UITextField!
    @IBOutlet weak var yearsOwnerHorseField: UITextField!
    
    @IBOutlet weak var ReferenceView: UIView!
    
    
    open var userRole: UserRole? {
        get { return wizardNavigationController?.wizardObject as? UserRole }
    }
    
    private var numberOfHorsesRange: (min: Float, max: Float) {
        if let horsesNumberType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.numberOfHorses.rawValue) as? HKStructureNumericProperty {
            return (min: Float(horsesNumberType.min), max: Float(horsesNumberType.max))
        }
        return (min: 0.0, max: 0.0)
    }
    
    private var yearsOwnedHourseTypeRange: (min: Float, max: Float) {
        if let yearsOwnedHourseType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.yearsOwnedHourse.rawValue) as? HKStructureNumericProperty {
            return (min: Float(yearsOwnedHourseType.min), max: Float(yearsOwnedHourseType.max))
        }
        return (min: 0.0, max: 0.0)
    }
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized,
                                                  rool: HKTextValidator.ruleNotEmpty())
    private let validatorInvalidEmail = HKTextValidator(errorMessage: "text_validator_invalid_email".localized,
                                                  rool: HKTextValidator.ruleEmail())
    private let validatorInvalidPhone = HKTextValidator(errorMessage: "text_validator_invalid_phone".localized,
                                                      rool: HKTextValidator.rulePhoneNumber())
    
    private var isInit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.horsesNumberField.placeholder = String(format: "%.0f-%0.f", numberOfHorsesRange.min, numberOfHorsesRange.max)
        if yearsOwnedHourseTypeRange.min != yearsOwnedHourseTypeRange.max {
            self.yearsOwnerHorseField.placeholder = String(format: "%.0f-%.0f", yearsOwnedHourseTypeRange.min, yearsOwnedHourseTypeRange.max)
        } else {
            self.yearsOwnerHorseField.placeholder = String(format: "%.0f", yearsOwnedHourseTypeRange.min)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let role = self.wizardNavigationController?.wizardObject as? UserRole {
                self.switchStableOwner.isOn = role.riderStableOwner ?? false
                self.horsesNumberField.text = String(format: "%.0f", Float(role.riderNumberHorses ?? 0))
                self.yearsOwnerHorseField.text = String(format: "%.0f", Float(role.riderYearsOwnedHorse ?? 0))
                self.textPresentation.text = role.presentation
                self.textRefName.text = role.riderReferenceName
                self.textRefPhone.text = role.riderReferencePhone
                
                if  role.riderAge <= CHILD_MAX_AGE {
                    ReferenceView.isHidden = false
                    self.textParentEmail.text = role.riderParentEmail
                }
            }
            isInit = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if let role = self.wizardNavigationController?.wizardObject as? UserRole {
            role.riderStableOwner = self.switchStableOwner.isOn
            role.riderNumberHorses = Int(self.horsesNumberField.text ?? "0")
            role.riderYearsOwnedHorse = Int(self.yearsOwnerHorseField.text ?? "0")
            role.presentation = self.textPresentation.text
            role.riderReferenceName = self.textRefName.text
            role.riderReferencePhone = self.textRefPhone.text
            role.riderParentEmail = self.textParentEmail.text
        }
        
        if isValid() {
            self.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        var isValid = true
        
        guard validatorNotEmpty.validate(string: self.textPresentation.text),
            !self.textPresentation.text.trimmingCharacters(in: .whitespaces).isEmpty else {
                CustomAlertView(okTitle: Str.screen_registration_rider_presentation_title,
                                okMessage: Str.screen_registration_rider_presentation_title_hint,
                                okAction: nil).show()
                return false
        }
        
        if self.userRole?.isChild ?? true {
            
            isValid = validatorInvalidEmail.validate(self.textParentEmail) && isValid
            return isValid
        }
        
        let horsesNumberValidatorInvalidAmount = HKTextValidator(errorMessage: "text_validator_invalid_amount".localized,
                                                   rool: HKTextValidator.ruleIntNumber(min: Int(numberOfHorsesRange.min),
                                                                                     max: Int(numberOfHorsesRange.max)))
        let yearsOwnerHorseValidatorInvalidAmount = HKTextValidator(errorMessage: "text_validator_invalid_amount".localized,
                                                               rool: HKTextValidator.ruleIntNumber(min: Int(yearsOwnedHourseTypeRange.min),
                                                                                                 max: Int(yearsOwnedHourseTypeRange.max)))

        isValid = horsesNumberValidatorInvalidAmount.validate(self.horsesNumberField) && isValid
        isValid = yearsOwnerHorseValidatorInvalidAmount.validate(self.yearsOwnerHorseField) && isValid
        
        return isValid
    }
}
