//
//  HorseRegistration1ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseRegistration1ViewController: HKBaseTableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet open var tableView: UITableView!
    
    open var horse: HKAsset? {
        get { return wizardNavigationController?.wizardObject as? HKAsset }
    }

    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized,
                                                  rool: HKTextValidator.ruleNotEmpty())
    
    private let checkableItemCell = "CheckableItemCell"
    private let sectionTitleCell = "SectionTitleCell"
    private let sectionTitleNoDescCell = "SectionTitleNoDescCell"
    private let detailsEditItemCell = "DetailsEditItemCell"
    private let detailsButtonCell = "DetailsButtonCell"
    private let actionButtonCell = "ActionButtonCell"
    
    
    private var competenceLevel: [String] = []
    private lazy var competenceLevelDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.competenceLevel.contains(property.value) {
                self.competenceLevel.append(property.value)
            }
            else if !isOn {
                if let index = self.competenceLevel.index(of: property.value) {
                    self.competenceLevel.remove(at: index)
                }
            }
        }
    }
    
    private var competed: [String] = []
    private lazy var competedDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.competed.contains(property.value) {
                self.competed.append(property.value)
            }
            else if !isOn {
                if let index = self.competed.index(of: property.value) {
                    self.competed.remove(at: index)
                }
            }
        }
    }
    
    private var horseType: [String] = []
    private lazy var horseTypeDelegate = { (_ object: AnyObject?, _ isOn: Bool) -> Void in
        if let property = object as? HKStructureEnumPropertyValue {
            if isOn && !self.horseType.contains(property.value) {
                self.horseType.append(property.value)
            }
            else if !isOn {
                if let index = self.horseType.index(of: property.value) {
                    self.horseType.remove(at: index)
                }
            }
        }
    }
    
    private var isInit = false
    
    private var horseName: String?
    @IBOutlet weak var horseNameField: UITextField!
    
    private var birthDate: Date?
    @IBOutlet weak var btnBirthDate: UIButton!
    
    @IBOutlet weak var btnBreed: UIButton!
    private var horseBreed: String?
    private var breed: [(name: String, value: String)] {
        let horseBreedProperties = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseBreed.rawValue] as! HKStructureEnumProperty
        return horseBreedProperties.values.map { (name: $0.name, value: $0.value) }
    }
    
    @IBOutlet weak var btnBeenRiding: UIButton!
    private var yearsBeenRiding: String?
    private var beenRiding: [(name: String, value: String)] {
        return getPropertyListTuple(HKAsset.Fields.horseYearsBeenRiding.rawValue)
    }
    
    private func getPropertyListTuple(_ propertyName: String) -> [(name: String, value: String)] {
        let property = AppApi.getHorseStructure()?.properties?[propertyName] as! HKStructureEnumProperty
        return property.values.map { (name: $0.name, value: $0.value) }
    }

    override func viewDidLoad() {
        self.horseName = self.horse?.name
        self.birthDate = self.horse?.horseBirthDate
        self.horseBreed = self.horse?.horseBreed
        self.horseType = self.horse?.horseType ?? []
        self.competenceLevel = self.horse?.horseCompetenceLevel ?? []
        self.competed = self.horse?.horseCompeted ?? []
        self.yearsBeenRiding = self.horse?.horseYearsBeenRiding
        
        self.buildItemsSource()
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 73
        
        navigationController?.setToolbarHidden(false, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    private func buildItemsSource() {
        self.itemsSource.removeAll()
        var section = HKTableViewSection()
        let sectionTitleBinder = SectionTitleCell.buildBinder()
        
        // horse name
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_name, desc: ""), sectionTitleBinder))
        
        section.items.append((detailsEditItemCell, horse, { (viewController, cell, item) in
            let detailsEditCell = (cell as? DetailsEditItemCell)!
            let parent = viewController as! HorseRegistration1ViewController
            
            detailsEditCell.editField.text = parent.horseName
            detailsEditCell.editField.placeholder = Str.screen_registration_horse_name_hint
//            detailsEditCell.editField.delegate = parent
            detailsEditCell.editField.addTarget(parent, action: #selector(parent.horseNameFieldDidChange(_:)), for: .editingChanged)
            self.horseNameField = detailsEditCell.editField
        }))
        
        // horse birth date
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_birth_date, desc: ""), sectionTitleBinder))
        
        section.items.append((actionButtonCell, horse, { (viewController, cell, item) in
            let buttonCell = cell as! ActionButtonCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration1ViewController
            var buttonTitle = ""
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .none
 
            if let horseBirthDate = horse.horseBirthDate {
                buttonTitle = dateFormatter.string(from: horseBirthDate)
            } else {
                if parent.birthDate != nil {
                    buttonTitle = dateFormatter.string(from: parent.birthDate!)
                } else {
                    buttonTitle = Str.screen_registration_horse_birth_date_hint
                }
            }
            
            buttonCell.actionButton.setTitle(buttonTitle, for: .normal)
            buttonCell.actionButton.addTarget(parent, action: #selector(parent.actionSelectBirthDay), for: .touchDown)
            self.btnBirthDate = buttonCell.actionButton
        }))
        
        // Breed
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_breed, desc: ""), sectionTitleBinder))
        
        section.items.append((actionButtonCell, self.horse, { (viewController, cell, item) in
            let buttonCell = cell as! ActionButtonCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration1ViewController
            var buttonTitle = String()

            if let horseBreed = horse.horseBreed {
                buttonTitle = parent.breed.first(where: { $0.value == horseBreed })!.name
            } else {
                if parent.horseBreed != nil {
                    if let value = parent.breed.first(where: { $0.value == parent.horseBreed }) {
                        buttonTitle = value.name
                    }
                } else {
                    parent.horseBreed = parent.breed.first?.value
                    buttonTitle = (parent.breed.first?.name)!
                }
            }
            
            buttonCell.actionButton.setTitle(buttonTitle, for: .normal)
            buttonCell.actionButton.addTarget(self, action: #selector(self.actionSelectBreed), for: .touchDown)
            self.btnBreed = buttonCell.actionButton
        }))
        
        // Type of horse
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_kind, desc: ""), sectionTitleBinder))
        
        let horseType = (AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseType.rawValue] as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        var binder = buildHorseTypeBinder()
        for value in horseType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        // Competence Level
        section.items.append((sectionTitleNoDescCell, (title: Str.screen_registration_horse_competence_level, desc: ""), sectionTitleBinder))
        
        let completeceLevelType = (AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseCompetenceLevel.rawValue] as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        binder = buildCompetenceLevelBinder()
        for value in completeceLevelType.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        self.itemsSource.append(section)
        
        // Competed
        section = HKTableViewSection()
        section.items.append((self.sectionTitleNoDescCell, (title: Str.screen_registration_horse_competed, desc: ""), sectionTitleBinder))
        
        let competed = (AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseCompetitionField.rawValue] as! HKStructureArrayProperty).subtype as! HKStructureEnumProperty
        
        binder = buildCompetedBinder()
        for value in competed.values {
            section.items.append((self.checkableItemCell, value, binder))
        }
        
        self.itemsSource.append(section)
        
        // Horse Years Been Riding
        section = HKTableViewSection()
        section.items.append((self.sectionTitleNoDescCell, (title: Str.screen_registration_horse_been_riding, desc: ""), sectionTitleBinder))
        
        section.items.append((actionButtonCell, self.horse, { (viewController, cell, item) in
            let buttonCell = cell as! ActionButtonCell
            let horse = item as! HKAsset
            let parent = viewController as! HorseRegistration1ViewController
            var buttonTitle = String()
            
            if let horseYearsBeenRiding = horse.horseYearsBeenRiding {
                buttonTitle = parent.beenRiding.first(where: { $0.value == horseYearsBeenRiding })!.name
            } else {
                if parent.yearsBeenRiding != nil {
                    if let value = parent.beenRiding.first(where: { $0.value == parent.yearsBeenRiding }) {
                        buttonTitle = value.name
                    }
                } else {
                    parent.yearsBeenRiding = parent.beenRiding.first?.value
                    buttonTitle = (parent.beenRiding.first?.name)!
                }
            }
            
            buttonCell.actionButton.setTitle(buttonTitle, for: .normal)
            buttonCell.actionButton.addTarget(parent, action: #selector(parent.actionSelectBeenRiding), for: .touchDown)
            parent.btnBeenRiding = buttonCell.actionButton
        }))
        
        self.itemsSource.append(section)
    }

    private func buildHorseTypeBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! HorseRegistration1ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.horseType.contains(property.value)
            checkableCell.switchDelegate = parent.horseTypeDelegate
        }
    }
    
    private func buildCompetenceLevelBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! HorseRegistration1ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.competenceLevel.contains(property.value)
            checkableCell.switchDelegate = parent.competenceLevelDelegate
        }
    }
    
    private func buildCompetedBinder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let property = item as! HKStructureEnumPropertyValue
            let checkableCell = cell as! CheckableItemCell
            let parent = viewController as! HorseRegistration1ViewController
            
            checkableCell.object = item as AnyObject
            checkableCell.titleLabel.text = property.name
            checkableCell.switchView.isOn = parent.competed.contains(property.value)
            checkableCell.switchDelegate = parent.competedDelegate
        }
    }
    
    @IBAction func actionSelectBirthDay(_ sender: UIButton) {
        DatePickerDialog().show(Str.screen_registration_horse_birth_date,
                                doneButtonTitle: "dlg_common_button_ok".localized,
                                cancelButtonTitle: "dlg_common_button_cancel".localized,
                                defaultDate: self.birthDate ?? Date(), minimumDate: nil, maximumDate: Date(),
                                datePickerMode: .date) { [unowned self] (date) in
                                    if date != nil {
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateStyle = .short
                                        dateFormatter.timeStyle = .none
                                        
                                        self.birthDate = date
                                        self.btnBirthDate.setTitle(dateFormatter.string(from: date!), for: .normal)
                                    }
        }
    }
    
    @IBAction func actionSelectBreed(_ sender: UIButton) {
        let alert = UIAlertController(title: "screen_registration_horse_breed".localized, message: "\n\n\n\n\n\n", preferredStyle: .actionSheet)
        alert.isModalInPopover = true
        
        let pickerView = UIPickerView(frame: CGRect.init(x: 5, y: 20, width: UIScreen.main.bounds.width - 37, height: 180))
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.selectRow(self.breed.index{$0.value == self.horseBreed!}!, inComponent: 0, animated: true)
        alert.view.addSubview(pickerView)
        
        let actionOk = UIAlertAction(title: "dlg_common_button_ok".localized, style: .default) { [unowned self] (action) in
            let selectedItem = self.breed[pickerView.selectedRow(inComponent: 0)]

            self.horseBreed = selectedItem.value
            self.btnBreed.setTitle(selectedItem.name, for: .normal)
        }
        alert.addAction(actionOk)
        
        let actionCancel = UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel, handler: nil)
        alert.addAction(actionCancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func actionSelectBeenRiding(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        for value in beenRiding {
            alertController.addAction(UIAlertAction(title: value.name, style: .default) {
                [unowned self] action in
                self.yearsBeenRiding = value.value
                self.updateViews()
            })
        }
        
        self.present(alertController, animated: true)
    }
    
    private func updateViews() {
        if beenRiding.isEmpty {
            if self.btnBeenRiding != nil {
                self.btnBeenRiding.isUserInteractionEnabled = false
            }
            self.btnBeenRiding.setTitle("ERROR!", for: .normal)
        }
        
        if self.btnBeenRiding != nil {
            self.btnBeenRiding.isUserInteractionEnabled = true
        }
        if let value = self.beenRiding.first(where: { $0.value == self.yearsBeenRiding }) {
            self.btnBeenRiding.setTitle(value.name, for: .normal)
        }
        else {
            self.yearsBeenRiding = self.beenRiding.first?.value
            if self.btnBeenRiding != nil {
                self.btnBeenRiding.setTitle((self.beenRiding.first?.name)!, for: .normal)
            }
        }
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if isValid() {
            if let horse = self.wizardNavigationController?.wizardObject as? HKAsset {
                horse.horseCompetenceLevel = competenceLevel
                horse.horseCompeted = competed
                horse.horseType = horseType
                horse.horseName = horseName
                horse.horseBirthDate = birthDate
                horse.horseBreed = horseBreed
                horse.horseYearsBeenRiding = yearsBeenRiding
            }
            
            self.wizardNavigationController?.nextViewController()
        }
    }
    
    private func isValid() -> Bool {
        if self.birthDate == nil {
            CustomAlertView(okTitle: Str.screen_registration_horse_birth_date,
                            okMessage: Str.screen_registration_horse_birth_date_hint,
                            okAction: nil).show()
            return false
        }
        
        if self.horseType.isEmpty {
            CustomAlertView(okTitle: Str.screen_registration_horse_kind,
                            okMessage: Str.screen_registration_empty_list_title,
                            okAction: nil).show()
            return false
        }
        
        if self.competenceLevel.isEmpty {
            CustomAlertView(okTitle: Str.screen_registration_horse_competence_level,
                            okMessage: Str.screen_registration_empty_list_title,
                            okAction: nil).show()
            return false
        }
        
        var isValid = true
        
        if self.horseNameField != nil {
            isValid = validatorNotEmpty.validate(self.horseNameField) && isValid
        }
        let stringDate = ApiDateFormatter.string(from: self.birthDate, dateStyle: .short, time: .none)!
        isValid = validatorNotEmpty.validate(string: stringDate) && isValid
        isValid = !self.horseType.isEmpty && isValid
        isValid = !self.competenceLevel.isEmpty && isValid
        
        return isValid
    }
    
    // MARK: - UITextFieldDelegate
    
    @objc func horseNameFieldDidChange(_ textField: UITextField) {
        self.horseName = textField.text
    }
    
    // MARK: - UIPickerViewDataSource
    
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return breed.count
    }
    
    // MARK: - UIPickerViewDelegate
//    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        btnBreed.setTitle(breed[row], for: .normal)
//    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return breed[row].name
    }
}
