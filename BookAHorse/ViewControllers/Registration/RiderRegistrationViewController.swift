//
//  RiderRegistrationViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/24/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistrationViewController: HKWizardNavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let role = AppApi.riderRole() != nil ? UserRole(from: AppApi.riderRole())! : UserRole()
        role.roleId = AppApi.ROLE_RIDER
        role.riderName = "\(HonkioApi.activeUser()?.firstname ?? "") \(HonkioApi.activeUser()?.lastname ?? "")"
        role.email = HonkioApi.activeUser()?.login
        role.phone = HonkioApi.activeUser()?.phone
        
        self.wizardObject = role
        
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration1ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration2ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration3ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration4ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration5ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration6ViewController"))
//        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "RiderRegistration7ViewController"))
    }
    
    open override func onWizardComplete(_ viewController: UIViewController, tag: String?, wizardObject: AnyObject?) {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioApi.userSetRole(wizardObject as! UserRole, shop: nil, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                AppApi.apiInstance.onRoleGet(role: response.result as! UserRole)
                AppSettings.theme = ROLE_RIDER
                self?.showMainScreen()
            }
            else if let strongSelf = self {
                let error = response.anyError()
                if !AppErrors.handleError(strongSelf, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
//        viewController.title = "\(self.viewControllers.count + 1)/\(self.wizardViewControllers.count)"
        viewController.title = Str.screen_registration_rider_title
        super.pushViewController(viewController, animated: animated)
    }
    
    open func showMainScreen() {
        if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
            
            let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
            appDelegate.changeRootViewController(viewController)
        }
    }
}
