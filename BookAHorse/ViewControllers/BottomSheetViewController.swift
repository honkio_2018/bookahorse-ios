//
//  BottomSheetViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 26/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate
import CoreLocation

class BottomSheetViewController: HKBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var backgroundCellImage = UIImage(named: "bg_horse_no_photo")
    let formatter = MeasurementFormatter()
    let cellReuseIdentifier = String(describing: HorseListItemCell.self)
    
    let assetRowHeight: CGFloat = 220.0
    let fullView: CGFloat = 100
    var partialView: CGFloat {
        let navHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
        return UIScreen.main.bounds.height - assetRowHeight - navHeight
    }
    
    public var assets: HKAsset? {
        didSet {
            if assets != nil {
                tableView?.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = assetRowHeight
        tableView.tableFooterView = UIView()
        let nib = UINib.init(nibName: cellReuseIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellReuseIdentifier)

        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = .ceiling
        numberFormatter .maximumFractionDigits = 1
        formatter.numberFormatter = numberFormatter
        formatter.unitOptions = .naturalScale
        
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareBackgroundView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height - 100)
            })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)

        let y = self.view.frame.minY
        if (y + translation.y >= fullView) && (y + translation.y <= partialView) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                }
                
                }, completion: { [weak self] _ in
                    if ( velocity.y < 0 ) {
                        self?.tableView.isScrollEnabled = true
                    }
            })
        }
    }
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .dark)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        view.insertSubview(bluredView, at: 0)
    }
}

extension BottomSheetViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)
        guard
            let assetCell = cell as? HorseListItemCell,
            let asset = self.assets
            else {
                assertionFailure("cell or asset is nil, cannot build table cell!")
                return cell!
        }

        assetCell.horseName.text = asset.name.uppercased()
        // assetCell.visibilityIconView.isHidden = asset.isVisible

        if let defaultPicture = asset.properties?["default_picture"] {
            if let horseImageView = assetCell.horseImage {
                horseImageView.imageFromURL(link: (defaultPicture as? String),
                                            errorImage: self.backgroundCellImage,
                                            contentMode: nil)
            }
        } else {
            assetCell.horseImage.image = self.backgroundCellImage
        }

        var result = [String]()
        // Add horse breed
        if let horseBreed = AppApi.getHorseStructure()?.findEnumPropertyValue(HKAsset.Fields.horseBreed.rawValue,
                                                                              value: asset.horseBreed)?.name, !horseBreed.isEmpty {
            result.append(horseBreed)
        }

        // Add address of horse (in next version)
        // if let address = asset.address, !address.admin.isEmpty {
        //     result.append(address.city)
        // }

        // Add distance info
        if let currentLocation = HonkioApi.deviceLocation() {
            let horseLocation = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
            let distance = currentLocation.distance(from: horseLocation)
            let distanceInMeters = Measurement(value: distance, unit: UnitLength.meters)
            result.append(self.formatter.string(from: distanceInMeters))
        }

        assetCell.horseInfo.text = result.filter({ !$0.isEmpty }).joined(separator: ", ")
    
        return assetCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        guard HonkioApi.isConnected(),
            let asset = self.assets
            else { return }

        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseProductsViewController.self), viewController: self) as? HKAssetViewProtocol {
            controller.loadAsset(asset.assetId)
            self.navigationController?.pushViewController(controller as! UIViewController, animated: true)
        }
    }
}

extension BottomSheetViewController: UIGestureRecognizerDelegate {

    // Solution
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y

        let y = view.frame.minY
        if (y == fullView && tableView.contentOffset.y == 0 && direction > 0) || (y == partialView) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return false
    }
    
}
