//
//  IntroNavigationViewController.swift
//  BookAHorse
//
//  Created by Dash on 25.09.2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

open class IntroNavigationViewController: UINavigationController {
    
    open override func viewDidLoad() {
        self.viewControllers = [IntroViewController()]
    }

}
