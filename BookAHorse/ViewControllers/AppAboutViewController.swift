//
//  AppAboutViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AppAboutViewController: HKAboutViewController {
    
    @IBOutlet weak var appVersionLabel: UILabel!
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Bundle.main.releaseVersionNumber
        //Bundle.main.buildVersionNumber
        appVersionLabel.text = Bundle.main.releaseVersionNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
