//
//  ProductDetailsViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 21/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class ProductDetailsViewController: HKBaseViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var descriptionTextView: CustomUITextView!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var minuteDurationField: UITextField!
    @IBOutlet weak var hourDurationField: UITextField!
    @IBOutlet weak var dayDurationField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    
    enum Mode {
        case new
        case edit
    }
    
    public var product: Product? {
        didSet {
            if let product = product {
                viewMode = .edit
                showProduct(product)
            }
        }
    }
    
    private let currency = HonkioApi.mainShopInfo()?.merchant.currency
    private var viewMode: Mode = .new
    
    private let priceValidator = HKTextValidatorSet(validators: [
        HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty()),
        HKTextValidator(errorMessage: "text_validator_invalid_amount".localized, rool: HKTextValidator.ruleDoubleNumber()),
        HKTextValidator(errorMessage: "screen_merchant_product_amount_les_than_min".localized, rool: HKTextValidator.ruleDoubleNumber(min: 100.0, max: nil))
        ])
    
    private let nameValidator = HKTextValidatorSet(validators: [
        HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty()),
        HKTextValidator(errorMessage: "text_validator_name_length".localized, rool: HKTextValidator.ruleName())
        ])
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    private let validatorIntNumber = HKTextValidator(errorMessage: "text_validator_invalid_amount".localized, rool: HKTextValidator.ruleIntNumber())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        currencyLabel.text = currency

        // add right button on navigation bar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(ProductDetailsViewController.actionMenu))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonDidTapped(_ sender: UIButton) {
        dismissKeyboard()
        checkProduct()
    }
    
    func checkProduct() {
        guard isValid() else { return }

        if getDuration().second >= PRODUCT_DURATION_MIN_VALUE * 60 {
            saveProduct()
        } else {
             CustomAlertView(okTitle: Str.screen_merchant_product_duration_dialog_title,
                             okMessage: Str.screen_merchant_product_duration_dialog_message,
                             okAction: nil).show()
        }
    }
    
    func saveProduct() {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioMerchantApi.merchantProductSet(getProduct(), shop:AppApi.getOwnerShop(), flags: 0) { [unowned self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                self.navigationController?.popViewController(animated: true)
                return
            } else {
                let error = response.anyError()
                if !AppErrors.handleError(self, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    public func showProduct(_ product: Product) {
        nameField.text = product.name
        descriptionTextView.text = product.desc
        priceField.text = String(product.amount)
        // convert miliseconds to minutes and set text duration
        var duration: TimeInterval = 0.0
        duration.millisecond = product.duration
        minuteDurationField.text = String(duration.minute)
        hourDurationField.text = String(duration.hour % 24)
        dayDurationField.text = String(duration.day)
        currencyLabel.text = product.currency
//        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func getProduct() -> Product {
    
        let currentProduct: Product = product != nil ? product! : Product()
        
        currentProduct.name = nameField.text ?? ""
        currentProduct.desc = descriptionTextView.text
        currentProduct.amount = Double(priceField.text!) ?? 0.0
        currentProduct.duration = getDuration().millisecond //convert to milliseconds
        currentProduct.currency = currency
        currentProduct.typeId = AppApi.PRODUCT_ID_TYPE
        currentProduct.isActive = true
        return currentProduct
    }
    
    func getDuration() -> TimeInterval {
        var duration: TimeInterval = 0.0
        duration.minute = Int(minuteDurationField.text ?? "") ?? 0
        duration.hour = Int(hourDurationField.text ?? "") ?? 0
        duration.day = Int(dayDurationField.text ?? "") ?? 0
        return duration
    }
    
    @objc func actionMenu(_ sender: UIBarButtonItem) {

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {[unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        if viewMode == .edit {
            alertController.addAction(UIAlertAction(title: Str.menu_product_details_delete, style: .default) {[unowned self] action in
                self.hideProduct()
            })
        }
        
        self.present(alertController, animated: true)
    }
    
    func hideProduct() {
        BusyIndicator.shared.showProgressView(self.view)
        let product = getProduct()
        product.isActive = false
        product.isVisible = false
        HonkioMerchantApi.merchantProductSet(product, shop:AppApi.getOwnerShop(), flags: 0)
        { [unowned self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                self.navigationController?.popViewController(animated: true)
                return
            } else {
                let error = response.anyError()
                if !AppErrors.handleError(self, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    private func isProductValid() -> Bool {
        var isValid = true
        
        isValid = priceValidator.validate(priceField) && isValid
        isValid = nameValidator.validate(nameField) && isValid
        isValid = validatorNotEmpty.validate(string: descriptionTextView.text) && isValid
        
        if let minuteDuration = minuteDurationField.text, !minuteDuration.isEmpty {
            isValid = isValid && true
        } else if let hourDuration = hourDurationField.text, !hourDuration.isEmpty {
            isValid = isValid && true
        } else if let dayDuration = dayDurationField.text, !dayDuration.isEmpty {
            isValid = isValid && true
        } else {
            isValid = validatorIntNumber.validate(dayDurationField) && isValid
            isValid = validatorIntNumber.validate(hourDurationField) && isValid
            isValid = validatorIntNumber.validate(minuteDurationField) && isValid
        }
        
        return isValid
    }

    private func isValid() -> Bool {
        return isProductValid()
    }
}
