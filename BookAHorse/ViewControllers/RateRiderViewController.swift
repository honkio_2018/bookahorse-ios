//
//  RateRiderViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 26/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate


class RateRiderViewController: HKBaseTableViewController, HKOrderViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    let OrderRateCellIdentifier = String(describing: OrderRateCell.self)
    let OrderTableViewCellIdentifier = String(describing: OrderTableViewCell.self)
    
    public var order: Order! {
        didSet {
            self.initDataSource()
            self.tableView.reloadData()
        }
    }
    
    public var isEditable: Bool = true
    
    var riderComment: UserComment = UserComment() {
        didSet {
            self.initDataSource()
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 156
        self.tableView.tableFooterView = UIView()
        
        // order rate and comment cell
        self.tableView.register(UINib.init(nibName: OrderRateCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: OrderRateCellIdentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func initDataSource() {
        //        let user = HonkioApi.activeUser()
        guard let order = self.order else {
            assertionFailure("Error: Order is nil")
            return
        }
        self.itemsSource.removeAll()
        let section = HKTableViewSection()
        
        // order cell
        section.items.append((OrderTableViewCellIdentifier, order, {(viewController, cell, item) in
            let orderCell = cell as! OrderTableViewCell
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            
            orderCell.dateLabel.text = dateFormatter.string(from: order.startDate)
            orderCell.titleLabel.text = order.title
            orderCell.imgHasMessages.isHidden = true
            
            if !order.isStatus(Order.Status.bookRequest.rawValue) {
                orderCell.descLabel.text = order.statusName
            }
            else {
                orderCell.descLabel.text = "-"
            }
            
            orderCell.photoView.setBorder(.small)
            orderCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + order.asset,
                                             errorImage: UIImage(named: "ic_placeholder_horse"),
                                             contentMode: nil)
        }))
    
        // rate owner
        section.items.append((OrderRateCellIdentifier, riderComment, { (viewController, cell, item) in
            let riderRatingCell = cell as! OrderRateCell
            let parent = viewController as! RateRiderViewController
            let comment = item as? UserComment
            
            riderRatingCell.commentTextView.text = comment?.text
            riderRatingCell.showRating(comment?.mark)
            
            riderRatingCell.delegate = parent
            riderRatingCell.commentTextView.delegate = parent
            riderRatingCell.headerLabel.text = parent.isEditable ? Str.screen_rate_owner_question : Str.screen_rate_owner_rated_rider
            riderRatingCell.warningLabel.text = parent.isEditable ? Str.screen_rate_rider_warning : ""
        }))
        
        self.itemsSource.append(section)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioApi.userGetRole(AppApi.ROLE_RIDER, userId: order.thirdPerson, flags: 0) { [weak self] (response) in
            if response.isStatusAccept() {
                let riderRole = response.result as? UserRole
                self?.riderComment.voter = nil
                self?.riderComment.text = self?.riderComment.text ?? ""
                self?.riderComment.orderId = self?.order.orderId
                self?.riderComment.object = riderRole?.objectId
                self?.riderComment.objectType = HK_OBJECT_TYPE_USER_ROLE
                HonkioMerchantApi.merchantVote(self?.riderComment, flags: 0, handler: { [weak self] (response) in
                    BusyIndicator.shared.hideProgressView()
                    if response.isStatusAccept() {
                        self?.navigationController?.popViewController(animated: true)
                    }
                    else {
                        self?.onError(response.anyError())
                    }
                })
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    private func onError(_ error: ApiError) {
        BusyIndicator.shared.hideProgressView()
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
}

extension RateRiderViewController: OrderRateCellDelegate {
    
    func ratingDidSet(_ sender: OrderRateCell, _ ratingValue: Int) {
        riderComment.mark = ratingValue
    }
    
    func textViewDidChange(_ textView: UITextView) {
        riderComment.text = textView.text
    }
}
