//
//  MainTabViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 08/05/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import AVKit
import HonkioAppTemplate

class MainAuthorisedViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // let imageName: String = AppSettings.theme == ROLE_RIDER ? "rider" : "owner"
        let image = UIImage(named: "logo_nav")
        let logoImageView = UIImageView(image: image)
        // logoImageView.frame = CGRect(x: -40, y: 0, width: 150, height: 20)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = image
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -25
        navigationItem.leftBarButtonItems = [negativeSpacer, imageItem]
        
        // size of title navigation bar
//        self.naigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "System", size: 18)!]
        self.selectedIndex = 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionMainMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        alertController.addAction(UIAlertAction(title: Str.screen_main_menu_view_profile, style: .default) {
            [unowned self] action in
            if AppSettings.theme == ROLE_RIDER {
                if let viewController = AppController.getViewControllerById(AppController.UserDetails, viewController: self) as? RiderDetailsViewController {
                    viewController.userRole = AppApi.riderRole()
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            else if let shop = AppApi.getOwnerShop() {
                if let controller = AppController.getViewControllerById(AppController.Shop, viewController: self) as? HKBaseShopViewController {
                    controller.load(byIdentyty: shop)
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            }
        })
        
        alertController.addAction(UIAlertAction(title: Str.screen_main_menu_help_video, style: .default) {
            [unowned self] action in
            let viewController = AVPlayerViewController()
            viewController.player = AVPlayer(url: URL(string: Str.screen_intro_video_link)!)
            self.navigationController?.pushViewController(viewController, animated: true)
        })
        
        alertController.addAction(UIAlertAction(title: Str.screen_main_menu_switch_mode, style: .default) {
            [unowned self] action in
            
            if AppSettings.theme == ROLE_RIDER && !AppApi.isRegisteredAsOwner() {
                if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: OwnerRegistrationViewController.self), viewController: self) {
                    self.present(controller, animated: true, completion: nil)
                }
            }
            else if AppSettings.theme == ROLE_OWNER && !AppApi.isRegisteredAsRider() {
                if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: RiderRegistrationViewController.self), viewController: self) {
                    self.present(controller, animated: true, completion: nil)
                }
            }
            else {
                AppSettings.switchTheme()
                if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
                    
                    let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                    appDelegate.changeRootViewController(viewController)
                }
            }
        })
        
        if AppSettings.theme == ROLE_OWNER {
            alertController.addAction(UIAlertAction(title: Str.screen_main_menu_earnings, style: .default) {
                [unowned self] action in
                if let viewController = AppController.instance.getViewControllerByIdentifier("ReportViewController", viewController: self) {
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            })
        }
        
        self.present(alertController, animated: true)
    }

}
