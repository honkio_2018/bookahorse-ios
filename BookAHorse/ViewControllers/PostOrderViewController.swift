//
//  PostOrderViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostOrderViewController: HKBaseViewController {
    
    @IBOutlet weak var textComment: CustomUITextView!
    
    public var asset: HKAsset!
    public var products: [BookedProduct]!
    public var event: Event!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(PostOrderViewController.actionNext))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.textComment.becomeFirstResponder()
    }
    
    @IBAction func actionNext(_ sender: Any?) {
        if let controller = AppController.getViewControllerById(AppController.OrderDetails, viewController: self) as? HKOrderViewProtocol {
            let order = Order()!
            order.userOwner = HonkioApi.activeUser()?.userId
            order.currency = HonkioApi.mainShopInfo()?.merchant?.currency
            order.asset = self.asset.assetId
            if AppApi.riderRole()?.isChild ?? true {
                order.parentEmail = AppApi.riderRole()?.riderParentEmail
                order.model = Order.Model.modelRequest.rawValue
                order.status = Order.StatusRequest.new.rawValue
            }
            else {
                order.thirdPerson = HonkioApi.activeUser()?.userId
                order.model = Order.Model.model.rawValue
                order.status = Order.Status.bookRequest.rawValue
            }
            
            order.creationDate = Date()
            order.title = self.asset.name
            order.startDate = self.event.start
            order.endDate = calculateEndDate()
            order.shopId = self.asset.horseShop
            order.products = self.products
            order.comment = self.textComment.text
            
            controller.order = order
            self.navigationController?.pushViewController(controller as! UIViewController, animated: true)
        }
    }
    
    private func calculateEndDate() -> Date {
        var durationMillisec = 0
        for book in self.products {
            durationMillisec += book.product.duration * Int(book.count)
        }
        
        var dateComp = DateComponents()
        dateComp.second = durationMillisec / 1000
        
        return Calendar.current.date(byAdding: dateComp, to: self.event.start)!
    }

}
