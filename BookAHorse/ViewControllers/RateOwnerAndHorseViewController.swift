//
//  RateOwnerAndHorseViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RateOwnerAndHorseViewController: HKBaseTableViewController, HKOrderViewProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    let OrderRateCellIdentifier = String(describing: OrderRateCell.self)
    let OrderTableViewCellIdentifier = String(describing: OrderTableViewCell.self)
    
    public var order: Order! {
        didSet {
            self.initDataSource()
            self.tableView.reloadData()
        }
    }
    
    public var isEditable: Bool = true
    
    var ownerComment = UserComment() {
        didSet {
            self.initDataSource()
            self.tableView.reloadData()
        }
    }
    
    var horseComment = UserComment() {
        didSet {
            self.initDataSource()
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
         self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 156
        self.tableView.tableFooterView = UIView()
        
        // order rate and comment cell
        self.tableView.register(UINib.init(nibName: OrderRateCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: OrderRateCellIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func initDataSource() {
//        let user = HonkioApi.activeUser()
        guard let order = self.order else {
            assertionFailure("Error: Order is nil")
            return
        }
        self.itemsSource.removeAll()
        let section = HKTableViewSection()
        
        // order cell
        section.items.append((OrderTableViewCellIdentifier, order, {(viewController, cell, item) in
            let orderCell = cell as! OrderTableViewCell
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            
            orderCell.dateLabel.text = dateFormatter.string(from: order.startDate)
            orderCell.titleLabel.text = order.title
            orderCell.imgHasMessages.isHidden = true
            
            if !order.isStatus(Order.Status.bookRequest.rawValue) {
                orderCell.descLabel.text = order.statusName
            }
            else {
                orderCell.descLabel.text = "-"
            }
            
            orderCell.photoView.setBorder(.small)
            orderCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + order.asset,
                                             errorImage: UIImage(named: "ic_placeholder_horse"),
                                             contentMode: nil)
        }))
        
        // rate owner
        section.items.append((OrderRateCellIdentifier, ownerComment, { (viewController, cell, item) in
            let ownerRatingCell = cell as! OrderRateCell
            let parent = viewController as! RateOwnerAndHorseViewController
            let comment = item as? UserComment
            
            ownerRatingCell.commentTextView.text = comment?.text
            ownerRatingCell.showRating(comment?.mark)
            
            ownerRatingCell.delegate = parent
//            ownerRatingCell = Str.screen_rate_comment_hint
            ownerRatingCell.commentTextView.delegate = parent
             // for detect text view cell type: owner or horse rating comment
            ownerRatingCell.commentTextView.tag = 1
            ownerRatingCell.headerLabel.text = parent.isEditable ? Str.screen_rate_rider_question : Str.screen_rate_rider_rated_owner
            ownerRatingCell.warningLabel.text = parent.isEditable ? Str.screen_rate_owner_warning : ""
            ownerRatingCell.tag = 1
        }))
        
        // rate horse
        section.items.append((OrderRateCellIdentifier, horseComment, {(viewController, cell, item) in
            let horseRatingCell = cell as! OrderRateCell
            let parent = viewController as! RateOwnerAndHorseViewController
            let comment = item as? UserComment
            
            horseRatingCell.commentTextView.text = comment?.text
            horseRatingCell.showRating(comment?.mark)
            
            horseRatingCell.delegate = parent
//            horseRatingCell = Str.screen_rate_comment_hint
            horseRatingCell.commentTextView.delegate = parent
            // for detect text field cell type: owner or horse rating comment
            horseRatingCell.commentTextView.tag = 2
            horseRatingCell.headerLabel.text = parent.isEditable ? Str.screen_rate_rider_horse_question : Str.screen_rate_rider_rated_horse
            horseRatingCell.warningLabel.text = ""
            horseRatingCell.tag = 2
        }))
        
        self.itemsSource.append(section)
    }

    @IBAction func submitAction(_ sender: UIButton) {
        sendOwnerComment()
    }
    
    private func sendOwnerComment() {
        BusyIndicator.shared.showProgressView(self.view)

        ownerComment.text = ownerComment.text ?? ""
        ownerComment.orderId = order.orderId
        ownerComment.object = order.shopId
        ownerComment.objectType = HK_OBJECT_TYPE_SHOP
        ownerComment.voter = AppApi.riderRole()
        HonkioApi.userVote(ownerComment, flags: 0) { [weak self] (response) in
            if response.isStatusAccept() {
                self?.sendHorseComment();
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    private func sendHorseComment() {
        horseComment.text = horseComment.text ?? ""
        horseComment.orderId = order.orderId
        horseComment.object = order.asset
        horseComment.objectType = HK_OBJECT_TYPE_ASSET
        horseComment.voter = AppApi.riderRole()
        HonkioApi.userVote(horseComment, flags: 0) { [weak self] (response) in
            if response.isStatusAccept() {
                BusyIndicator.shared.hideProgressView()
                self?.navigationController?.popViewController(animated: true)
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    private func onError(_ error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
    
}

extension RateOwnerAndHorseViewController: OrderRateCellDelegate {

    func ratingDidSet(_ sender: OrderRateCell, _ ratingValue: Int) {
        if sender.tag == 1 {
            ownerComment.mark = ratingValue
        } else {
            horseComment.mark = ratingValue
        }
    }
    
    func textViewDidChange(_ textView: CustomUITextView) {
        if textView.tag == 1 {
            ownerComment.text = textView.text
        } else {
            horseComment.text = textView.text
        }
    }
}
