//
//  PhotoViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/18/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PhotoViewController: HKBaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!

}
