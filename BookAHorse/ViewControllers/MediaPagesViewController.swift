//
//  MediaPagesViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/18/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import AVKit
import HonkioAppTemplate

class MediaPagesViewController: HKBaseMediaPagesViewController {

    override func viewControllerFor(_ mediaFile: HKMediaFile) -> UIViewController {
        if mediaFile.type == HK_MEDIA_FILE_TYPE_IMAGE {
            let viewController = AppController.instance.getViewControllerByIdentifier("PhotoViewController", viewController: self) as! PhotoViewController
            viewController.imageView.imageFromURL(link: mediaFile.url, contentMode: nil, fallAction: {})
            return viewController
        }
        else if mediaFile.type == HK_MEDIA_FILE_TYPE_VIDEO {
            if let url = mediaFile.url {
                let viewController = AVPlayerViewController()
                viewController.player = AVPlayer(url: URL(string: url)!)
                return viewController
            }
        }
        return UIViewController()
    }
}
