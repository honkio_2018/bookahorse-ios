//
//  PushesTableViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/5/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PushesTableViewController: HKBasePushesTableViewController {
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    let dateComponentsFormatter = DateComponentsFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        self.tableView.tableFooterView = UIView()
        
        dateComponentsFormatter.allowedUnits = [.year,.month,.weekOfMonth,.day,.hour,.minute]
        dateComponentsFormatter.maximumUnitCount = 1
        dateComponentsFormatter.unitsStyle = .full
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action,
                                                                 target: self,
                                                                 action: #selector(openActionsMenu))
    }
    
    override open func cellIdentifier(_ item: Push) -> String {
        return String(describing: PushItemCell.self)
    }
    
    override func buildSource(_ lazySection: HKBaseLazyTableViewController.HKTableViewSection) {
        
        let headerSection = HKBaseTableViewController.HKTableViewSection()
        
        headerSection.items.append(("MarkAllAsReadedCell", nil, { (viewController, cell, item) in
            let markCell = cell as? MarkAllAsReadedCell
            
            if !(markCell?.isSetup ?? false) {
                markCell?.isSetup = true
                markCell?.button.addTarget(viewController, action: #selector(PushesTableViewController.markAllAsRead), for: .touchUpInside)
            }
        }))
        
        self.itemsSource.append(headerSection)
        self.itemsSource.append(lazySection)
    }
    
    @objc func actionMarkAllAsRead() {
        for item in self.lazySection.items as [HKTableViewItem] {
            (item.item as? Push)?.isReaded = true
        }
        self.tableView.reloadData()
        
        HonkioApi.userSetPush(true, filter: nil, flags: MSG_FLAG_NO_WARNING)
    }
    
    override open func buildCellBunder() -> HKTableViewItemInitializer? {
        return { [unowned self] (viewController, cell, item) in
            
            let pushCell = cell as! PushItemCell
            let push = item as! Push
            
            pushCell.photoView.setBorder(.small)
            pushCell.unreadCheckView.isHidden = push.isReaded
            pushCell.labelDate.text =  "\(self.dateComponentsFormatter.string(from: push.date, to: Date()) ?? "") \("screen_notifications_title_ago".localized)"
            
            if push.type == PUSH_TYPE_ORDER_STATUS_CHANGED {
                let entity = push.entity as! OrderStatusChangedPushEntity
                
                pushCell.labelTitle1.text = entity.orderTitle
                pushCell.labelTitle2.text = Str.screen_notifications_system_message
                pushCell.photoView.image = self.iconForStatus(entity.orderStatus)
                pushCell.labelDesc.text = push.message
                pushCell.labelDesc.font = UIFont.systemFont(ofSize: pushCell.labelDesc.font.pointSize)
            }
            else if push.type == PUSH_TYPE_ORDER_CHAT_MESSAGE {
                let entity = push.entity as! OrderChatMessagePushEntity
                
                pushCell.labelTitle1.text = "\(entity.senderFirstName ?? "") \(entity.senderLastName ?? "")"
                
                if entity.senderRole == AppApi.ROLE_RIDER {
                    pushCell.labelTitle2.text = Str.screen_notifications_rider_message
                    pushCell.photoView.image = UIImage(named: "ic_placeholder_rider_circle")
                    pushCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_USER_PHOTO + (entity.senderId ?? ""),
                                                    errorImage: pushCell.photoView.image,
                                                    contentMode: nil)
                }
                else {
                    pushCell.labelTitle2.text = Str.screen_notifications_owner_message
                    pushCell.photoView.image = UIImage(named: "ic_placeholder_owner_circle")
                    pushCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_SHOP_PHOTO + (entity.shopId ?? ""),
                                                    errorImage: pushCell.photoView.image,
                                                    contentMode: nil)
                }
                
                pushCell.labelDesc.text = entity.text
                pushCell.labelDesc.font = UIFont.italicSystemFont(ofSize: pushCell.labelDesc.font.pointSize)
            }
            else if push.type == PUSH_TYPE_ASSET_CHANGED {
                let entity = push.entity as! AssetChangedPushEntity
                
                pushCell.labelTitle1.text = entity.assetName
                pushCell.labelTitle2.text = Str.screen_notifications_system_message
                pushCell.photoView.image = UIImage(named: "ic_placeholder_horse")
                pushCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + (entity.assetId ?? ""),
                                                errorImage: pushCell.photoView.image,
                                                contentMode: nil)
                pushCell.labelDesc.text = push.message
            }
            else if push.type == PUSH_TYPE_SYSYEM {
                let entity = push.entity as! HKSystemMessagePushEntity
                
                pushCell.labelTitle1.text = ""
                pushCell.labelTitle2.text = Str.screen_notifications_system_message
                pushCell.labelDesc.text = entity.subject
                pushCell.photoView.image = UIImage(named: "ic_launcher")
            }
            else {
                pushCell.labelTitle1.text = ""
                pushCell.labelTitle2.text = Str.screen_notifications_system_message
                pushCell.labelDesc.text = push.message
                pushCell.photoView.image = UIImage(named: "ic_launcher")
            }
        }
    }
    
    override open func itemDidClicked(_ push: Push, indexPath: IndexPath) {
        let isOwner = AppApi.isRegisteredAsOwner() //&& self.asset?.horseShop == AppApi.getOwnerShop()?.identityId
        switch push.type {
        case PUSH_TYPE_ORDER_STATUS_CHANGED:
            let orderVc = BAHOrderViewController()
            orderVc.load(buId: (push.entity as? OrderStatusChangedPushEntity)?.orderId, shop: nil, asMerchant: isOwner)
            self.navigationController?.pushViewController(orderVc, animated: true)
            break
            
        case PUSH_TYPE_ORDER_CHAT_MESSAGE:
            let orderVc = BAHOrderViewController()
            orderVc.load(buId: (push.entity as? OrderChatMessagePushEntity)?.orderId, shop: nil, asMerchant: isOwner)
            self.navigationController?.pushViewController(orderVc, animated: true)
            break
        case PUSH_TYPE_ASSET_CHANGED:
            if let controller = AppController.getViewControllerById(AppController.Asset, viewController: self) as? HKAssetViewProtocol {
                controller.loadAsset((push.entity as? AssetChangedPushEntity)?.assetId ?? "")
                self.navigationController?.pushViewController(controller as! UIViewController, animated: true)
                HonkioApi.userSetPush(true, filter: PushFilter(id: push.pushId), flags: MSG_FLAG_NO_WARNING, handler: nil)
            }
            break
        case PUSH_TYPE_SYSYEM:
            let entity = push.entity as! HKSystemMessagePushEntity
            CustomAlertView(okTitle: entity.subject, okMessage: entity.message ?? "null", okAction: nil).show()
            HonkioApi.userSetPush(true, filter: PushFilter(id: push.pushId), flags: MSG_FLAG_NO_WARNING, handler: nil)
            break
        default:
            HonkioApi.userSetPush(true, filter: PushFilter(id: push.pushId), flags: MSG_FLAG_NO_WARNING, handler: nil)
        }
        push.isReaded = true
        self.tableView.reloadData()
    }
    
    @objc open func openActionsMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        alertController.addAction(UIAlertAction(title: "action_mark_all_read".localized, style: .default) {
            [unowned self] action in
            self.markAllAsRead()
        })
        
        self.present(alertController, animated: true)
    }
    
    private func iconForStatus(_ status: String?) -> UIImage {
        if status == nil {
            return UIImage(named: "ic_launcher")!
        }
        
        return UIImage(named: "from_system_circle")!
    }

    override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
