//
//  MerchantHorseDetailsViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/17/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

open class MerchantHorseDetailsViewController: UITabBarController {

    var asset: HKAsset? {
        didSet {
            if let viewControllers = self.viewControllers {
                for viewController in viewControllers {
                    if let assetVC = viewController as? MerchantScheduleViewController {
                        assetVC.horse = self.asset
                    }
                    else if let assetVC = viewController as? HorseDetailsViewController {
                        assetVC.asset = self.asset
                    }
                }
            }
        }
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(self.actionMenu(_:)))
    }
    
    @IBAction func actionMenu(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        // settings
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        // edit horse details (run registration wizard)
        alertController.addAction(UIAlertAction(title: Str.menu_horse_details_edit, style: .default) {[unowned self] action in
            if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseRegistrationViewController.self), viewController: self) as? HorseRegistrationViewController {
                controller.horse = self.asset
                self.present(controller, animated: true)
            }
        })
        
        // share
        alertController.addAction(UIAlertAction(title: Str.menu_horse_details_share, style: .default) {[unowned self] action in
            if let assetId = self.asset?.assetId {
                ShareUtils.shareAsset(assetId, viewController: self)
            }
        })
        
        self.present(alertController, animated: true)
    }
    
}
