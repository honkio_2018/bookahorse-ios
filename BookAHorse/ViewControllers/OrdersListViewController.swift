//
//  OrdersListViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 19/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

class OrdersListViewController: HKBaseOrdersListViewController {

    let dateFormatter = DateFormatter()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 64
        tableView.tableFooterView = UIView()
        
        dateFormatter.setLocalizedDateFormatFromTemplate("d/M-y")
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    //MARK: - HONKIO
    override open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.queryUnreadMessages = true
        filter.queryProductDetails = true
    }
    
    override open func cellIdentifier(_ item: Order) -> String {
        return "OrderTableViewCell"
    }
    
    override open func buildCellBunder() -> HKTableViewItemInitializer {
        return { (viewController, cell, item) in
            (viewController as? OrdersListViewController)?.bindCell(cell as! OrderTableViewCell, order: item as! Order)
        }
    }
    
    open func bindCell(_ orderCell: OrderTableViewCell, order: Order) {
        orderCell.dateLabel.text = dateFormatter.string(from: order.startDate) //ApiDateFormatter.string(from: order.startDate, dateStyle: .short, time: .short)
        orderCell.titleLabel.text = order.title
        
        if !order.isStatus(Order.Status.bookRequest.rawValue) {
            orderCell.descLabel.text = order.statusName
        }
        else {
            orderCell.descLabel.text = ""
        }
        
        orderCell.imgHasMessages.isHidden = order.unreadChatMessagesCount == 0
        
        orderCell.photoView.setBorder(.small)
        orderCell.photoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + order.asset,
                               errorImage: UIImage(named: "ic_placeholder_horse"),
                               contentMode: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let order = self.lazyItem(indexPath) as? Order {
            order.unreadChatMessagesCount = 0
            tableView.reloadData()
            
            let controller = AppController.getViewControllerById(AppController.OrderDetails, viewController: self) as! OrderDetailsViewController
            controller.order = order
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
