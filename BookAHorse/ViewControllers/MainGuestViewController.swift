//
//  GuestViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/24/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import AVKit
import HonkioAppTemplate

class MainGuestViewController: UITabBarController, HKLoginViewLoginDelegate {
    
    @IBOutlet weak var loginButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        
        let image = UIImage(named: "logo_with_text")
        let logoImageView = UIImageView(image: image)
//        logoImageView.frame = CGRect(x: -40, y: 0, width: 150, height: 20)
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = image
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -25
        navigationItem.leftBarButtonItems = [negativeSpacer, imageItem]
//        self.navigationItem.titleView = imageView
        
        loginButtonItem.title = "login_btn_login".localized.uppercased()
    }
    
    @IBAction func actionLogin(_ sender:UIBarButtonItem!) {
        if let viewController = AppController.instance.getViewControllerById(AppController.Login, viewController: self) as? HKLoginViewProtocol {
            viewController.doLogin = false
            viewController.loginDelegate = self
            self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
        }
    }
    

    @IBAction func actionMenu(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_main_menu_help_video, style: .default) {
            [unowned self] action in
            let viewController = AVPlayerViewController()
            viewController.player = AVPlayer(url: URL(string: Str.screen_intro_video_link)!)
            self.navigationController?.pushViewController(viewController, animated: true)
        })
        
        self.present(alertController, animated: true)
    }
    
    
    func loginFinished(_ view: HKLoginViewProtocol, _ isLogedin: Bool) {
        if isLogedin {
            // Restart main screen if logedin
            if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
                
                let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                appDelegate.changeRootViewController(viewController)
            }
        }
        else if let delegate = view as? HKLoginViewLoginDelegate {
            delegate.loginFinished(view, isLogedin)
        }
    }
}
