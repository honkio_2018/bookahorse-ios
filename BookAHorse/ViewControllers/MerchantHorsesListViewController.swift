//
//  MerchantHorsesListViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 19/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MerchantHorsesListViewController: HKBaseAssetsListViewController, HorseRegistrationDelegate {

    @IBOutlet weak var addHorseButton: UIButton!
    @IBOutlet weak var emptyLabel: UILabel!
    
    var backgroundCellImage = UIImage(named: "bg_horse_no_photo")
    let formatter = MeasurementFormatter()
    let cellReuseIdentifier = String(describing: HorseListItemCell.self)
    
    // MARK: - HorseRegistrationDelegate protocol
    
    func horseDidCreated(_ horseAsset: HKAsset) {
        if let controller = AppController.instance.getViewControllerByIdentifier(
            String(describing: HorseDetailsViewController.self), viewController: self) as? HorseDetailsViewController {
            controller.asset = horseAsset
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 220
        tableView.tableFooterView = UIView()
        
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = .ceiling
        numberFormatter .maximumFractionDigits = 1
        formatter.numberFormatter = numberFormatter
        formatter.unitOptions = .naturalScale

        // register table cell view form nib file
        let horseListItemCellNib = UINib.init(nibName: cellReuseIdentifier, bundle: Bundle.main)
        tableView.register(horseListItemCellNib, forCellReuseIdentifier: cellReuseIdentifier)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MerchantHorsesListViewController.horseUpdate(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_ASSET_SET)),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self,
                                                      name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_ASSET_SET)),
                                                      object: nil)
        }
    }
    
    @IBAction func addHorseDidTapped(_ sender: UIButton) {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseRegistrationViewController.self), viewController: self) as? HorseRegistrationViewController {
            controller.registrationDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        HonkioMerchantApi.merchantAssetList(filter, shop:AppApi.getOwnerShop(), flags:0, handler:responseHandler)
    }
    
    @objc public func horseUpdate(_ notification: Notification) {
        self.refresh()
    }
    
    override func cellIdentifier(_ item: HKAsset) -> String {
        return cellReuseIdentifier
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    override func buildCellBunder() -> HKTableViewItemInitializer? {
            return { [unowned self] (viewController, cell, item) in
                guard
                    let assetCell = cell as? HorseListItemCell,
                    let asset = item as? HKAsset
                else {
                    assertionFailure("assetCell or asset is nil, cannot build table cell!")
                    return
                }
                
                assetCell.horseName.text = asset.name.uppercased()
                if !asset.isVisible {
                    assetCell.sheduleImage.isHidden = false
                    let visibleIcon = UIImage(named: "ic_visibility_off")!
                    assetCell.sheduleImage.image = visibleIcon
                    assetCell.sheduleImage.tintColor = UIColor(netHex: AppColors.appOrange)
                    assetCell.sheduleImage.contentMode = .scaleAspectFit
                    assetCell.scheduleImageWidthConstraint.constant = visibleIcon.size.width
                } else {
                    if asset.isHasValidSchedule {
                        assetCell.sheduleImage.isHidden = false
                        assetCell.sheduleImage.image = UIImage(named: "from_system")
                    }
                }
                
                if let defaultPicture = asset.properties?["default_picture"] {
                    assetCell.horseImage.imageFromURL(link: (defaultPicture as? String),
                                                      errorImage: self.backgroundCellImage,
                                                      contentMode: nil)
                } else {
                    assetCell.horseImage.image = self.backgroundCellImage
                }
                
                var result = [String]()
                // Add horse breed
                let horseBreed = AppApi.getHorseStructure()?.findEnumPropertyValue(HKAsset.Fields.horseBreed.rawValue,
                                                                                   value: asset.horseBreed)?.name ?? ""
                result.append(horseBreed)
                // Add address of horse
                if let address = asset.address, !address.admin.isEmpty {
                    result.append(address.city)
                }
                
                
                // Add distance info
                if let currentLocation = HonkioApi.deviceLocation() {
                    let horseLocation = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
                    let distance = currentLocation.distance(from: horseLocation)
                    let distanceInMeters = Measurement(value: distance, unit: UnitLength.meters)
                    result.append(self.formatter.string(from: distanceInMeters))
                }
                
                assetCell.horseInfo.text = result.filter({ !$0.isEmpty }).joined(separator: ", ")
        }
    }
    
    override func setupFilter(_ filter: AssetFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.structureIds = [AppApi.HORSE_STRUCTURE]
        filter.isQuerySchedule = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let asset = self.lazyItem(indexPath) as? HKAsset {
            if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: MerchantHorseDetailsViewController.self), viewController: self) as? MerchantHorseDetailsViewController {
                controller.asset = asset
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
