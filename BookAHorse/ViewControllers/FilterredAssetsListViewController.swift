//
//  FilterredAssetsListViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/26/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class FilterredAssetsListViewController: AssetsListViewController {
    
    enum LocationFilter: String {
        case around = "around"
        case custom = "custom"
        case map = "map"
    }
    
    enum CustomFilter: String {
        case all = "all"
        case suitable = "suitable"
    }
    
    private static let LocationFilterKey = "BookHorse_LocationFilter"
    private static let LocationLatKey = "BookHorse_LocationLat"
    private static let LocationLonKey = "BookHorse_LocationLon"
    private static let CustomFilterKey = "BookHorse_CustomFilter"
    
    @IBOutlet weak var btnFilterLocation: UIButton!
    @IBOutlet weak var btnFilterCustom: UIButton!
    @IBOutlet weak var emptyLabel: UILabel!
    
    private var locationFilter: LocationFilter = LocationFilter.init(rawValue: StoreData.load(fromStore: LocationFilterKey) as? String ?? LocationFilter.around.rawValue)! {
        didSet {
            StoreData.save(toStore: self.locationFilter.rawValue, forKey: FilterredAssetsListViewController.LocationFilterKey)
            
            self.refresh()
            self.updateFilterLabels()
        }
    }
    
    private var customFilter: CustomFilter = CustomFilter.init(rawValue: StoreData.load(fromStore: CustomFilterKey) as? String ?? CustomFilter.suitable.rawValue)! {
        didSet {
            StoreData.save(toStore: self.customFilter.rawValue, forKey: FilterredAssetsListViewController.CustomFilterKey)
                
            self.refresh()
            self.updateFilterLabels()
        }
    }
    
    private var customLatitude: String? = StoreData.load(fromStore: LocationLatKey) as? String {
        didSet {
            StoreData.save(toStore: self.customLatitude, forKey: FilterredAssetsListViewController.LocationLatKey)
        }
    }
    private var customLongitude: String? = StoreData.load(fromStore: LocationLonKey) as? String {
        didSet {
            StoreData.save(toStore: self.customLatitude, forKey: FilterredAssetsListViewController.LocationLonKey)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add icon image on right side from text title
        btnFilterLocation.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        btnFilterCustom.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        
        self.updateFilterLabels()
    }
    
    @IBAction func locationFilterAction(_ sender: Any?) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_horses_list_filter_location_device, style: .default) {
            [unowned self] action in
            self.locationFilter = .around
        })
        alertController.addAction(UIAlertAction(title: Str.screen_horses_list_filter_location_custom, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {
                viewController.readOnly = false
                viewController.completeAction = { [unowned self] (viewController, place, location) in
                    self.customLatitude = String(location?.coordinate.latitude ?? 0.0)
                    self.customLongitude = String(location?.coordinate.longitude ?? 0.0)
                    self.locationFilter = .custom
                    self.navigationController?.popViewController(animated: true)
                }
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        alertController.addAction(UIAlertAction(title: Str.screen_horses_list_filter_location_on_map, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerByIdentifier("HorsesMapViewController", viewController: self) as? HorsesMapViewController {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        self.present(alertController, animated: true)
    }
    
    @IBAction func customFilterAction(_ sender: Any?) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_horses_list_filter_suitable, style: .default) {
            [unowned self] action in
            self.customFilter = .suitable
        })
        alertController.addAction(UIAlertAction(title: Str.screen_horses_list_filter_all, style: .default) {
            [unowned self] action in
            self.customFilter = .all
        })
        
        self.present(alertController, animated: true)
    }
    
    override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    override func setupFilter(_ filter: AssetFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        
        switch self.locationFilter {
        case .around:
            // do nothing. Device location is already set in super.setupFilter
            break
        case .custom:
            filter.latitude = self.customLatitude
            filter.longitude = self.customLongitude
            filter.radius = nil
            break
        case .map:
            // do nothing. Map filter should show map screen
            break
        }
        
        switch self.customFilter {
        case .all:
            filter.propertiesFilter = nil
            break
        case .suitable:
            filter.propertiesFilter = HorseMatchingHelper.buildSuitableFilter(AppApi.riderRole(), filter.propertiesFilter)
            break
        }
    }
    
    private func updateFilterLabels() {
        switch self.locationFilter {
        case .around:
            btnFilterLocation.setTitle(Str.screen_horses_list_filter_location_device, for: .normal)
            break
        case .custom:
            btnFilterLocation.setTitle(Str.screen_horses_list_filter_location_custom, for: .normal)
            break
        case .map:
            btnFilterLocation.setTitle(Str.screen_horses_list_filter_location_on_map, for: .normal)
            break
        }
        
        switch self.customFilter {
        case .all:
            btnFilterCustom.setTitle(Str.screen_horses_list_filter_all, for: .normal)
            break
        case .suitable:
            btnFilterCustom.setTitle(Str.screen_horses_list_filter_suitable, for: .normal)
            break
        }
    }
}
