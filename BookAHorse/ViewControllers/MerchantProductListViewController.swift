//
//  MerchantHorseActivityViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 20/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MerchantProductListViewController: HKBaseMerchantProductListTableViewController {
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    let productItemCellName = "ProductItemCell"
    let currencyFormatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 61
        tableView.tableFooterView = UIView()
        
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.current
        
        // register table cell view form nib file
        let productItemCellNameNib = UINib.init(nibName: productItemCellName, bundle: Bundle.main)
        tableView.register(productItemCellNameNib, forCellReuseIdentifier: productItemCellName)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MerchantProductListViewController.productsUpdate(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_PRODUCT)),
                                               object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self,
                                                      name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_PRODUCT)),
                                                      object: nil)
        }
    }
    
    override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)

        HonkioMerchantApi.merchantProductList(filter, shop:AppApi.getOwnerShop(), flags:0, handler:responseHandler)
    }
    
    @objc public func productsUpdate(_ notification: Notification) {
        self.refresh()
    }

    @IBAction func addActivityDidTapped(_ sender: UIButton) {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: ProductDetailsViewController.self), viewController: self) as? ProductDetailsViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    open override func cellIdentifier(_ item: Product) -> String {
        return productItemCellName
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    override func buildCellBunder() -> (HKTableViewItemInitializer)? {
        return {(viewController, cell, item) in
            guard let productCell = cell as? ProductItemCell,
                let product = item as? Product
                else {
                    assertionFailure("productCell or product is nil, cannot build table cell!")
                    return
            }
            var duration: TimeInterval = 0.0
            duration.millisecond = product.duration
            
            var durationAsString: String = ""
            if duration.day != 0 {
                durationAsString += "\(duration.day) d."
            }
            
            if (duration.hour % 24) != 0 {
                durationAsString += "\(duration.hour % 24) h."
            }
            
            if duration.minute != 0 {
                durationAsString += "\(duration.minute) min."
            }
            productCell.labelName.text = "\(product.name ?? "-") (\(durationAsString))"
//            self.currencyFormatter.currencyCode = product.currency
//            productCell.labelAmount.text = self.currencyFormatter.string(from: NSNumber(value: product.amount))
            productCell.labelAmount.text = String(format: "%@ %@", String(format: "%g", product.amount), product.currency ?? "")
            productCell.labelDesc.text = product.desc
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let product = self.lazyItem(indexPath) as? Product {
            if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: ProductDetailsViewController.self), viewController: self) as? ProductDetailsViewController {
                controller.product = product
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    override open func setupFilter(_ filter: MerchantProductsFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
        if let owner = AppApi.apiInstance.ownerRole {
            filter.merchantId = owner.ownerMerchantId
        }
    }
    
    override func filterList(_ list: [Any?]) -> [Any?] {
        return list.filter({ (item) -> Bool in
            return (item as? Product)?.typeId == AppApi.PRODUCT_ID_TYPE
        })
    }
}
