//
//  MerchantScheduleViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 26/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class MerchantScheduleViewController: HKBaseScheduleTableViewController, ListItemEventCellDelegate {
    
    @IBOutlet weak var emptyLabel: UILabel!
    
    let ListItemEventCellName = "ListItemEventCell"
    let EventsSectionTitleCellName = "EventsSectionTitleCell"
    
    let dateFormatter = DateFormatter()
    
    open var horse: HKAsset? {
        didSet {
            if let horse = horse {
                filter.startDate = Date()
                filter.endDate = Calendar.current.date(byAdding: .day, value: 7, to: filter.startDate)!
                filter.objectId = horse.assetId
                filter.objectType = HK_OBJECT_TYPE_ASSET
                filter.queryType = "free"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Str.screen_horse_merchant_schedule_title
        
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        dateFormatter.setLocalizedDateFormatFromTemplate(HorseEventsListViewController.TIME_FORMAT)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 49
        tableView.tableFooterView = UIView()
        
        // register table cell view form nib file
        let listItemEventCellNameNib = UINib.init(nibName: ListItemEventCellName, bundle: Bundle.main)
        tableView.register(listItemEventCellNameNib, forCellReuseIdentifier: ListItemEventCellName)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MerchantScheduleViewController.eventUpdate(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_EVENT)),
                                               object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self,
                                                      name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_EVENT)),
                                                      object: nil)
        }
    }
    
    open override func toList(_ response : Response) -> [Any?] {
        let list = (response.result as! CalendarSchedule).list
        return list!
    }

    // Activate you horse did tapped
    @IBAction func addSchedule(_ sender: UIButton) {
        let event = CalendarEvent()
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: PostScheduleWizardViewController.self), viewController: self) as? PostScheduleWizardViewController {
            controller.event = event
            controller.horse = self.horse
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        let headerBinder = buildHeaderBunder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMM d")
        
        var group: String! = nil
        if list.count > 0 {
            if let firstItem = list.first as? Event {
                group = dateFormatter.string(from: firstItem.start)
                items.append((EventsSectionTitleCellName, group, headerBinder))
            }
        }
        
        for listItem in list as! [Event?] {
            let newGroup = dateFormatter.string(from: (listItem?.start)!)
            if !newGroup.elementsEqual(group)  {
                items.append((EventsSectionTitleCellName, newGroup, headerBinder))
                group = newGroup
                listItem?.type = "EventGroup"
            }
            items.append((ListItemEventCellName, listItem, itemBinder))
        }
        return items
    }
    
    @objc public func eventUpdate(_ notification: Notification) {
        self.refresh()
    }
    
    open override func cellIdentifier(_ item: Event) -> String {
        return ListItemEventCellName
    }
            
    override func buildCellBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            guard let itemEventCell = cell as? ListItemEventCell,
                let event = item as? CalendarEvent
                else {
                    assertionFailure("itemEventCell or event is nil, cannot build table cell!")
                    return
            }
            
            let dateFormatter = (viewController as! MerchantScheduleViewController).dateFormatter
            
            let timeStart = dateFormatter.string(from: event.start)
            if event.type == "EventGroup" {
                itemEventCell.titleLabel.text = "\(timeStart), \(CalendarEvent.getRepeatingNameStringId(event.repeating))"
            } else {
                let timeEnd = dateFormatter.string(from: event.end)
                itemEventCell.titleLabel.text = "\(timeStart) - \(timeEnd), \(CalendarEvent.getRepeatingNameStringId(event.repeating))"
            }
            
            itemEventCell.productsLabel.text = event.products.compactMap{ $0.name }.joined(separator:";")
            itemEventCell.delegate = viewController as? ListItemEventCellDelegate
        }
    }
    
    open func buildHeaderBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let sectionTitleCell = cell as! EventsSectionTitleCell
            
            sectionTitleCell.labelTitle.text = item as? String
            sectionTitleCell.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
        }
    }
    
    open override func setupFilter(_ filter: ScheduleFilter, count: Int, skip: Int) {
        // FIXME Schedule list currently not support paging
        // Do nothing
    }
    
    // MARK: - Delegate
    func actionBook(_ cell: ListItemEventCell) {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: PostScheduleWizardViewController.self), viewController: self) as? PostScheduleWizardViewController {
            controller.horse = self.horse
            controller.event = getEventFromCell(cell)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func getEventFromCell(_ cell: ListItemEventCell) -> CalendarEvent? {
        let indexPath = self.tableView.indexPath(for: cell)
        if let indexPath = indexPath {
            let section = self.itemsSource[(indexPath as IndexPath).section]
            let item = section.items[(indexPath as IndexPath).row]
            let event = item.item as? CalendarEvent
            return event
        }
        return nil
    }
    
    open override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
