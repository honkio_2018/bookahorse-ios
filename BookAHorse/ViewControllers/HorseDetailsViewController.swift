//
//  HorseDetailsViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseDetailsViewController: HKBaseTableViewController,
    HKAssetViewProtocol,
    HorseDetailsMediaItemCellDelegate,
    HKMediaPickViewControllerDelegate,
    ProcessProtocol,
    HorseDetailsVisibleCellDelegate,
    DetailsPhotoCellChildPhotoDelegate {
    
    private var assetId: String?
    var asset: HKAsset? {
        didSet {
            if self.assetId != asset?.assetId || self.shop == nil {
                HonkioApi.shopFind(byId: self.asset!.horseShop!, flags: 0) {[weak self] (response) in
                    if let list = (response.result as? ShopFind)?.list {
                        if list.count > 0 {
                            self?.shop = list[0]
                        }
                    }
                }
            }
            
            if self.assetId != asset?.assetId || self.ratings == nil {
                let rateFilter = RatingsFilter()
                rateFilter.object = self.asset?.assetId
                rateFilter.objectType = HK_OBJECT_TYPE_ASSET
                HonkioApi.getRate(rateFilter, flags: 0) {[weak self] (response) in
                    self?.ratings = response.result as? RateList
                }
            }
            
            if self.assetId != asset?.assetId || self.mediaFiles == nil {
                self.updateMediaList()
            }
            
            self.assetId = asset?.assetId
            
            self.initDataSource()
            self.tableView?.reloadData()
        }
    }
    
    private var shop: Shop? {
        didSet {
            if self.shop != nil {
                self.checkOrders(self.shop!)
            }
        }
    }
    
    private var isOrderExists: Bool? {
        didSet {
            self.initDataSource()
            
            // Can be called before view creation
            if self.tableView != nil {
                self.tableView.reloadData()
            }
        }
    }
    
    private var ratings: RateList? {
        didSet {
            self.initDataSource()
            self.tableView?.reloadData()
        }
    }
    
    private var mediaFiles: HKMediaFileList? {
        didSet {
            if self.asset?.horsePhoto == nil && self.isOwnerHorse {
                let mediaList = self.mediaFiles?.list
                let imageIndex = mediaList?.firstIndex(where: { (file) -> Bool in
                    file.url != nil && file.type == HK_MEDIA_FILE_TYPE_IMAGE
                })
                
                if (imageIndex ?? -1) >= 0 {
                    self.setAsHorsePicture(mediaList![imageIndex!])
                }
            }
            
            self.initDataSource()
            self.tableView?.reloadData()
        }
    }
    
    private var isOwnerHorse: Bool {
        return AppApi.isRegisteredAsOwner() && self.asset?.horseShop == AppApi.getOwnerShop()?.identityId
    }
    
    private let DetailsDoublePhotoCellIdentifier = "DetailsDoublePhotoCell"
    private let DetailsNameCellIdentifier = "DetailsNameCell"
    private let DetailsRatingCellIdentifier = "DetailsRatingCell"
    private let HorseDetailsVisibleCellIdentifier = "HorseDetailsVisibleCell"
    private let HorseDetailsMediaItemCellIdentifier = "HorseDetailsMediaItemCell"
    private let DetailItemCellIdentifier = "DetailItemCell"
    private let HorseDetailsTemperamentCellIdentifier = "HorseDetailsTemperamentCell"
    private let DetailsNumericRangeSliderCellIdentifier = "DetailsNumericRangeSliderCell"

    @IBOutlet weak var tableView: UITableView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add right button on navigation bar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(HorseDetailsViewController.actionMenu))
    
        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50

        // horse and owner photo cell
        self.tableView.register(UINib.init(nibName: DetailsDoublePhotoCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: DetailsDoublePhotoCellIdentifier)
        // horse name
        self.tableView.register(UINib.init(nibName: DetailsNameCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: DetailsNameCellIdentifier)
        // rating
        self.tableView.register(UINib.init(nibName: DetailsRatingCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: DetailsRatingCellIdentifier)
        // visible cell
        self.tableView.register(UINib.init(nibName: HorseDetailsVisibleCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: HorseDetailsVisibleCellIdentifier)
        
        self.tableView.register(UINib.init(nibName: HorseDetailsMediaItemCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: HorseDetailsMediaItemCellIdentifier)
        // main cell
        self.tableView.register(UINib.init(nibName: DetailItemCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: DetailItemCellIdentifier)
        // numeric range slider cell
        self.tableView.register(UINib.init(nibName: DetailsNumericRangeSliderCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: DetailsNumericRangeSliderCellIdentifier)
        // temperament slider cell
        self.tableView.register(UINib.init(nibName: HorseDetailsTemperamentCellIdentifier, bundle: nil),
                                forCellReuseIdentifier: HorseDetailsTemperamentCellIdentifier)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HorseDetailsViewController.horseUpdate(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_ASSET_SET)),
                                               object: nil)
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self,
                                                      name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_ASSET_SET)),
                                                      object: nil)
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.sourceItem(indexPath)?.identifierOrNibName == DetailsRatingCellIdentifier {
            if let viewController = AppController.instance.getViewControllerByIdentifier(String(describing: BaseRatingsViewController.self), viewController: self) as? BaseRatingsViewController {
                viewController.filter = FeedbackFilter()
                viewController.filter?.objectId = self.asset?.assetId
                viewController.filter?.objectType = HK_OBJECT_TYPE_ASSET
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else if let _ = self.sourceItem(indexPath)?.item as? HKAddress {
            actionShowLocation()
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func actionMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.menu_horse_details_abuse, style: .default) {
            [unowned self] action in
            self.showAbuseCommentDialog(Str.screen_horse_details_abuse_dialog_title,
                                        Str.screen_horse_details_abuse_dialog_comment_title,
                                        Str.screen_horse_details_abuse_dialog_comment_hint,
                                        Str.screen_horse_details_abuse_send,
                                        self.asset?.getId(),
                                        .asset)
        })
        
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        alertController.addAction(UIAlertAction(title: Str.menu_horse_details_share, style: .default) {
            [unowned self] action in
            if let assetId = self.assetId {
                ShareUtils.shareAsset(assetId, viewController: self)
            }
        })
        
        self.present(alertController, animated: true)
    }
    
    // Horse Edit Details Wizard Complete
    @objc public func horseUpdate(_ notification: Notification) {
        self.initDataSource()
        self.tableView?.reloadData()
    }
    
    // MARK: - HKAssetViewProtocol
    
    func loadAsset(_ assetId: String) {
        HonkioApi.userGetAsset(assetId, flags: 0) { [weak self] (response) in
            if response.isStatusAccept() {
                self?.asset = response.result as? HKAsset
            }
        }
    }
    
    func onMediaItemClick(_ cell: HorseDetailsMediaItemCell, _ item: HKMediaFile?) {
        if item != nil {
            let isOwner = AppApi.isRegisteredAsOwner() && self.asset?.horseShop == AppApi.getOwnerShop()?.identityId
            
            if isOwner {
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
                
                alertController.addAction(UIAlertAction(title: Str.menu_media_show, style: .default) {
                    [unowned self] action in
                    if let viewController = AppController.getViewControllerById(AppController.MediaFiles, viewController: self) as? HKMediaPagesViewDelegate {
                        viewController.mediaList = self.mediaFiles?.list
                        viewController.initialPage = self.mediaFiles?.list?.firstIndex(of: item!) ?? 0
                        self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
                    }
                })
                
                if item?.type == HK_MEDIA_FILE_TYPE_IMAGE {
                    alertController.addAction(UIAlertAction(title: Str.menu_media_set_as_profile_picture, style: .default) {
                        [unowned self] action in
                        self.setAsHorsePicture(item!)
                    })
                }
                
                alertController.addAction(UIAlertAction(title: Str.menu_media_remove_file, style: .default) {
                    [unowned self] action in
                    self.removeMediaFile(item!)
                })
                
                self.present(alertController, animated: true)
            }
            else if let viewController = AppController.getViewControllerById(AppController.MediaFiles, viewController: self) as? HKMediaPagesViewDelegate {
                viewController.mediaList = self.mediaFiles?.list
                viewController.initialPage = self.mediaFiles?.list?.firstIndex(of: item!) ?? 0
                self.navigationController?.pushViewController(viewController as! UIViewController, animated: true)
            }
        }
        else {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
            
            alertController.addAction(UIAlertAction(title: Str.screen_menu_media_upload_video_title, style: .default) {
                [unowned self] action in
                if let viewController = AppController.instance.getViewControllerByIdentifier(String(describing: MediaPickViewController.self), viewController: self) as? MediaPickViewController {
                    viewController.mediaType = HK_MEDIA_FILE_TYPE_VIDEO
                    viewController.pickDelegate = self
                    viewController.title = Str.screen_horse_media_title
                    viewController.labelReason.text = ""
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            })
            
            alertController.addAction(UIAlertAction(title: Str.screen_menu_media_upload_picture_title, style: .default) {
                [unowned self] action in
                if let viewController = AppController.instance.getViewControllerByIdentifier(String(describing: MediaPickViewController.self), viewController: self) as? MediaPickViewController {
                    viewController.mediaType = HK_MEDIA_FILE_TYPE_IMAGE
                    viewController.pickDelegate = self
                    viewController.title = Str.screen_horse_media_title
                    viewController.labelReason.text = ""
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            })
            
            self.present(alertController, animated: true)
        }
    }
    
    func imageDidPicked(_ viewController: HKBaseMediaPickViewController, _ image: UIImage) {
        self.navigationController?.popViewController(animated: true)
        BusyIndicator.shared.showProgressView(self.view)
        let uploadMediaModel = MediaUploadProcessModel(object: self.asset?.assetId, type: HK_OBJECT_TYPE_ASSET, data: UIImageJPEGRepresentation(image.fixRotation(), 0.85), mimeType: "image/jpeg")
        uploadMediaModel?.processDelegate = self
        uploadMediaModel?.start()
    }
    
    func mediaDidPicked(_ viewController: HKBaseMediaPickViewController, _ type: String, _ url: NSURL) {
        self.navigationController?.popViewController(animated: true)
        BusyIndicator.shared.showProgressView(self.view)
        let uploadMediaModel = MediaUploadProcessModel(object: self.asset?.assetId, type: HK_OBJECT_TYPE_ASSET, filePath: url.path)
        uploadMediaModel?.processDelegate = self
        uploadMediaModel?.start()
    }
    
    func onComplete(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        self.updateMediaList()
    }
    
    func onError(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        onError(response.anyError())
    }
    
    func onError(_ error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
    
    func childPhotoDidClick(_ cell: DetailsDoublePhotoCell, view: UIView) {
        if let controller = AppController.getViewControllerById(AppController.Shop, viewController: self) as? HKBaseShopViewController {
            if let identity = self.shop {
                controller.load(byIdentyty: identity)
            }
            else {
                controller.load(byId: self.asset!.horseShop!)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: Visibility
    func visibleButtonClick(_ cell: HorseDetailsVisibleCell) {
        if self.asset?.isVisible ?? false {
            self.setHorseVisibility(false)
        }
        else {
            self.checkHorseSchedule { [weak self] in
                self?.setHorseVisibility(true)
            }
        }
        //todo
    }
    
    @objc func actionShowLocation() {
        if let controller = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self) as? MapViewController {
            
            if let asset = self.asset {
                controller.location = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func setHorseVisibility(_ isVisible: Bool) {
        BusyIndicator.shared.showProgressView(self.view)
        
        let horse = HKAsset()!
        horse.assetId = self.asset?.assetId
        horse.latitude = self.asset?.latitude ?? 0.0
        horse.longitude = self.asset?.longitude ?? 0.0
        horse.isVisible = isVisible
        HonkioMerchantApi.merchantAssetSet(horse, shop: AppApi.getOwnerShop(), flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                self?.asset = response.result as? HKAsset
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    private func checkHorseSchedule(_ okAction: @escaping ()->Void) {
        BusyIndicator.shared.showProgressView(self.view)
        
        var dateComp = DateComponents()
        dateComp.day = 30
        
        let filter = ScheduleFilter()
        filter.objectId = self.asset?.assetId
        filter.objectType = HK_OBJECT_TYPE_ASSET
        filter.startDate = Date()
        filter.endDate = Calendar.current.date(byAdding: dateComp, to: filter.startDate)!
        HonkioApi.getSchedule(filter, shop: self.shop, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if (response.result as? CalendarSchedule)?.list.count ?? 0 > 0 {
                    okAction()
                }
                else {
                    CustomAlertView(okTitle: Str.screen_horse_details_change_visibility_message_header, okMessage: Str.screen_horse_details_schedule_not_set_message, okAction: nil).show()
                }
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    private func checkOrders(_ shop: Shop) {
        if HonkioApi.isConnected() && AppSettings.theme == ROLE_RIDER {
            let filter = OrderFilter()
            if let riderRole = AppApi.apiInstance.riderRole, riderRole.isChild {
                filter.thirdPerson = HonkioApi.activeUser()?.userId
            } else {
                filter.owner = HonkioApi.activeUser()?.userId
            }
            filter.isQueryOnlyCount = true
            filter.model = Order.Model.model.rawValue
            filter.status = [Order.Status.booked.rawValue, Order.Status.rideCompleted.rawValue].joined(separator: "|")
            HonkioApi.userGetOrders(filter, shop: shop, flags: 0) { [weak self] (response) in
                self?.isOrderExists = response.isStatusAccept() && ((response.result as? OrdersList)?.totalCount ?? 0) > 0
            }
        }
        else {
            self.isOrderExists = false
        }
    }
    
    // MARK: MediaFiles
    private func updateMediaList() {
        let mediaFilter = MediaFileFilter()
        mediaFilter.objectId = self.asset?.assetId
        mediaFilter.access = HK_MEDIA_FILE_ACCESS_PUBLIC
        
        HonkioApi.userMediaFilesList(mediaFilter, flags: 0) {[weak self]  (response) in
            self?.mediaFiles = response.result as? HKMediaFileList
        }
    }
    
    private func setAsHorsePicture(_ mediaFile: HKMediaFile) {
        self.asset?.horsePhoto = mediaFile.url
        HonkioMerchantApi.merchantAssetSet(self.asset, shop: AppApi.getOwnerShop(), flags: 0) { (response) in
            // Do nothing
        }
        self.tableView?.reloadData()
    }
    
    private func removeMediaFile(_ mediaFile: HKMediaFile) {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioApi.userClearMediaUrl(mediaFile.objectId, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if let index = self?.mediaFiles?.list.index(of: mediaFile) {
                    self?.mediaFiles?.list.remove(at: index)
                    self?.tableView.reloadData()
                }
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    // MARK: Data initialization
    private func initDataSource() {
        self.itemsSource.removeAll()
        
        guard self.asset != nil else { return }
        
        // Calculate hidable fields
        
        var ownerName: String = "..."
        var horseAddress: String = "-"
        
        if self.isOrderExists == true || self.asset?.horseOwner == HonkioApi.activeUser()?.userId {
            ownerName = self.shop?.name ?? "..."
            horseAddress = self.asset?.address?.fullAddress() ?? "-"
        }
        else if self.isOrderExists == false {
            ownerName = Str.screen_order_details_hidden_data
            horseAddress = self.asset?.address?.shortAddress() ?? "-"
        }
        else if self.isOrderExists == nil {
            horseAddress = self.asset?.address?.shortAddress() ?? "-"
        }
        
        
        let sectionHeader = HKTableViewSection()
        let sectionVisible = HKTableViewSection()
        let section = HKTableViewSection()

        // Horse and Owner Photo
        sectionHeader.items.append((DetailsDoublePhotoCellIdentifier, self.asset, { [unowned self] (viewController, cell, item) in
            let photoCell = cell as! DetailsDoublePhotoCell
            let asset = item as! HKAsset
            
            photoCell.mainPhotoView.setBorder(.large)
            if let horsePhoto = asset.horsePhoto {
                photoCell.mainPhotoView.imageFromURL(link: horsePhoto,
                                                     errorImage: UIImage(named: "ic_placeholder_horse"),
                                                     contentMode: nil)
            } else {
                photoCell.mainPhotoView.image = UIImage(named: "ic_placeholder_horse")
            }
            
            if !self.isOwnerHorse {
                photoCell.childPhotoView.setBorder(.small)
                var ownerPhotoUrl: String
                if let shopId = asset.horseShop {
                    ownerPhotoUrl = AppApi.MEDIA_URL_SHOP_PHOTO + shopId
                    photoCell.childPhotoView.imageFromURL(link: ownerPhotoUrl,
                                                          errorImage: UIImage(named: "ic_placeholder_owner"),
                                                          contentMode: nil)
                } else {
                    photoCell.childPhotoView.image = UIImage(named: "ic_placeholder_owner")
                }
                
                photoCell.childPhotoDelegate = viewController as? DetailsPhotoCellChildPhotoDelegate
            }
            
        }))

        // Horse Name
        sectionHeader.items.append((DetailsNameCellIdentifier, self.asset, {(viewController, cell, item) in
            let nameCell = cell as! DetailsNameCell
            let asset = item as! HKAsset
            
            nameCell.nameLabel.text = asset.horseName
            nameCell.isUserInteractionEnabled = false
        }))
        
        // Rating
        if self.ratings != nil {
            sectionHeader.items.append(("DetailsRatingCell", self.ratings, {(viewController, cell, item) in
                (cell as? DetailsRatingCell)?.showRating(item as? RateList)
            }))
        }
        else {
            sectionHeader.items.append(("ProgressCell", nil, nil))
        }
        
        self.itemsSource.append(sectionHeader)
        
        // Visible
        if isOwnerHorse {
            sectionVisible.items.append((HorseDetailsVisibleCellIdentifier, self.asset, {(viewController, cell, item) in
                let visibleCell = cell as! HorseDetailsVisibleCell
                let asset = item as! HKAsset

                visibleCell.visibleLabel.text = asset.isVisible ? "screen_horse_details_txt_visible".localized :
                    "screen_horse_details_txt_invisible".localized
                visibleCell.publishButton.setTitle(asset.isVisible ? "screen_horse_details_btn_visible_off".localized :
                    "screen_horse_details_btn_visible_on".localized, for: .normal)
                
                visibleCell.delegate = viewController as? HorseDetailsVisibleCellDelegate
            }))
            
            self.itemsSource.append(sectionVisible)
        }
        
        // Media files
        if let mediaFiles = self.mediaFiles, mediaFiles.list.count > 0 || isOwnerHorse {
            section.items.append(("HorseDetailsMediaCell", (media: self.mediaFiles, isOwnerHorse: isOwnerHorse), {(viewController, cell, item) in
                let mediaCell = cell as! HorseDetailsMediaCell
                let mediaItem = item as? (media: HKMediaFileList, isOwnerHorse: Bool)
                
                mediaCell.mediaFiles = mediaItem?.media
                mediaCell.isEditable = mediaItem?.isOwnerHorse ?? false
                mediaCell.delegate = viewController as? HorseDetailsMediaItemCellDelegate
            }))
        }
        
        // Owner Name
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_horse_details_owner_name, ownerName))
        
        // Birth Date
        var birthDateAsString = "-"
        if let birthDate = self.asset?.horseBirthDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .none
            birthDateAsString = dateFormatter.string(from: birthDate)
        }
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_horse_details_birth_date, birthDateAsString))
        
        // Breed
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_horse_details_breed, self.getNameFor(self.asset?.horseBreed, HKAsset.Fields.horseBreed.rawValue)))
        
        // Horse Type
        section.items.append((DetailItemCellIdentifier, self.asset, {[unowned self] (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = "screen_horse_details_kind".localized
            if let horseType = asset.horseType {
                itemCell.detailInfoLabel.text = self.getNameFor(horseType, HKAsset.Fields.horseType.rawValue)
                if (itemCell.detailInfoLabel.text?.isEmpty)! {
                    // for extra space
                    itemCell.detailInfoLabel.text = "-"
                }
            } else {
                // for extra space
                itemCell.detailInfoLabel.text = "-"
            }
        }))
        
        // Riding group
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = Str.screen_horse_details_riding_group
            if let horseCompetenceLevel = asset.horseCompetenceLevel {
                itemCell.detailInfoLabel.text = self.getNameFor(horseCompetenceLevel, HKAsset.Fields.horseCompetenceLevel.rawValue)
            } else {
                itemCell.detailInfoLabel.text = "-"
            }
        }))
        
        // Competed
        section.items.append((DetailItemCellIdentifier, self.asset, {[unowned self] (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
        
            itemCell.detailNameLabel.text = "screen_horse_details_competed".localized
            if let horseCompeted = asset.horseCompeted {
                itemCell.detailInfoLabel.text = self.getNameFor(horseCompeted, HKAsset.Fields.horseCompetitionField.rawValue)
                if (itemCell.detailInfoLabel.text?.isEmpty)! {
                    // for extra space
                    itemCell.detailInfoLabel.text = "-"
                }
            } else {
                // for extra space (label has height else height equal zero)
                itemCell.detailInfoLabel.text = "-"
            }
        }))
        
        // Years horse been riding
        section.items.append((DetailItemCellIdentifier, self.asset, {(viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            let been = AppApi.getHorseStructure()?.findEnumPropertyValue(HKAsset.Fields.horseYearsBeenRiding.rawValue,
                                                                   value: asset.horseYearsBeenRiding)
            itemCell.detailNameLabel.text = "screen_horse_details_been_riding".localized
            itemCell.detailInfoLabel.text = been != nil ? been?.name : "-"
        }))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        // Description of horse
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = "screen_horse_details_description".localized
            if !HonkioApi.isConnected() {
                itemCell.detailInfoLabel.text = Str.screen_order_details_hidden_data
            } else if let horseDescription = asset.horseDescription {
                itemCell.detailInfoLabel.text = horseDescription
            } else {
                itemCell.detailInfoLabel.text = "-"
            }
        }))
        
        // Insured
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = Str.screen_horse_details_insured
            itemCell.detailInfoLabel.text = asset.horseInsured ?? false
                ? Str.screen_horse_details_yes
                : Str.screen_horse_details_no
        }))
        
        // Rider weight
        section.items.append( (DetailsNumericRangeSliderCellIdentifier,
                               (detailName: "screen_horse_details_rider_weight".localized,
                                min: self.asset?.horseRiderWeightMin,
                                max: self.asset?.horseRiderWeightMax,
                                minFieldName: HKAsset.Fields.horseRiderWeightMin.rawValue,
                                maxFieldName: HKAsset.Fields.horseRiderWeightMax.rawValue
            ),
                               self.buildNumericRangeSliderCellBunder()) )
        
        // Rider age
        section.items.append( (DetailsNumericRangeSliderCellIdentifier,
                               (detailName: "screen_horse_details_rider_age".localized,
                                min: self.asset?.horseRiderAgeMin,
                                max: self.asset?.horseRiderAgeMax,
                                minFieldName: HKAsset.Fields.horseRiderAgeMin.rawValue,
                                maxFieldName: HKAsset.Fields.horseRiderAgeMax.rawValue
                               ),
                              self.buildNumericRangeSliderCellBunder()) )

        // Rider Length
        section.items.append( (DetailsNumericRangeSliderCellIdentifier,
                               (detailName: "screen_horse_details_rider_length".localized,
                                min: self.asset?.horseRiderLengthMin,
                                max: self.asset?.horseRiderLengthMax,
                                minFieldName: HKAsset.Fields.horseRiderLengthMin.rawValue,
                                maxFieldName: HKAsset.Fields.horseRiderLengthMax.rawValue
                               ),
                               self.buildNumericRangeSliderCellBunder()) )
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        // Location
        section.items.append((DetailItemCellIdentifier, horseAddress, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let address = item as? String
            
            itemCell.detailNameLabel.text = "screen_horse_details_horse_address".localized
            itemCell.detailInfoLabel.text = address ?? "-"
        }))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        // Temperament of the Horse
        section.items.append((HorseDetailsTemperamentCellIdentifier, self.asset, {(viewController, cell, item) in
            let itemCell = cell as! HorseDetailsTemperamentCell
            let asset = item as! HKAsset
            
            let horseTemperament = AppApi.getHorseStructure()?.properties?[HKAsset.Fields.horseTemperament.rawValue] as! HKStructureNumericProperty
            
            itemCell.temperametSlider.minimumValue = Float(horseTemperament.min)
            itemCell.temperametSlider.maximumValue = Float(horseTemperament.max)
            if let horseTemperament = asset.horseTemperament {
                itemCell.temperametSlider.value = Float(horseTemperament)
            } else {
                itemCell.temperametSlider.value = itemCell.temperametSlider.minimumValue
            }
            itemCell.isUserInteractionEnabled = false
        }))
        
        // Afraid of
        section.items.append((DetailItemCellIdentifier, self.asset, { [unowned self] (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = Str.screen_horse_details_afraid
            if let horseAfraid = asset.horseAfraid {
                if let name = self.getNameFor(horseAfraid, HKAsset.Fields.horseAfraid.rawValue) {
                    itemCell.detailInfoLabel.text = name.isEmpty ? "-" : name
                } else {
                    itemCell.detailInfoLabel.text = "-"
                }
            } else {
                itemCell.detailInfoLabel.text = "-"
            }
        }))
        
        // Other fears
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = "screen_horse_details_other_fear".localized
            itemCell.detailInfoLabel.text = "-"
            if let horseOtherFear = asset.horseOtherFear {
                if !horseOtherFear.isEmpty {
                    itemCell.detailInfoLabel.text = horseOtherFear
                }
            }
        }))
        
        // Lesson type
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = Str.screen_rider_details_lesson_type
            itemCell.detailInfoLabel.text = AppApi.getHorseStructure()?
                    .findEnumPropertyValue(HKAsset.Fields.lessonType.rawValue, value: asset.lessonType)?
                    .name ?? "-"
        }))
        
        // Type of Riding
        section.items.append((DetailItemCellIdentifier, self.asset, { (viewController, cell, item) in
            let itemCell = cell as! DetailItemCell
            let asset = item as! HKAsset
            
            itemCell.detailNameLabel.text = Str.screen_rider_details_type_of_riding
            itemCell.detailInfoLabel.text = AppApi.getHorseStructure()?
                .findEnumPropertyValue(HKAsset.Fields.typeOfRiding.rawValue, value: asset.typeOfRiding)?
                .name ?? "-"
        }))
        
        self.itemsSource.append(section)
    }
    
    private func buildNumericRangeSliderCellBunder() -> HKTableViewItemInitializer {
        return { (viewController, cell, item) in
            let rangeCell = cell as! DetailsNumericRangeSliderCell
            let data = item as! (detailName: String, min: Int?, max: Int?, minFieldName: String, maxFieldName: String)
            
            let minimumValue = AppApi.getHorseStructure()?.properties?[data.minFieldName] as! HKStructureNumericProperty
            let maximumValue = AppApi.getHorseStructure()?.properties?[data.maxFieldName] as! HKStructureNumericProperty
            rangeCell.valueRangeSlider.setMinMaxValue(CGFloat(minimumValue.min), maxValue: CGFloat(maximumValue.max))
//            rangeCell.valueRangeSlider.knobSize = 0.0
            
            var leftValue: CGFloat = 0.0
            var rightValue: CGFloat = 0.0
            
            rangeCell.detailNameLabel.text = data.detailName
            if let minValue = data.min {
                rangeCell.minValueLabel.text = String(minValue)
                leftValue = CGFloat(minValue)
            }
            
            if let maxValue = data.max {
                rangeCell.maxValueLabel.text = String(maxValue)
                rightValue = CGFloat(maxValue)
            } else {
                rangeCell.maxValueLabel.text = "0"
            }
            
            rangeCell.valueRangeSlider.setValue(CGFloat(leftValue), right: CGFloat(rightValue))
            rangeCell.valueRangeSlider.rightThumbImage = nil
            rangeCell.valueRangeSlider.leftThumbImage = nil
            
            rangeCell.isUserInteractionEnabled = false
        }
    }
    
    private func getNameFor(_ values: [String]?, _ field: String) -> String? {
        if values == nil {
            return "-"
        }
        
        var result: String = ""
        for value in values!.sorted() {
            let enumItem: HKStructureEnumPropertyValue? = AppApi.getHorseStructure()?.findEnumPropertyValue(field, value: value)
            if let enumItem = enumItem {
                if result.count > 0 {
                    result += "\n"
                }
                result += enumItem.name
            }
        }
        
        if result.count > 0 {
            return result.trimmingCharacters(in: .whitespaces)
            // cut new line symbol in begining variable result
//            let index = result.index(result.startIndex, offsetBy: 1)
//            return String(result[index...])
        }
        
        return result
    }
    
    private func getNameFor(_ value: String?, _ field: String) -> String? {
        if value == nil {
            return "-"
        }
        return AppApi.getHorseStructure()?.findEnumPropertyValue(field, value: value)?.name
    }
}
