//
//  CustomPinViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 9/21/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class CustomPinViewController: HKPinViewController {

    override open func viewDidLoad() {
        
        super.viewDidLoad()
        
        for view in dotView.subviews {
            view.backgroundColor = UIColor.white
        }
    }
    
    open override func setDotActive(_ dotNumber: Int, isActive:Bool) {
        if let view = self.dotView.viewWithTag(dotNumber) {
            if isActive {
                view.backgroundColor = view.tintColor
            }
            else {
                view.backgroundColor = UIColor.white
            }
        }
    }
}
