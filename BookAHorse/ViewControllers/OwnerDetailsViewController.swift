//
//  OwnerDetailsViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 13/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate
import MobileCoreServices

class OwnerDetailsViewController: HKBaseTableViewController, DetailsPhotoCellMainPhotoDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ProcessProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    var shopInfo: ShopInfo? {
        didSet {
            let filter = RatingsFilter()
            filter.object = self.shopInfo?.shop.identityId
            filter.objectType = HK_OBJECT_TYPE_SHOP
            HonkioApi.getRate(filter, flags: 0) { [weak self] (response) in
                self?.ratings = response.result as? RateList
            }
            
            self.updateDataSource()
            self.tableView.reloadData()
        }
    }
    
    private var ratings: RateList? {
        didSet {
            self.updateDataSource()
            self.tableView.reloadData()
        }
    }
    
    var isCurrentUser: Bool {
        return AppApi.getOwnerShop() != nil && (shopInfo?.shop.identityId == AppApi.getOwnerShop()?.identityId)
    }
    
    var isHasOrders: Bool? = nil
    
    var isUserVisible: Bool {
        return isCurrentUser || isHasOrders ?? false
    }
    
    var userPhoto: UIImage?
    
    let DetailsButtonCellIdentifier = String(describing: DetailsButtonCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = self.sourceItem(indexPath)
        if item?.identifierOrNibName == "DetailsRatingCell" {
            if let viewController = AppController.instance.getViewControllerByIdentifier("BaseRatingsViewController", viewController: self) as? BaseRatingsViewController {
                viewController.filter = FeedbackFilter()
                viewController.filter?.objectId = self.shopInfo?.shop.identityId
                viewController.filter?.objectType = HK_OBJECT_TYPE_SHOP
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else if item?.identifierOrNibName == DetailsButtonCellIdentifier {
            self.actionViewAllHorses()
        }
    }
    
    func updateDataSource() {
        self.itemsSource.removeAll()
        
        let section = HKTableViewSection()
        
        section.items.append(("DetailsPhotoCell", AppApi.MEDIA_URL_SHOP_PHOTO + (self.shopInfo?.shop.identityId)!, {(viewController, cell, item) in
            let photoCell = (cell as? DetailsPhotoCell)
            photoCell?.setLargeImage()
            photoCell?.mainPhotoView.imageFromURL(link: item as? String,
                                                  errorImage: UIImage(named: "ic_placeholder_owner"),
                                                  contentMode: nil,
                                                  isCache: false)
            
            photoCell?.mainPhotoView.setBorder(.large)
            photoCell?.mainPhotoDelegate = viewController as? DetailsPhotoCellMainPhotoDelegate
        }))
        
        section.items.append(("DetailsNameCell", self.isUserVisible ? self.shopInfo?.shop.name : Str.screen_owner_details_hidden_data, {(viewController, cell, item) in
            (cell as? DetailsNameCell)?.nameLabel.text = item as? String
        }))
        
        if self.ratings == nil {
            section.items.append(("ProgressCell", nil, nil))
        } else {
            section.items.append(("DetailsRatingCell", self.ratings, {(viewController, cell, item) in
                (cell as? DetailsRatingCell)?.showRating(item as? RateList)
            }))
        }
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_email_title, self.isUserVisible ? self.shopInfo?.merchant.supportEmail : Str.screen_owner_details_hidden_data))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_address, self.isUserVisible ? self.shopInfo?.shop.address : Str.screen_owner_details_hidden_data))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_phone_title, self.isUserVisible ? self.shopInfo?.merchant.supportPhone : Str.screen_owner_details_hidden_data))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_presentation_title, self.isUserVisible ? self.shopInfo?.shop.description : Str.screen_owner_details_hidden_data))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        let ownCompetenceLevel = UserRole.Fields.ownCompetenceLevel.rawValue
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_tuition_title, getNameFor(self.shopInfo?.shop.getProperty(UserRole.Fields.strOwnCompetenceLevel.rawValue) as? String, ownCompetenceLevel)))
        let propertyList = self.shopInfo?.shop.getProperty(UserRole.Fields.listCompetenceLevel.rawValue)
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_owner_details_skill_level, getNameFor(propertyList as? [String], UserRole.Fields.competenceLevel.rawValue)))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        if isCurrentUser  {
            showOwnerData(section)
        } else if isHasOrders == nil {
            checkOrders(section)
        }
        
        // button view all horses
        section.items.append((DetailsButtonCellIdentifier, nil, { [unowned self] (viewController, cell, item) in
            let detailsCell = cell as! DetailsButtonCell
            
            detailsCell.detailButton.titleLabel?.text = Str.screen_owner_details_button_view_horses_title
            detailsCell.detailButton.addTarget(self, action: #selector(OwnerDetailsViewController.actionViewAllHorses), for: UIControlEvents.touchUpInside)
        }))
        
        self.itemsSource.append(section)
    }
    
    private func showOwnerData(_ section: HKTableViewSection) {
        section.items.append(DetailItemCell
            .buildSourceItem("screen_owner_details_company_id_title".localized, AppApi.getOwnerMerchant()?.businessId))
        section.items.append(DetailItemCell
            .buildSourceItem("screen_owner_details_bank_iban_title".localized, AppApi.getOwnerMerchant()?.bankIban))
        section.items.append(DetailItemCell
            .buildSourceItem("screen_owner_details_bank_swift_title".localized, AppApi.getOwnerMerchant()?.bankSwift))
        section.items.append(DetailItemCell
            .buildSourceItem("screen_owner_details_vat_title".localized, AppApi.getOwnerMerchant()?.vat))
        section.items.append(DetailItemCell
            .buildSourceItem("screen_owner_details_vatnr_title".localized, AppApi.getOwnerMerchant()?.vatNumber))
    }
    
    private func checkOrders(_ section: HKTableViewSection) {
        if HonkioApi.isConnected() && AppSettings.theme == ROLE_RIDER {
            let filter = OrderFilter()
            if let _ = AppApi.apiInstance.riderRole?.isChild {
                filter.thirdPerson = HonkioApi.activeUser()?.userId
            } else {
                filter.owner = HonkioApi.activeUser()?.userId
            }
            filter.isQueryOnlyCount = true
            filter.model = Order.Model.model.rawValue
            filter.status = Order.Status.rideCompleted.rawValue + "|" + Order.Status.booked.rawValue
            HonkioApi.userGetOrders(filter, shop: self.shopInfo?.shop, flags: 0) { [weak self] (response) in
                self?.isHasOrders = response.isStatusAccept() &&
                    ((response.result as? OrdersList)?.totalCount ?? 0) > 0
                
                if self?.isHasOrders ?? false {
                    self?.updateDataSource()
                    self?.tableView.reloadData()
                }
            }
        }
        else {
            self.isHasOrders = false
        }
    }
    
    @IBAction func actionViewAllHorses() {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: AssetsListViewController.self), viewController: self) as? AssetsListViewController {
            controller.filter.structureIds = [AppApi.HORSE_STRUCTURE]
            controller.filter.merchantId = self.shopInfo?.merchant.merchantId
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func getNameFor(_ values: [String]?, _ field: String) -> String? {
        if values == nil {
            return "-"
        }
        
        var result: String = ""
        for value in values!.sorted() {
            let enumItem: HKStructureEnumPropertyValue? = AppApi.getOwnerStructure()?.findEnumPropertyValue(field, value: value)
            if let enumItem = enumItem {
                if result.count > 0 {
                    result += "\n"
                }
                result += enumItem.name
            }
        }
        
        return result
    }
    
    private func getNameFor(_ value: String?, _ field: String) -> String? {
        if value == nil {
            return "-"
        }
        return AppApi.getOwnerStructure()?.findEnumPropertyValue(field, value: value)?.name
    }
    
    func mainPhotoDidClick(_ cell: DetailsPhotoCell, view: UIView) {
        guard let shop = AppApi.getOwnerShop() else {
            return
        }
        
        if shop.identityId != self.shopInfo?.shop.identityId {
            return
        }
        
        // let tappedImage = tapGestureRecognizer.view as! UIImageView
        let alertController = UIAlertController(title: Str.media_picker_dialog_title, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: Str.media_picker_from_camera, style: .default) {
            [unowned self] action in
            self.pickAction(.camera)
        })
        
        alertController.addAction(UIAlertAction(title: Str.media_picker_from_folder, style: .default) {
            [unowned self] action in
            self.pickAction(.photoLibrary)
        })
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        self.present(alertController, animated: true)
    }
    
    open func pickAction(_ sourceType: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let viewController = UIImagePickerController()
            viewController.delegate = self
            viewController.sourceType = sourceType
            viewController.allowsEditing = true
            viewController.mediaTypes = [kUTTypeImage as String]
            
            self.present(viewController, animated: true, completion: nil)
            
        } else {
            switch sourceType {
            case .camera:
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Camera Unavailable".localized, okAction: nil).show()
                break
            case .photoLibrary:
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Unable to find a photo album on your device".localized, okAction: nil).show()
                break
            default:
                preconditionFailure("Provided source type not supported")
                break
            }
        }
    }
    
    @objc open func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        
        if let _ = info[UIImagePickerControllerMediaType] as? String {
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                self.imageDidPicked(image)
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.imageDidPicked(image)
            }
        }
    }
    
    open func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imageDidPicked(_ image: UIImage) {
        BusyIndicator.shared.showProgressView(self.view)
        let uploadMediaModel = MediaUploadProcessModel(object: self.shopInfo?.shop?.identityId, type: HK_OBJECT_TYPE_SHOP, data: UIImageJPEGRepresentation(image.fixRotation(), 0.85), mimeType: "image/jpeg")
        uploadMediaModel?.processDelegate = self
        uploadMediaModel?.start()
    }
    
    func onComplete(_ model: BaseProcessModel!, response: Response!) {
        let shop = MerchantShop(from: AppApi.getOwnerShop())
        
        shop?.settings = [ MERCHANT_SHOP_FIELD_LOGO_LARGE : response.url ]
        shop?.resetGeneralFields()
        
        HonkioMerchantApi.merchantSetShop(shop, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if let shop = response.result as? MerchantShop {
                    AppApi.apiInstance.onShopGet(shop: shop)
                    self?.shopInfo?.shop.logoLarge = shop.getProperty(MERCHANT_SHOP_FIELD_LOGO_LARGE) as? String
                }
                self?.updateDataSource()
                self?.tableView.reloadData()
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    func onError(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        onError(response.anyError())
    }
    
    func onError(_ error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
}
