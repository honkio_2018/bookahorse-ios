//
//  RiderDetailsViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/9/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import HonkioAppTemplate

class RiderDetailsViewController: HKBaseTableViewController, HorseDetailsMediaItemCellDelegate, HKMediaPickViewControllerDelegate, ProcessProtocol, DetailRiderVideoCellDelegate, HKWizardViewControllerCompletionDelegate, DetailsPhotoCellMainPhotoDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var userRoleId: String?
    var userRole: UserRole? {
        didSet {
            guard let userRole = self.userRole else { return }
            
            if userRole.objectId.isEmpty {
                loadRole(userRole.roleId, user: userRole.userId)
            }
            
            if self.userRoleId != userRole.objectId || self.ratings == nil {
                
                let rateFilter = RatingsFilter()
                rateFilter.object = userRole.objectId
                rateFilter.objectType = HK_OBJECT_TYPE_USER_ROLE
                HonkioApi.getRate(rateFilter, flags: 0) {[weak self] (response) in
                    self?.ratings = response.result as? RateList
                }
            }
            
            if self.userRoleId != userRole.objectId || self.mediaFiles == nil {
                
                let mediaFilter = MediaFileFilter()
                mediaFilter.objectId = userRole.objectId
                mediaFilter.mediaType = HK_MEDIA_FILE_TYPE_VIDEO
                mediaFilter.access = HK_MEDIA_FILE_ACCESS_PUBLIC
                
                HonkioApi.userMediaFilesList(mediaFilter, flags: 0) {[weak self]  (response) in
                    self?.mediaFiles = response.result as? HKMediaFileList
                }
            }
            
            self.userRoleId = userRole.objectId
            
            self.updateDataSource()
            self.tableView.reloadData()
        }
    }
    
    private var ratings: RateList? {
        didSet {
            self.updateDataSource()
            self.tableView.reloadData()
        }
    }
    
    private var mediaFiles: HKMediaFileList? {
        didSet {
            self.updateDataSource()
            self.tableView.reloadData()
        }
    }
    
    let dateFormatter: DateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        
        // add right button on navigation bar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_main_menu"),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(RiderDetailsViewController.actionMenu))
    }
    
    public func loadRole(_ roleId: String, user: String) {
        HonkioApi.userGetRole(roleId, userId: user, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                self?.userRole = response.result as? UserRole
            }
            else if let strongSelf = self {
                let error = response.anyError()
                if !AppErrors.handleError(strongSelf, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.sourceItem(indexPath)?.identifierOrNibName == "DetailsRatingCell" {
            if let viewController = AppController.instance.getViewControllerByIdentifier("BaseRatingsViewController", viewController: self) as? BaseRatingsViewController {
                viewController.filter = FeedbackFilter()
                viewController.filter?.objectId = self.userRole?.objectId
                viewController.filter?.objectType = HK_OBJECT_TYPE_USER_ROLE
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func mainPhotoDidClick(_ cell: DetailsPhotoCell, view: UIView) {
        guard let role = AppApi.riderRole() else {
            return
        }
        
        if role.objectId != self.userRole?.objectId {
            return
        }
        
        let alertController = UIAlertController(title: Str.media_picker_dialog_title, message: nil, preferredStyle: .actionSheet)
        
        alertController.addAction(UIAlertAction(title: Str.media_picker_from_camera, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.pickImage(.camera)
        }))
        
        alertController.addAction(UIAlertAction(title: Str.media_picker_from_folder, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.pickImage(.photoLibrary)
        }))
        
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: MediaFiles
    private func updateMediaList() {
        let mediaFilter = MediaFileFilter()
        mediaFilter.objectId = userRole?.objectId
        mediaFilter.access = HK_MEDIA_FILE_ACCESS_PUBLIC
        
        HonkioApi.userMediaFilesList(mediaFilter, flags: 0) {[weak self]  (response) in
            self?.mediaFiles = response.result as? HKMediaFileList
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - HorseDetailsMediaItemCellDelegate
    func actionRuderVideoEdit(_ cell: DetailRiderVideoCell) {
        if self.mediaFiles?.list.count ?? 0 == 0 {
            if self.userRole?.userId == HonkioApi.activeUser()?.userId {
                // upload video
                if let viewController = AppController.instance.getViewControllerByIdentifier(String(describing: MediaPickViewController.self), viewController: self) as? MediaPickViewController {
                    viewController.mediaType = HK_MEDIA_FILE_TYPE_VIDEO
                    viewController.pickDelegate = self
                    viewController.title = Str.screen_rider_media_title
                    viewController.labelReason.text = Str.screen_rider_details_upload_video
                    
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
        else {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
            
            alertController.addAction(UIAlertAction(title: Str.menu_media_show, style: .default) {[unowned self] action in
                if let url = self.mediaFiles?.list?[0].url {
                    let videoURL = URL(string: url)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = AVPlayer(url: videoURL!)
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                }
            })
            
            alertController.addAction(UIAlertAction(title: Str.menu_media_remove_file, style: .default) {
                [unowned self] action in
                self.removeMediaFile((self.mediaFiles?.list?[0])!)
            })
            
            self.present(alertController, animated: true)
        
        }
    }
    
    private func removeMediaFile(_ mediaFile: HKMediaFile) {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioApi.userClearMediaUrl(mediaFile.objectId, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if let index = self?.mediaFiles?.list.index(of: mediaFile) {
                    self?.mediaFiles?.list.remove(at: index)
                    self?.updateDataSource()
                    self?.tableView.reloadData()
                }
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    // MARK: - HorseDetailsMediaItemCellDelegate
    func onMediaItemClick(_ cell: HorseDetailsMediaItemCell, _ item: HKMediaFile?) {
        
    }
    
    // MARK: - HKMediaPickViewControllerDelegate
    func imageDidPicked(_ viewController: HKBaseMediaPickViewController, _ image: UIImage) {
    }
    
    func mediaDidPicked(_ viewController: HKBaseMediaPickViewController, _ type: String, _ url: NSURL) {
        self.navigationController?.popViewController(animated: true)
        BusyIndicator.shared.showProgressView(self.view)
        let uploadMediaModel = MediaUploadProcessModel(object: userRole?.objectId, type: HK_OBJECT_TYPE_USER_ROLE, filePath: url.path)
        uploadMediaModel?.processDelegate = self
        uploadMediaModel?.start()
    }
    
    func onComplete(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        self.updateMediaList()
    }
    
    func onError(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        onError(response.anyError())
    }
    
    func onError(_ error: ApiError) {
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
    
    func onWizardComplete(_ viewController: UIViewController, tag: String?, wizardObject: AnyObject?) {
        viewController.dismiss(animated: true) { [unowned self] in
            BusyIndicator.shared.showProgressView(self.view)
            HonkioApi.userSetRole(wizardObject as! UserRole, shop: nil, flags: 0) {[weak self] (response) in
                BusyIndicator.shared.hideProgressView()
                if response.isStatusAccept() {
                    self?.userRole = response.result as? UserRole
                }
                else if let strongSelf = self {
                    let error = response.anyError()
                    if !AppErrors.handleError(strongSelf, error: error) {
                        CustomAlertView(apiError: error, okAction: nil).show()
                    }
                }
            }
        }
    }
    
    private func updateDataSource() {
        self.itemsSource.removeAll()
        
        let section = HKTableViewSection()
        
        section.items.append(("DetailsPhotoCell", AppApi.MEDIA_URL_USER_PHOTO + self.userRole!.userId, { (viewController, cell, item) in
            let photoCell = (cell as? DetailsPhotoCell)
            
            photoCell?.mainPhotoView.imageFromURL(link: item as? String,
                                                  errorImage: UIImage(named: "ic_placeholder_rider"),
                                                  contentMode: nil,
                                                  isCache: false)
            photoCell?.mainPhotoView.setBorder(.large)
            photoCell?.mainPhotoDelegate = viewController as? DetailsPhotoCellMainPhotoDelegate
        }))
        
        section.items.append(("DetailsNameCell", self.userRole?.riderName, {(viewController, cell, item) in
            (cell as? DetailsNameCell)?.nameLabel.text = item as? String
        }))
        
        if self.ratings != nil {
            section.items.append(("DetailsRatingCell", self.ratings, {(viewController, cell, item) in
                (cell as? DetailsRatingCell)?.showRating(item as? RateList)
            }))
        }
        else {
            section.items.append(("ProgressCell", nil, nil))
        }
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_email_title, self.userRole?.email))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_phone_title, self.userRole?.phone))
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_address, self.userRole?.riderAddress?.fullAddress()))
        
        if self.mediaFiles != nil {
            var message =  Str.screen_rider_details_video_title
            if self.mediaFiles?.list.count ?? 0 == 0 {
                message = self.userRole?.userId == HonkioApi.activeUser()?.userId
                    ? Str.screen_rider_details_upload_video_title   // Please upload video
                    : Str.screen_rider_details_no_video_title       // Video not uploaded by rider
            }
            section.items.append(("DetailRiderVideoCell", message, {(viewController, cell, item) in
                let videoCell = cell as! DetailRiderVideoCell
                videoCell.labelText.text = item as? String
                if item as? String != Str.screen_rider_details_video_title {
                    videoCell.iconImage.image = UIImage(named: "ic_upload_video")
                } else {
                    videoCell.iconImage.image = UIImage(named: "ic_movie")
                }
                videoCell.delegate = viewController as? DetailRiderVideoCellDelegate
            }))
        }
        else {
            section.items.append(("ProgressCell", nil, nil))
        }
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_age, self.userRole?.riderAge))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_weight, self.userRole?.riderWeight))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_height, self.userRole?.riderLength))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_riding_week, self.userRole?.riderRideFrequencyWeek))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_riding_year, self.userRole?.riderRideFrequencyYear))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_riding_years_total, self.userRole?.riderYearsTotal))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_level, getNameFor(self.userRole?.riderCompetenceLevel, UserRole.Fields.competenceLevel.rawValue)))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_competition_field, getNameFor(self.userRole?.riderCompetition, UserRole.Fields.competition.rawValue)))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_horse_type, getNameFor(self.userRole?.riderHorseType, UserRole.Fields.horseType.rawValue)))
        
        // Prefered temperament
        section.items.append(("HorseDetailsTemperamentRangeCell", self.userRole, {(viewController, cell, item) in
            let rangeDetailsCell = cell as! DetailsNumericRangeSliderCell
            let userRole = item as? UserRole
            
            rangeDetailsCell.detailNameLabel.text = Str.screen_rider_details_temperament
            
            let rangeMinType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.temperamentMin.rawValue) as? HKStructureNumericProperty
            let rangeMaxType = AppApi.getRiderStructure()?.getProperty(UserRole.Fields.temperamentMax.rawValue) as? HKStructureNumericProperty
            
            if rangeMinType != nil && rangeMaxType != nil {
                rangeDetailsCell.valueRangeSlider.setMinMaxValue(CGFloat(rangeMinType!.min), maxValue: CGFloat(rangeMaxType!.max))
            }
            else {
                rangeDetailsCell.valueRangeSlider.setMinMaxValue(0.0, maxValue: 100.0)
            }
            
            rangeDetailsCell.valueRangeSlider.setValue(CGFloat(userRole?.riderHorseTemperamentMin ?? 0), right: CGFloat(userRole?.riderHorseTemperamentMax ?? 0))
            rangeDetailsCell.valueRangeSlider.isUserInteractionEnabled = false
            rangeDetailsCell.valueRangeSlider.rightThumbImage = nil
            rangeDetailsCell.valueRangeSlider.leftThumbImage = nil
        }))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_lesson_type, getNameFor(self.userRole?.riderLessonType, UserRole.Fields.lessonType.rawValue)))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_type_of_riding, getNameFor(self.userRole?.riderTypeOfRiding, UserRole.Fields.typeOfRiding.rawValue)))
        
        section.items.append(("DetailItemSplitterCell", nil, nil))
        
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_stable_owner, self.userRole?.riderStableOwner))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_number_of_horses, self.userRole?.riderNumberHorses))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_year_own_horse, self.userRole?.riderYearsOwnedHorse))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_presentation_title, self.userRole?.presentation))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_reference_name_title, self.userRole?.riderReferenceName))
        section.items.append(DetailItemCell
            .buildSourceItem(Str.screen_rider_details_reference_phone_title, self.userRole?.riderReferencePhone))
        if let parentEmail = self.userRole?.riderParentEmail, !parentEmail.isEmpty {
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_rider_details_reference_parent_email_title, self.userRole?.riderParentEmail))
        }
        
        self.itemsSource.append(section)
    }
    
    private func getNameFor(_ values: [String]?, _ field: String) -> String? {
        if values == nil {
            return "-"
        }
        
        var result: String = ""
        for value in values!.sorted() {
            let enumItem: HKStructureEnumPropertyValue? = AppApi.getRiderStructure()?.findEnumPropertyValue(field, value: value)
            if let enumItem = enumItem {
                if result.count > 0 {
                    result += "\n"
                }
                result += enumItem.name
            }
        }
        
        return result
    }
    
    private func getNameFor(_ value: String?, _ field: String) -> String? {
        if value == nil {
            return "-"
        }
        return AppApi.getRiderStructure()?.findEnumPropertyValue(field, value: value)?.name
    }
    
    @IBAction func actionMenu() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
            [unowned self] action in
            if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        })
        
        if self.userRole?.userId == HonkioApi.activeUser()?.userId {
            alertController.addAction(UIAlertAction(title: Str.menu_rider_details_edit, style: .default) { [unowned self] action in
                if let userRole = self.userRole {
                    if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: RiderRegistrationViewController.self), viewController: self) as? RiderRegistrationViewController {
                        controller.wizardObject = UserRole(from: userRole)
                        controller.setCompletionDelegate(self, tag: nil)
                        self.present(controller, animated: true, completion: nil)
                    }
                }
            })
        }
        
        self.present(alertController, animated: true)
    }
}

extension RiderDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func pickImage(_ sourceType: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = sourceType
            imagePickerController.delegate = self;
            self.present(imagePickerController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Missing camera or photo library",
                                                    message: "You can't take photo, there is no camera or photo library.",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
            self.present(alertController, animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let resizedImage = resizeImage(image, 100.00, opaque: true)
            self.uploadUserPhoto(resizedImage)
            print("get image")
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadUserPhoto(_ image: UIImage!) {
        guard image != nil else { return }

        BusyIndicator.shared.showProgressView(self.view)
        let data = UIImageJPEGRepresentation(image.fixRotation(), 0.85)
        
        URLCache.shared.removeAllCachedResponses()
        HonkioApi.userUpdatePhoto(MessageBinary(data: data, andMimeType: "image/jpeg"), flags: 0, responseHandler: { [weak self] (response) in
            if response.isStatusAccept() {
                let rider = UserRole(from: self?.userRole)!
                rider.riderPhoto = response.url
                HonkioApi.userSetRole(rider, shop: nil, flags: 0) {[weak self](response) in
                    BusyIndicator.shared.hideProgressView()
                    if response.isStatusAccept() {
                        self?.updateDataSource()
                        self?.tableView.reloadData()
                        print("screen_rider_details_profile_picture_set")
                    }
                }
            }
        })

    }
    
    func resizeImage(_ image: UIImage, _ dimension: CGFloat, opaque: Bool, contentMode: UIViewContentMode = .scaleAspectFit) -> UIImage {
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage
        
        let size = image.size
        let aspectRatio =  size.width/size.height
        
        switch contentMode {
        case .scaleAspectFit:
            if aspectRatio > 1 {                            // Landscape image
                width = dimension
                height = dimension / aspectRatio
            } else {                                        // Portrait image
                height = dimension
                width = dimension * aspectRatio
            }
            
        default:
            fatalError("UIIMage.resizeToFit(): FATAL: Unimplemented ContentMode")
        }
        
        if #available(iOS 10.0, *) {
            let renderFormat = UIGraphicsImageRendererFormat.default()
            renderFormat.opaque = opaque
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
            newImage = renderer.image {
                (context) in
                image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), opaque, 0)
            image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            newImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return newImage
    }
}
