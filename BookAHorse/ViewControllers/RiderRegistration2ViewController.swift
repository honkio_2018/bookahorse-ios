//
//  RiderRegistration2ViewController.swift
//  BookAHorse
//
//  Created by Dash on 25.09.2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class RiderRegistration2ViewController: HKWizardViewController {
    
    @IBOutlet weak var addresView: AddressView!
    
    private var isInit: Bool = false
    
    override func viewDidLoad() {
        self.addresView.parentViewController = self
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInit {
            if let userRole = self.wizardNavigationController?.wizardObject as? UserRole {
                self.addresView.setAddress(userRole.riderAddress)
            }
            isInit = true
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        BusyIndicator.shared.showProgressView(self.view)
        self.addresView.syncAddress { [unowned self] in
            BusyIndicator.shared.hideProgressView()
            
            if let userRole = self.wizardNavigationController?.wizardObject as? UserRole {
                userRole.riderAddress = self.addresView.getAddress()
            }
            
            if self.isValid() {
                self.nextViewController()
            }
        }
    }
    
    private func isValid() -> Bool {
        return self.addresView.isValid()
    }

}
