//
//  AssetsListViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 23/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate
import CoreLocation

class AssetsListViewController: HKBaseAssetsListViewController {
    
    var backgroundCellImage = UIImage(named: "bg_horse_no_photo")
    let formatter = MeasurementFormatter()
    let cellReuseIdentifier = String(describing: HorseListItemCell.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 220
        tableView.tableFooterView = UIView()
        
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = .ceiling
        numberFormatter .maximumFractionDigits = 1
        formatter.numberFormatter = numberFormatter
        formatter.unitOptions = .naturalScale
        
        // cell
        tableView.register(UINib.init(nibName: cellReuseIdentifier, bundle: nil),
                                forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    override func cellIdentifier(_ item: HKAsset) -> String {
        return cellReuseIdentifier
    }
    
    override func buildCellBunder() -> HKTableViewItemInitializer? {
        return { [unowned self] (viewController, cell, item) in
            guard
                let assetCell = cell as? HorseListItemCell,
                let asset = item as? HKAsset
            else {
                assertionFailure("cell or asset is nil, cannot build table cell!")
                return
            }
            
            assetCell.horseName.text = asset.name.uppercased()

            if let defaultPicture = asset.properties?["default_picture"] {
                if let horseImageView = assetCell.horseImage {
                    horseImageView.imageFromURL(link: (defaultPicture as? String),
                                                errorImage: self.backgroundCellImage,
                                                contentMode: nil)
                }
            } else {
                assetCell.horseImage.image = self.backgroundCellImage
            }
            
            var result = [String]()
            // Add horse breed
            if let horseBreed = AppApi.getHorseStructure()?.findEnumPropertyValue(HKAsset.Fields.horseBreed.rawValue,
                                                                               value: asset.horseBreed)?.name, !horseBreed.isEmpty {
                result.append(horseBreed)
            }
            
            // Add distance info
            if let currentLocation = HonkioApi.deviceLocation() {
                let horseLocation = CLLocation(latitude: asset.latitude, longitude: asset.longitude)
                let distance = currentLocation.distance(from: horseLocation)
                let distanceInMeters = Measurement(value: distance, unit: UnitLength.meters)
                result.append(self.formatter.string(from: distanceInMeters))
            }
            
            assetCell.horseInfo.text = result.filter({ !$0.isEmpty }).joined(separator: ", ")
        }
    }
    
    override func setupFilter(_ filter: AssetFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.structureIds = [AppApi.HORSE_STRUCTURE]
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let asset = self.lazyItem(indexPath) as? HKAsset {
            if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseProductsViewController.self), viewController: self) as? HKAssetViewProtocol {
                controller.loadAsset(asset.assetId)
                self.navigationController?.pushViewController(controller as! UIViewController, animated: true)
            }
        }
    }
}

