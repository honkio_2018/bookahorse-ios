//
//  OrderDetailsViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OrderDetailsViewController: HKBaseTableViewController, HKOrderViewProtocol, HKOrderSetVCStrategyDelegate, DetailsPhotoCellChildPhotoDelegate, DetailsPhotoCellMainPhotoDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buttonOrderAction: UIBarButtonItem!
    
    @IBOutlet weak var toolBar1: UIView!
    @IBOutlet weak var toolBar1Item: UIButton!
    
    @IBOutlet weak var toolBar2: UIView!
    @IBOutlet weak var toolBar2ItemPositive: UIButton!
    @IBOutlet weak var toolBar2ItemNegative: UIButton!
    
    private let TagOrderPublishing = "OrderPublishing"
    private let TagOrderApproveByParent = "OrderApproveByParent"
    
    public var order: Order! {
        didSet {
            self.shop = nil
            self.asset = nil
            self.riderRole = nil
            self.ownerRole = nil
            
            if self.order.shopId != nil {
                HonkioApi.shopFind(byId: self.order.shopId, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        let shopFind = response.result as! ShopFind
                        if shopFind.list.count > 0 {
                            self?.shop = shopFind.list.first
                            if let shop = self?.shop {
                                self?.checkOrders(shop)
                            }
                        }
                    }
                }
            }
            
            if self.order.asset != nil {
                HonkioApi.userGetAsset(self.order.asset, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        self?.asset = response.result as? HKAsset
                    }
                }
            }
            
            if self.order.thirdPerson != nil {
                HonkioApi.userGetRole(AppApi.ROLE_RIDER, userId: self.order.thirdPerson, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        if self?.order.model == Order.Model.model.rawValue {
                            self?.riderRole = response.result as? UserRole
                        }
                        else {
                            self?.ownerRole = response.result as? UserRole
                        }
                    }
                }
            }
            
            if self.order.userOwner != self.order.thirdPerson {
                HonkioApi.userGetRole(AppApi.ROLE_RIDER, userId: self.order.userOwner, flags: 0) {[weak self] (response) in
                    if response.isStatusAccept() {
                        if self?.order.model == Order.Model.model.rawValue {
                            self?.ownerRole = response.result as? UserRole
                        }
                        else {
                            self?.riderRole = response.result as? UserRole
                        }
                    }
                }
            }
            
            if isCommentsVisible() {
                self.loadComments()
            }
            
            if self.order != nil {
                HonkioApi.userSetPush(true, filter: PushFilter().addContent(PUSH_FILTER_QUERY_CONTENT_ORDER, value: self.order.orderId), flags: MSG_FLAG_LOW_PRIORITY + MSG_FLAG_NO_WARNING, handler: nil)
            }
            
            self.initDataSource()
            self.tableView.reloadData()
            self.setupActionViews()
        }
    }
    
    private var shop: Shop? {
        didSet {
            if self.shop != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var asset: HKAsset? {
        didSet {
            if self.asset != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var riderRole: UserRole? {
        didSet {
            if self.riderRole != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var ownerRole: UserRole? {
        didSet {
            if self.riderRole != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var ownerName: String? {
        didSet {
            if self.ownerName != nil {
                self.initDataSource()
                self.tableView.reloadData()
            }
        }
    }
    
    private var orderComments: [UserComment]? {
        didSet {
            if self.orderComments != nil {
                self.initDataSource()
                self.tableView.reloadData()
                self.setupActionViews()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50
        
        self.toolBar1Item.titleLabel?.textAlignment = .center;
        self.toolBar2ItemPositive.titleLabel?.textAlignment = .center;
        self.toolBar2ItemNegative.titleLabel?.textAlignment = .center;
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(OrderDetailsViewController.userDidComment(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_VOTE)), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(OrderDetailsViewController.userDidComment(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_VOTE)), object: nil)
        }
        else {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: DetailsDoublePhotoCellDelegate protocol
    
    func mainPhotoDidClick(_ cell: DetailsPhotoCell, view: UIView) {
        let user = HonkioApi.activeUser()
        if order.thirdPerson == user?.userId || order.userOwner == user?.userId {
            // Is rider or parent
            self.actionShowHorseDetails()
        }
        else {
            // Is horse owner
            self.actionShowRiderDetails()
        }
    }
    
    func childPhotoDidClick(_ cell: DetailsDoublePhotoCell, view: UIView) {
        let user = HonkioApi.activeUser()
        if order.thirdPerson == user?.userId || order.userOwner == user?.userId {
            // Is rider or parent
            self.actionShowOwnerDetails()
        }
        else {
            // Is horse owner
            self.actionShowHorseDetails()
        }
    }
    
    // MARK: - HKOrderSetVCStrategyDelegate protocol
    
    func orderWillSet(_ vcStrategy: HKSimpleOrderSetVCStrategy) {
        BusyIndicator.shared.showProgressView(self.view)
    }
    
    func orderDidSet(_ vcStrategy: HKSimpleOrderSetVCStrategy, order: Order) {
        BusyIndicator.shared.hideProgressView()
        if vcStrategy.tag == TagOrderPublishing {
            CustomAlertView(okTitle: Str.screen_order_details_dialog_order_published_title, okMessage: Str.screen_order_details_dialog_order_published_message) {
                [unowned self] in
                if let viewControllers = self.navigationController?.viewControllers {
                    self.navigationController?.viewControllers = [viewControllers.first!, viewControllers.last!]
                }
            }.show()
        }
        else if vcStrategy.tag == TagOrderApproveByParent {
            let newOrder = Order(from: self.order)!
            newOrder.status = Order.StatusRequest.approved.rawValue
            HonkioApi.userSetOrder(newOrder, flags: MSG_FLAG_NO_WARNING, handler: nil)
        }
        
        self.order = order
    }
    
    func onOrderSetError(_ vcStrategy: HKSimpleOrderSetVCStrategy, error: ApiError) {
        BusyIndicator.shared.hideProgressView()
        if !AppErrors.handleError(self, error: error) {
            if error.code == 3305 || error.code == ErrUserOrder.timeReservationError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_reserving_time_title, okMessage: Str.screen_order_details_dlg_error_reserving_time_message, okAction: nil).show()
            }
            else if error.code == ErrUserOrder.paymentError.rawValue {
                CustomAlertView(okTitle: Str.screen_order_details_dlg_error_payment_title, okMessage: Str.screen_order_details_dlg_error_payment_message, okAction: nil).show()
            }
            else {
                CustomAlertView(apiError: error, okAction: nil).show()
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func orderAction(_ sender: Any?) {
        let currentUser = HonkioApi.activeUser()?.userId
        if self.order.userOwner == currentUser || self.order.thirdPerson == currentUser {
            // User is rider or order owner
            switch (self.order.status) {
            case Order.Status.bookRequest.rawValue, Order.Status.booked.rawValue:
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
                    [unowned self] action in
                    if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                })
                
                alertController.addAction(UIAlertAction(title: Str.menu_horse_details_abuse, style: .default) {
                    [unowned self] action in
                    self.showAbuseCommentDialog(Str.screen_order_details_abuse_dialog_title,
                                                Str.screen_order_details_abuse_dialog_comment_title,
                                                Str.screen_order_details_abuse_dialog_comment_hint,
                                                Str.screen_order_details_abuse_send,
                                                self.order.orderId,
                                                .order)
                })
                
                alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
                
                self.present(alertController, animated: true)

            default:
                // Do nothing
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                alertController.addAction(UIAlertAction(title: "action_settings".localized, style: .default) {
                    [unowned self] action in
                    if let viewController = AppController.instance.getViewControllerById(AppController.SettingsMain, viewController: self) {
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                })
                
                alertController.addAction(UIAlertAction(title: Str.menu_horse_details_abuse, style: .default) {
                    [unowned self] action in
                    self.showAbuseCommentDialog(Str.screen_order_details_abuse_dialog_title,
                                                Str.screen_order_details_abuse_dialog_comment_title,
                                                Str.screen_order_details_abuse_dialog_comment_hint,
                                                Str.screen_order_details_abuse_send,
                                                self.order.orderId,
                                                .order)
                })
                
                alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
                
                self.present(alertController, animated: true)
            }
        }
        else {
            // User is Horse Owner
            // Do nothing
        }
    }
    
    @IBAction func actionToolbar1(_ sender: Any?) {
        let currentUser = HonkioApi.activeUser()?.userId
        if self.order.orderId == nil {
            self.actionOrderPublish()
        }
        else if self.order.isStatus(Order.Status.rideCompleted.rawValue) && self.order.userOwner == currentUser {
            self.actionRateFromRider()
        }
        else if self.order.isStatus(Order.Status.rideCompleted.rawValue) && self.order.userOwner != currentUser && self.order.thirdPerson != currentUser {
            self.actionRateFromOwner()
        }
        else if self.order.isStatus(Order.Status.booked.rawValue) && self.order.userOwner != currentUser && self.order.thirdPerson != currentUser {
            self.actionOrderCancelByHorseOwner()
        }
        else if self.order.isStatusOne(of: [Order.Status.booked.rawValue, Order.Status.bookRequest.rawValue]) {
            if self.order.orderId != nil {
                self.actionOrderCancelByRider()
            }
        } else {
            // Should not be called
            print("Should not be called")
        }
    }
    
    @IBAction func actionToolbar2(_ sender: AnyObject?) {
        let currentUser = HonkioApi.activeUser()?.userId
        if self.order.model == Order.Model.modelRequest.rawValue && self.order.isStatus(Order.StatusRequest.new.rawValue) && self.order.thirdPerson == currentUser {
            // User is Parent. Request from child to parent
            
            if sender === self.toolBar2ItemPositive {
                self.actionOrderApproveByParent()
            }
            else {
                self.actionOrderRejectByParent()
            }
        }
        else if self.order.userOwner != currentUser && self.order.thirdPerson != currentUser {
            // User is Horse Owner
            
            if sender === self.toolBar2ItemPositive {
                self.actionOrderApproveByHorseOwner()
            }
            else {
                self.actionOrderCancelByHorseOwner()
            }
        }
        else {
            // Should not be called
        }
    }
    
    private func actionShowHorseDetails() {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: HorseDetailsViewController.self), viewController: self) as? HorseDetailsViewController {
            if let asset = self.asset {
                controller.asset = asset
            }
            else {
                controller.loadAsset(self.order.asset)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func actionShowOwnerDetails() {
        if let controller = AppController.getViewControllerById(AppController.Shop, viewController: self) as? HKBaseShopViewController {
            if let identity = self.shop {
                controller.load(byIdentyty: identity)
            }
            else {
                controller.load(byId: self.asset!.horseShop!)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func actionShowRiderDetails() {
        if let viewController = AppController.getViewControllerById(AppController.UserDetails, viewController: self) as? RiderDetailsViewController {
            if let rider = self.riderRole {
                viewController.userRole = rider
            }
            else {
                viewController.loadRole(AppApi.ROLE_RIDER, user: self.order.thirdPerson)
            }
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func actionOrderPublish() {
        moveOrder(self.order, toStatus: self.order.status, tag: TagOrderPublishing,
                  askAccount: self.order.model == Order.Model.model.rawValue)
    }
    
    private func actionOrderApproveByParent() {
        let newOrder = Order(from: order)!
        newOrder.status = Order.Status.bookRequest.rawValue
        newOrder.model = Order.Model.model.rawValue
        newOrder.thirdPerson = newOrder.userOwner
        newOrder.userOwner = HonkioApi.activeUser()?.userId
        newOrder.account = nil
        newOrder.orderId = nil
        
        let orderSetStrategy = HKOrderSetVCStrategy(TagOrderApproveByParent, parent: self)
        orderSetStrategy.removeOnCompletion = true
        orderSetStrategy.isPinRequired = true
        orderSetStrategy.isAccountRequired = true
        orderSetStrategy.userSetOrder(newOrder, flags: 0)
    }
    
    private func actionOrderRejectByParent() {
        moveOrder(self.order, toStatus: Order.StatusRequest.rejected.rawValue, tag: nil, askAccount: false)
    }
    
    private func actionOrderApproveByHorseOwner() {
        moveOrder(self.order, toStatus: Order.Status.booked.rawValue, tag: nil, askAccount: false)
    }
    
    private func actionOrderComplete() {
        moveOrder(self.order, toStatus: Order.Status.rideCompleted.rawValue, tag: nil)
    }
    
    private func actionOrderCancelByRider() {
        moveOrder(self.order, toStatus: Order.Status.canceldByRider.rawValue, tag: nil, askAccount: false)
    }
    
    private func actionOrderCancelByHorseOwner() {
        moveOrder(self.order, toStatus: Order.Status.canceledByOwner.rawValue, tag: nil, askAccount: false)
    }
    
    private func actionRateFromRider() {
        let controller = AppController.instance.getViewControllerByIdentifier(String(describing: RateOwnerAndHorseViewController.self), viewController: self) as! RateOwnerAndHorseViewController
        controller.order = order
        
        if let orderComments = self.orderComments {
            for comment in orderComments {
                if comment.objectType == HK_OBJECT_TYPE_SHOP {
                    controller.ownerComment = comment
                }
                else if comment.objectType == HK_OBJECT_TYPE_ASSET {
                    controller.horseComment = comment
                }
            }
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func actionRateFromOwner() {
        let controller = AppController.instance.getViewControllerByIdentifier(String(describing: RateRiderViewController.self), viewController: self) as! RateRiderViewController
        controller.order = order
        
        if let orderComments = self.orderComments {
            for comment in orderComments {
                if comment.objectType == HK_OBJECT_TYPE_USER_ROLE {
                    controller.riderComment = comment
                }
            }
        }
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func setupActionViews() {
        let currentUser = HonkioApi.activeUser()?.userId
        if self.order.userOwner == currentUser || self.order.thirdPerson == currentUser {
            if self.order.orderId == nil {
                // Publishing button
                self.toolBar1.isHidden = false
                self.toolBar1Item.setTitle(Order.Model.model.rawValue == order.model ? Str.screen_order_details_confirm : Str.screen_order_details_confirm_and_share_order, for: .normal)
            }
            else if self.order.userOwner == currentUser && self.order.isStatus(Order.Status.rideCompleted.rawValue) {
                // User is order owner and order is complete
                if !isHasRiderRatings() {
                    self.toolBar1.isHidden = false
                    self.toolBar1Item.setTitle(Str.screen_order_details_button_rate_owner, for: .normal)
                }
                else {
                    self.toolBar1.isHidden = true
                }
            }
            else if self.order.userOwner == currentUser && self.order.isStatusOne(of: [Order.Status.booked.rawValue, Order.Status.bookRequest.rawValue]) {
                self.toolBar1.isHidden = false
                self.toolBar1Item.setTitle(Str.screen_order_details_action_cancel, for: .normal)
            } else {
                self.toolBar1.isHidden = true
            }
            
            if self.order.model == Order.Model.modelRequest.rawValue && self.order.thirdPerson == HonkioApi.activeUser()?.userId {
                // User is parent and shpuld approwe order
                self.toolBar2.isHidden = !self.order.isStatus(Order.StatusRequest.new.rawValue)
                self.toolBar2ItemPositive.setTitle(Str.screen_order_details_button_approve_child_order, for: .normal)
                self.toolBar2ItemNegative.setTitle(Str.screen_order_details_reject_child_order, for: .normal)
            }
            else {
                self.toolBar2.isHidden = true
            }
        }
        else {
            // User is Horse Owner
            self.buttonOrderAction.isEnabled = false
            
            if self.order.isStatus(Order.Status.rideCompleted.rawValue) {
                if !isHasOwnerRatings() {
                    self.toolBar1.isHidden = false
                    self.toolBar1Item.setTitle(Str.screen_order_details_button_rate_rider, for: .normal)
                }
                else {
                    self.toolBar1.isHidden = true
                }
            }
            else if self.order.isStatus(Order.Status.booked.rawValue){
                self.toolBar1.isHidden = false
                self.toolBar1Item.setTitle(Str.screen_order_details_action_cancel, for: .normal)
            }
            else {
                self.toolBar1.isHidden = true
            }
            
            if self.order.isStatus(Order.Status.bookRequest.rawValue) {
                self.toolBar2.isHidden = false
                self.toolBar2ItemPositive.setTitle(Str.screen_order_details_action_approve, for: .normal)
                self.toolBar2ItemNegative.setTitle(Str.screen_order_details_action_reject, for: .normal)
            }
            else {
                self.toolBar2.isHidden = true
            }
            
        }
    }
    
    private func initDataSource() {
        let user = HonkioApi.activeUser()
        
        self.itemsSource.removeAll()
        
        if let order = self.order {
            let section = HKTableViewSection()
            
            if order.thirdPerson == user?.userId || order.userOwner == user?.userId {
                // User is a Rider who make an order
                section.items.append(("DetailsDoublePhotoCell", order, {(viewController, cell, item) in
                    let photosCell = cell as! DetailsDoublePhotoCell
                    let order = item as! Order
                    
                    photosCell.mainPhotoView.setBorder(.large)
                    photosCell.mainPhotoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + order.asset,
                                                         errorImage: UIImage(named: "ic_placeholder_horse"),
                                                         contentMode: nil)
                
                    photosCell.childPhotoView.setBorder(.small)
                    photosCell.childPhotoView.imageFromURL(link: AppApi.MEDIA_URL_SHOP_PHOTO + order.shopId,
                                                          errorImage: UIImage(named: "ic_placeholder_owner"),
                                                          contentMode: nil)
                    photosCell.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
                    
                    photosCell.mainPhotoDelegate = viewController as? DetailsPhotoCellMainPhotoDelegate
                    photosCell.childPhotoDelegate = viewController as? DetailsPhotoCellChildPhotoDelegate
                }))
            }
            else {
                // User is Horse Owner
                section.items.append(("DetailsDoublePhotoCell", order, {(viewController, cell, item) in
                    let photosCell = cell as! DetailsDoublePhotoCell
                    let order = item as! Order
                    
                    photosCell.mainPhotoView.setBorder(.large)
                    photosCell.mainPhotoView.imageFromURL(link: AppApi.MEDIA_URL_USER_PHOTO + order.thirdPerson,
                                                          errorImage: UIImage(named: "ic_placeholder_rider"),
                                                          contentMode: nil)
                    
                    photosCell.childPhotoView.setBorder(.small)
                    photosCell.childPhotoView.imageFromURL(link: AppApi.MEDIA_URL_ASSET + order.asset,
                                                           errorImage: UIImage(named: "ic_placeholder_horse"),
                                                            contentMode: nil)
                    photosCell.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
                    
                    photosCell.mainPhotoDelegate = viewController as? DetailsPhotoCellMainPhotoDelegate
                    photosCell.childPhotoDelegate = viewController as? DetailsPhotoCellChildPhotoDelegate
                }))
            }
            
            // Ride Title
            section.items.append(("DetailsNameCell", order.title, {(viewController, cell, item) in
                (cell as? DetailsNameCell)?.nameLabel.text = item as? String
                (cell as? DetailsNameCell)?.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
            }))
            
            // Chat
            if order.orderId != nil {
                section.items.append(("DetailsButtonCell", order, { (viewController, cell, item) in
                    let parent = viewController as! OrderDetailsViewController
                    (cell as? DetailsButtonCell)?.detailButton.setTitle(Str.screen_order_details_button_chat, for: .normal)
                    (cell as? DetailsButtonCell)?.detailButton.addTarget(parent, action: #selector(parent.actionChat(_:)), for: .touchUpInside)
                }))
            }
            
            // ratings
            if isCommentsVisible() {
                
                if let orderComments = self.orderComments {
                    
                    let commentBinder: HKTableViewItemInitializer = { (viewController, cell, item) in
                        let vc = viewController as? OrderDetailsViewController
                        let commentCell = cell as? UserFeedbackItemCell
                        let comment = item as? UserComment
                        
                        commentCell?.showRating(comment?.mark ?? 0)
                        
                        if comment?.objectType == HK_OBJECT_TYPE_USER_ROLE {
                            commentCell?.rateRecipientLabel.text = Str.screen_order_details_rate_recipient_rider
                            commentCell?.photoView.imageFromURL(
                                link: AppApi.MEDIA_URL_USER_PHOTO + (comment?.object ?? ""),
                                errorImage: UIImage(named: "ic_placeholder_rider"),
                                contentMode: nil)
                            
                            if vc?.riderRole?.objectId == comment?.object {
                                commentCell?.nameLabel.text = vc?.riderRole?.userName()
                            }
                            else if vc?.ownerRole?.objectId == comment?.object {
                                commentCell?.nameLabel.text = vc?.ownerRole?.userName()
                            }
                            else {
                                commentCell?.nameLabel.text = Str.screen_order_details_rider_name_loading
                            }
                        }
                        else if comment?.objectType == HK_OBJECT_TYPE_ASSET {
                            commentCell?.rateRecipientLabel.text = Str.screen_order_details_rate_recipient_horse
                            commentCell?.photoView.imageFromURL(
                                link: AppApi.MEDIA_URL_ASSET + (comment?.object ?? ""),
                                errorImage: UIImage(named: "ic_placeholder_horse"),
                                contentMode: nil)
                            
                            if vc?.asset?.assetId == comment?.object {
                                commentCell?.nameLabel.text = vc?.asset?.horseName
                            }
                            else {
                                commentCell?.nameLabel.text = Str.screen_order_details_horse_name_loading
                            }
                        }
                        else if comment?.objectType == HK_OBJECT_TYPE_SHOP {
                            commentCell?.rateRecipientLabel.text = Str.screen_order_details_rate_recipient_owner
                            commentCell?.photoView.imageFromURL(
                                link: AppApi.MEDIA_URL_SHOP_PHOTO + (comment?.object ?? ""),
                                errorImage: UIImage(named: "ic_placeholder_owner"),
                                contentMode: nil)
                            
                            if vc?.shop?.identityId == comment?.object {
                                commentCell?.nameLabel.text = vc?.shop?.name
                            }
                            else {
                                commentCell?.nameLabel.text = Str.screen_order_details_shop_name_loading
                            }
                        }
                        else {
                            commentCell?.rateRecipientLabel.text = ""
                            commentCell?.photoView.image = UIImage(named: "logo_nav")
                            commentCell?.nameLabel.text = "null"
                        }
                    }
                    
                    for comment in orderComments {
                        section.items.append(("UserFeedbackItemCell", comment, commentBinder))
                    }
                }
                else {
                    section.items.append(("ProgressCell", nil, nil))
                }
            }
            
            // Booking status
            if self.order.orderId != nil && !(self.order.status?.isEmpty ?? false)  {
                section.items.append(DetailItemCell
                    .buildSourceItem(Str.screen_order_details_status, order.statusName))
            } else {
                section.items.append(DetailItemCell
                    .buildSourceItem(Str.screen_order_details_status, "-"))
            }
            
            // Owner name
            var ownerName = ""
            if AppApi.getOwnerShop() != nil && (self.shop?.identityId == AppApi.getOwnerShop()?.identityId) {
                ownerName = self.shop?.name ?? Str.screen_order_details_data_loading
            } else {
                if self.ownerName != nil {
                    ownerName = self.ownerName!
                } else {
                    ownerName = Str.screen_owner_details_hidden_data
                }
            }
            
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_owner_name, ownerName))
            
            // Horse name
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_horse_name, self.asset?.name ?? Str.screen_order_details_data_loading))
            
            // Rider name
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_rider_name, self.riderRole?.riderName ?? Str.screen_order_details_data_loading))
            
            // userOwner - pay
            // thirdPerson - walk on horse
            if order.userOwner != order.thirdPerson {
                section.items.append(DetailItemCell
                    .buildSourceItem(Str.screen_order_details_parent_name, self.ownerRole?.riderName ?? Str.screen_order_details_data_loading))
            } else {
                // User is rider and order owner
                section.items.append(DetailItemCell
                    .buildSourceItem(Str.screen_order_details_creator_name, self.riderRole?.riderName ?? Str.screen_order_details_data_loading))
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_creation_date, dateFormatter.string(from: order.creationDate)))
            
            dateFormatter.timeStyle = .none
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_date, dateFormatter.string(from: order.startDate)))
            
            dateFormatter.dateStyle = .none
            dateFormatter.timeStyle = .short
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_start_time, dateFormatter.string(from: order.startDate)))
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_end_time, dateFormatter.string(from: order.endDate)))
            
            section.items.append(TwoDetailItemCell
                .buildSourceItem(Str.screen_order_details_product, order.products.names, order.products.desc))
            
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_amount, order.products.price))
            
            section.items.append(DetailItemCell
                .buildSourceItem(Str.screen_order_details_comments, order.comment))
            
            self.itemsSource.append(section)
        }
    }
    
    private func moveOrder(_ order: Order, toStatus: String, tag: String?, askAccount: Bool = true) {
        let newOrder = Order(from: order)!
        newOrder.status = toStatus
        
        let currentUserId = HonkioApi.activeUser()?.userId
        
        let orderSetStrategy = HKOrderSetVCStrategy(tag, parent: self)
        orderSetStrategy.removeOnCompletion = true
        orderSetStrategy.isPinRequired = true
        orderSetStrategy.isAccountRequired = askAccount
        orderSetStrategy.useMerchantApi = order.userOwner != currentUserId && order.thirdPerson != currentUserId
        orderSetStrategy.userSetOrder(newOrder, flags: 0)
    }
    
    @objc func actionChat(_ sender: UIButton) {
        if let controller = AppController.instance.getViewControllerByIdentifier(String(describing: OrderChatViewController.self), viewController: self) as? OrderChatViewController {
            controller.order = self.order
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func checkOrders(_ shop: Shop) {
        if HonkioApi.isConnected() && AppSettings.theme == ROLE_RIDER {
            let filter = OrderFilter()
            if let riderRole = AppApi.apiInstance.riderRole, riderRole.isChild {
                filter.thirdPerson = HonkioApi.activeUser()?.userId
            } else {
                filter.owner = HonkioApi.activeUser()?.userId
            }
            filter.isQueryOnlyCount = true
            filter.model = Order.Model.model.rawValue
            filter.status = [Order.Status.booked.rawValue, Order.Status.rideCompleted.rawValue].joined(separator: "|")
            HonkioApi.userGetOrders(filter, shop: shop, flags: 0) { [weak self] (response) in
                if response.isStatusAccept() {
                    let ordersList = response.result as! OrdersList
                    if ordersList.totalCount > 0 {
                        self?.ownerName = shop.name
                    } else {
                        self?.ownerName = Str.screen_order_details_hidden_data
                    }
                    return
                }
                self?.ownerName = Str.screen_order_details_hidden_data
            }
        }
    }
    
    @objc public func userDidComment(_ notification: NSNotification?) {
        let response = notification?.userInfo?[NOTIFY_EXTRA_RESULT] as? Response
        if let comment = response?.result as? UserComment {
            if comment.orderId == self.order.orderId {
                loadComments()
            }
        }
    }
    
    public func loadComments() {
        let filter = FeedbackFilter()
        filter.orderId = self.order.orderId
        HonkioApi.userGetComments(filter, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                self?.orderComments = (response.result as? [UserComment])?.sorted(by: {
                    (item1, item2) -> Bool in
                    return AppApi.ROLE_RIDER != item1.voter.roleId
                })
            }
        }
    }
    
    private func isCommentsVisible() -> Bool {
        return self.order != nil && self.order.isStatus(Order.Status.rideCompleted.rawValue)
    }
    
    private func isHasRiderRatings() -> Bool {
        if let orderComments = self.orderComments {
            var isHorseRated = false
            var isOwnerRated = false
            for comment in orderComments {
                isHorseRated = isHorseRated || comment.objectType == HK_OBJECT_TYPE_ASSET
                isOwnerRated = isOwnerRated || comment.objectType == HK_OBJECT_TYPE_SHOP
                
                if isHorseRated && isOwnerRated {
                    return true
                }
            }
        }
        else {
            // If comments not load lets think that they are exists
            return true
        }
        return false
    }
    
    private func isHasOwnerRatings() -> Bool {
        if let orderComments = self.orderComments {
            for comment in orderComments {
                if comment.objectType == HK_OBJECT_TYPE_USER_ROLE {
                    return true
                }
            }
        }
        else {
            // If comments not load lets think that they are exists
            return true
        }
        return false
    }
    
    private func productsName(_ products: [BookedProduct]?) -> String {
        if products == nil || products!.count == 0 {
            return "-"
        }
        
        var result = ""
        for booking in products! {
            if result.count > 0 {
                result += ", "
            }
            
            if booking.count > 1 {
                result += "\(booking.product.name ?? "null")(\(booking.count))"
            }
            else {
                result += "\(booking.product.name ?? "null")"
            }
        }
        
        return result
    }
    
    private func productsDesc(_ products: [BookedProduct]?) -> String {
        if products == nil || products!.count == 0 {
            return "-"
        }
        
        return products![0].product.desc ?? "-"
    }
    
    private func productsPrice(_ products: [BookedProduct]?) -> String {
        if products == nil || products!.count == 0 {
            return "-"
        }
        
        var price = 0.0
        var currency: String?
        
        for booking in products! {
            price += booking.getPrice()
            
            if currency == nil {
                currency = booking.product.currency
            }
        }
        
        return String(format: "%.2f %@", price, currency ?? "")
    }
}
