//
//  WPIntroLastPageView.swift
//  happs
//
//  Created by Shurygin Denis on 8/11/16.
//  Copyright © 2016 SoftDev. All rights reserved.
//

import UIKit

open class IntroLastPageView: IntroPageView {
    
    fileprivate static let nextButtonSize : CGFloat = 240.0
    
    open var delegate : IntroLastPageViewDelegate!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupSubviews()
    }
    
    override func setupSubviews() {
        super.setupSubviews()
        
        self.logoImageView.image = UIImage(named: "intro_state_4_image")
        self.titleView.text = "screen_intro_title_4".localized
        self.textView.text = "screen_intro_desc_4".localized
        
        self.logoImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(IntroLastPageView.actionShowVideo(_:))))
        self.logoImageView.isUserInteractionEnabled = true
    }
    
    
    @objc open func actionShowVideo(_ sender: Any) {
        self.delegate.actionShowVideo()
    }
}
