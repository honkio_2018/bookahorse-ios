//
//  PostScheduleTableViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 04/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostScheduleTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var fromDateField: UITextField!
    @IBOutlet weak var fromDatePicker: UIDatePicker!
    @IBOutlet weak var fromTimeField: UITextField!
    @IBOutlet weak var fromTimePicker: UIDatePicker!
    @IBOutlet weak var repeatPicker: UIPickerView!
    @IBOutlet weak var repeatField: UITextField!
    
    enum rowIndexes: Int {
        case fromDateRow
        case fromDatePickerRow
        case fromTimeRow
        case fromTimePickerRow
        case repeatRow
        case repeatPickerRow
        
        static let count: Int = {
            var max: Int = 0
            while let _ = rowIndexes(rawValue: max) { max += 1 }
            return max
        }()
    }
    
    open var event: CalendarEvent? {
        didSet {
            if let event = self.event, !self.isOneDayPeriod(event.start, event.end) {
                event.repeating = EVENT_REPEATING_ONCE
            }
        }
    }
    
    private var dateFormatter = DateFormatter()
    private var timeFormatter = DateFormatter()
    private var eventRepeating = [String]()
    
    private let validatorNotEmpty = HKTextValidator(errorMessage: "text_validator_field_empty".localized,
                                                  rool: HKTextValidator.ruleNotEmpty())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // cell height for table view rows
        tableView.estimatedRowHeight = 63.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        fromDatePicker.isHidden = true
        fromTimePicker.isHidden = true
        repeatPicker.isHidden = true
        
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        
        eventRepeating.append(Str.calendar_event_repeating_once)
        eventRepeating.append(Str.calendar_event_repeating_daily)
        eventRepeating.append(Str.calendar_event_repeating_weekly)
        eventRepeating.append(Str.calendar_event_repeating_monthly)
        
        if let event = event, event.eventId != nil {
            fromDateField.text = dateFormatter.string(from: event.start)
            fromDatePicker.date = event.start
            fromTimeField.text = timeFormatter.string(from: event.start)
            fromTimePicker.date = event.start
            if let index = eventRepeating.index(of: CalendarEvent.getRepeatingNameStringId(event.repeating)) {
                repeatPicker.selectRow(index, inComponent: 0, animated: false)
                repeatField.text = eventRepeating[index]
            }
        }
        else {
            fromDatePicker.date = Date()
            fromDateField.text = dateFormatter.string(from: fromDatePicker.date )
            fromTimeField.text = timeFormatter.string(from: fromDatePicker.date )
            fromTimePicker.date = fromDatePicker.date
            
            repeatPicker.selectRow(0, inComponent: 0, animated: false)
            repeatField.text = eventRepeating[0]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowIndexes.count
    }
    
    func getPickerRowHeight(_ picker: UIDatePicker) -> CGFloat {
        return picker.isHidden ? 0.0 : 216.0
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == rowIndexes.fromDatePickerRow.rawValue {
            return getPickerRowHeight(fromDatePicker)
        } else if indexPath.row == rowIndexes.fromTimePickerRow.rawValue {
            return getPickerRowHeight(fromTimePicker)
        } else if indexPath.row == rowIndexes.repeatPickerRow.rawValue {
            return repeatPicker.isHidden ? 0.0 : 108.0
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == rowIndexes.fromDateRow.rawValue {
            fromDatePicker.isHidden = !fromDatePicker.isHidden
            if fromDatePicker.isHidden {
                if fromDateField.text!.isEmpty {
                    fromDateField.text = dateFormatter.string(from:fromDatePicker.date)
                }
            } else {
                fromTimePicker.isHidden = true
                repeatPicker.isHidden = true
            }
        } else if indexPath.row == rowIndexes.fromTimeRow.rawValue {
            fromTimePicker.isHidden = !fromTimePicker.isHidden
            if fromTimePicker.isHidden {
                if fromTimeField.text!.isEmpty {
                    fromTimeField.text = timeFormatter.string(from:fromTimePicker.date)
                }
            } else {
                fromDatePicker.isHidden = true
                repeatPicker.isHidden = true
            }
        } else if indexPath.row == rowIndexes.repeatRow.rawValue {
            repeatPicker.isHidden = !repeatPicker.isHidden
            if repeatPicker.isHidden {
                if repeatField.text!.isEmpty {
                    repeatField.text = eventRepeating[repeatPicker.selectedRow(inComponent: 0)]
                }
            } else {
                fromDatePicker.isHidden = true
                fromTimePicker.isHidden = true
            }
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tableView.beginUpdates()
            // apple bug fix - some TV lines hide after animation
            self.tableView.deselectRow(at:indexPath, animated: true)
            self.tableView.endUpdates()
        })
    }

    @IBAction func timeDidChanged(_ sender: UIDatePicker) {
        fromTimeField.text = timeFormatter.string(from:fromTimePicker.date)
        self.updateRepeatPicker()
    }
    
    @IBAction func dateDidChanged(_ sender: UIDatePicker) {
        fromDateField.text = dateFormatter.string(from:fromDatePicker.date)
        self.updateRepeatPicker()
    }
    
    public func isValid() -> Bool {
        var isValid = true
        
        isValid = validatorNotEmpty.validate(fromDateField) && isValid
        isValid = validatorNotEmpty.validate(fromTimeField) && isValid
        isValid = validatorNotEmpty.validate(repeatField) && isValid
        
        return isValid
    }
    
    // MARK: - UIPickerViewDataSource
    
    // returns the number of 'columns' to display.
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.isOneDayPeriod() ? eventRepeating.count : 1
    }
    
    // MARK: - UIPickerViewDelegate
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        repeatField.text = eventRepeating[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return eventRepeating[row]
    }
    
    private func isOneDayPeriod() -> Bool {
        let calendar = Calendar.current
        let startTime = self.fromTimePicker.date
        let startDate = calendar.date(bySettingHour: calendar.component(.hour, from: startTime),
                                      minute: calendar.component(.minute, from: startTime),
                                      second: 0, of: self.fromDatePicker.date)!
        
        return isOneDayPeriod(startDate, startDate.addingTimeInterval(self.event?.maxProductDuration ?? 0.0))
    }
    
    private func isOneDayPeriod(_ startDate: Date?, _ endDate: Date?) -> Bool {
        if startDate == nil || endDate == nil {
            return false
        }
        
        let calendar = Calendar.current
        return calendar.component(.day, from: startDate!) == calendar.component(.day, from: endDate!)
    }
    
    private func updateRepeatPicker() {
        if !isOneDayPeriod() {
            repeatPicker.selectRow(0, inComponent: 0, animated: false)
            repeatField.text = eventRepeating[0]
        }
        repeatPicker.reloadAllComponents()
    }
}
