//
//  PostSchedule1ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 29/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostSchedule1ViewController: HKBaseMerchantProductListTableViewController {
    
    let productItemCellName = "ListItemProductSelectCell"
    let currencyFormatter = NumberFormatter()
    
    open var event: CalendarEvent? {
        get { return wizardNavigationController?.wizardObject as? CalendarEvent }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }
    
    private var products: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Str.screen_horse_merchant_post_schedule_title
        
        currencyFormatter.numberStyle = .currency
        
        products = event?.products ?? products
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 61
        tableView.tableFooterView = UIView()
        
        // register table cell view form nib file
        let productItemCellNameNib = UINib.init(nibName: productItemCellName, bundle: Bundle.main)
        tableView.register(productItemCellNameNib, forCellReuseIdentifier: productItemCellName)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PostSchedule1ViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_PRODUCT)), object: nil)
    }
    
    /**
     Removes observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on moved to parent controlled view.
     */
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_PRODUCT)), object: nil)
        }
    }
    
    @objc func refresh(_ notification : Notification) {
        refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override open func setupFilter(_ filter: MerchantProductsFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
        if let owner = AppApi.apiInstance.ownerRole {
            filter.merchantId = owner.ownerMerchantId
        }
    }
    
    open override func cellIdentifier(_ item: Product) -> String {
        return productItemCellName
    }
    
    open override func buildCellBunder() -> HKTableViewItemInitializer? {
        return { [unowned self] (viewController, cell, item) in
            guard let productCell = cell as? ListItemProductSelectCell,
                let product = item as? Product
                else {
                    assertionFailure("productCell or product is nil, cannot build table cell!")
                    return
            }
            
            var duration: TimeInterval = 0.0
            duration.millisecond = product.duration
            
            var durationAsString: String = ""
            if duration.day != 0 {
                durationAsString += "\(duration.day) d."
            }
            
            if (duration.hour % 24) != 0 {
                durationAsString += "\(duration.hour % 24) h."
            }
            
            if duration.minute != 0 {
                durationAsString += "\(duration.minute) min."
            }
            
            productCell.nameLabel.text = "\(product.name ?? "-") (\(durationAsString))"
            productCell.amountLabel.text = self.currencyFormatter.string(from: product.amount as NSNumber)
            productCell.descriptionLabel.text = product.desc
            
            var isSelected = false
            if let selection = (viewController as? PostSchedule1ViewController)?.products {
                for item in selection {
                    if item.productId == product.productId {
                        isSelected = true
                        break
                    }
                }
            }
            
            productCell.accessoryType = isSelected ? .checkmark : .none
        }
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if isValid() {
            event?.products = products
            self.wizardNavigationController?.nextViewController()
        } else {
            CustomAlertView(okTitle: Str.screen_horse_merchant_post_schedule_title,
                            okMessage: "screen_horse_merchant_post_schedule_select_product".localized,
                            okAction: nil).show()
        }
    }
    
    @IBAction func addActivity(_ sender: UIBarButtonItem) {
        if let controller = AppController.instance.getViewControllerByIdentifier("ProductDetailsViewController", viewController: self) as? ProductDetailsViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let productCell = self.tableView.cellForRow(at: indexPath),
            let cell = productCell as? ListItemProductSelectCell,
            let product = getProductFromCell(cell) else { return }
        
        if cell.accessoryType == .checkmark {
            cell.accessoryType = .none
            if let index = products.firstIndex(where: { (item) -> Bool in
                return item.productId == product.productId
            }) {
                products.remove(at: index)
            }
        } else {
            cell.accessoryType = .checkmark
            products.append(product)
        }
    }
    
    func getProductFromCell(_ cell: ListItemProductSelectCell) -> Product? {
        let indexPath = self.tableView.indexPath(for: cell)
        if let indexPath = indexPath {
            let section = self.itemsSource[(indexPath as IndexPath).section]
            let item = section.items[(indexPath as IndexPath).row]
            let product = item.item as? Product
            return product
        }
        return nil
    }
    
    private func isValid() -> Bool {
        return !products.isEmpty
    }
}
