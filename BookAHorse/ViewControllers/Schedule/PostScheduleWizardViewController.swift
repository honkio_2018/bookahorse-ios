//
//  PostScheduleWizardViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 29/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostScheduleWizardViewController: HKWizardNavigationController {
    
    var horse: HKAsset?
    var event: CalendarEvent? {
        didSet {
            self.wizardObject = event
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Str.screen_horse_merchant_post_schedule_title
        
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "PostSchedule1ViewController"))
        self.wizardViewControllers.append(self.buildViewControllerConstructor(identifier: "PostSchedule2ViewController"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    open override func wizardComplete() {
        BusyIndicator.shared.showProgressView(self.view)
        let event = self.wizardObject as! CalendarEvent
        event.type = EVENT_TYPE_TENTATIVE
        HonkioMerchantApi.merchantEventSet(event,
                                           objectId:horse?.getId(),
                                           objectType:HK_OBJECT_TYPE_ASSET,
                                           shop:AppApi.getOwnerShop(),
                                           flags:0)
        { [unowned self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                NotificationCenter.default.post(name: NSNotification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_EVENT)),
                                                object: nil)
                self.dismiss(animated: true, completion: nil)
            } else {
                let error = response.anyError()
                if !AppErrors.handleError(self, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
}
