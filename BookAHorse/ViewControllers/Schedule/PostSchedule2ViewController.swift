//
//  PostSchedule2ViewController.swift
//  BookAHorse
//
//  Created by Mikhail Li on 29/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class PostSchedule2ViewController: HKBaseViewController {

    @IBOutlet weak var containerView: UIView!
    var embededTableViewController: PostScheduleTableViewController?
    
    open var event: CalendarEvent {
        get { return wizardNavigationController?.wizardObject as! CalendarEvent }
    }
    
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        get { return self.navigationController as? HKBaseWizardNavigationController }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if event.eventId == nil {
             self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func nextButtonDidTapped(_ sender: UIButton) {
        if let viewController = embededTableViewController {
            if viewController.isValid() {
                switch viewController.repeatField.text! {
                case Str.calendar_event_repeating_once:
                    event.repeating = EVENT_REPEATING_ONCE
                case Str.calendar_event_repeating_daily:
                    event.repeating = EVENT_REPEATING_DAILY
                case Str.calendar_event_repeating_weekly:
                    event.repeating = EVENT_REPEATING_WEEKLY
                case Str.calendar_event_repeating_monthly:
                    event.repeating = EVENT_REPEATING_MONTHLY
                default:
                    print(viewController.repeatField.text!)
                }
                
                let calendar = Calendar.current
                let startTime = viewController.fromTimePicker.date
                let startDate = calendar.date(bySettingHour: calendar.component(.hour, from: startTime),
                                              minute: calendar.component(.minute, from: startTime),
                                              second: 0, of: viewController.fromDatePicker.date)
                
                let endDate = startDate?.addingTimeInterval(event.maxProductDuration)
                
                event.start = startDate
                event.initialStart = startDate
                event.end = endDate
                event.initialEnd = endDate
                
                self.wizardNavigationController?.nextViewController()
            }  else {
                CustomAlertView(okTitle: Str.screen_horse_merchant_post_schedule_title,
                                okMessage: "text_validator_field_empty".localized,
                                okAction: nil).show()
            }
        }
    }
    
    @IBAction func deletePeriodDidTapped(_ sender: UIBarButtonItem) {
        BusyIndicator.shared.showProgressView(self.view)
        if !event.eventId.isEmpty {
            HonkioMerchantApi.merchantEventDelete(event, shop: AppApi.getOwnerShop(), flags: 0) { (response) in
                BusyIndicator.shared.hideProgressView()
                if response.isStatusAccept() {
                    NotificationCenter.default.post(name: NSNotification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_EVENT)),
                                                    object: nil)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    let error = response.anyError()
                    if !AppErrors.handleError(self, error: error) {
                        CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedded" {
            embededTableViewController = segue.destination as? PostScheduleTableViewController
            embededTableViewController?.event = event
        }
    }
}
