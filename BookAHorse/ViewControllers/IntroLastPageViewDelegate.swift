//
//  WPIntroLastPageViewDelegate.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 9/14/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

public protocol IntroLastPageViewDelegate {
    func actionShowVideo()
}
