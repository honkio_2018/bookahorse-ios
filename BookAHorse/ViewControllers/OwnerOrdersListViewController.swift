//
//  OwnerOrdersListViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/13/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class OwnerOrdersListViewController: OrdersListViewController {
    
    enum OwnerFilter: String {
        case incoming = "incoming"
        case approved = "approved"
        case history = "history"
    }
    
    private static let OwnerFilterKey = "BookHorse_OwnerFilter"
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var emptyLabel: UILabel!
    
    private var ownerFilter: OwnerFilter = OwnerFilter.init(rawValue: StoreData.load(fromStore: OwnerFilterKey) as? String ?? OwnerFilter.incoming.rawValue)! {
        didSet {
            
            StoreData.save(toStore: self.ownerFilter.rawValue, forKey: OwnerOrdersListViewController.OwnerFilterKey)
            
            self.refresh()
            self.updateFilterLabels(self.ownerFilter)
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.shopIdentity = AppApi.getOwnerShop()
        btnFilter.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        self.updateFilterLabels(self.ownerFilter)
    }
    
    @IBAction func filterAction(_ sender: Any?) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized, style: .cancel))
        
        alertController.addAction(UIAlertAction(title: Str.screen_orders_list_filter_incoming, style: .default) {
            [unowned self] action in
            self.ownerFilter = .incoming
        })
        alertController.addAction(UIAlertAction(title: Str.screen_orders_list_filter_approved, style: .default) {
            [unowned self] action in
            self.ownerFilter = .approved
        })
        alertController.addAction(UIAlertAction(title: Str.screen_orders_list_filter_history, style: .default) {
            [unowned self] action in
            self.ownerFilter = .history
        })
        
        self.present(alertController, animated: true)
    }
    
    //MARK: - HONKIO
    override open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        super.setupFilter(filter, count: count, skip: skip)
        filter.childMerchants = false
        filter.model = Order.Model.model.rawValue
        
        switch self.ownerFilter {
        case .incoming:
            filter.status = [
                Order.Status.bookRequest.rawValue
            ].joined(separator: "|")
            setEmptyText(Str.screen_orders_list_incoming_empty_message)
        case .approved:
            filter.status = [
                Order.Status.booked.rawValue
            ].joined(separator: "|")
            setEmptyText(Str.screen_orders_list_approved_empty_message)
        case .history:
            filter.status = [
                Order.Status.canceldByRider.rawValue,
                Order.Status.canceledByOwner.rawValue,
                Order.Status.rideCompleted.rawValue
            ].joined(separator: "|")
            setEmptyText(Str.screen_orders_list_history_empty_message)
        }
    }
    
    override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(self.filter, count: count, skip: skip)
        HonkioMerchantApi.merchantGetOrders(self.filter, shop: AppApi.getOwnerShop(), flags: 0, handler: responseHandler)
    }
    
    private func updateFilterLabels(_ filter: OwnerFilter) {
        switch filter {
        case .incoming:
            self.btnFilter.setTitle(Str.screen_orders_list_filter_incoming, for: .normal)
        case .approved:
            self.btnFilter.setTitle(Str.screen_orders_list_filter_approved, for: .normal)
        case .history:
            self.btnFilter.setTitle(Str.screen_orders_list_filter_history, for: .normal)
        }
    }
    
    override func loadFinished() {
        self.emptyLabel?.isHidden = (self.lazyItemsCount() > 0)
        
        super.loadFinished()
    }
    
    func setEmptyText(_ text: String?) {
        self.emptyLabel?.text = text
    }
}
