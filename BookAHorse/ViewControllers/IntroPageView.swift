//
//  WPIntroPageView.swift
//  happs
//
//  Created by Shurygin Denis on 8/11/16.
//  Copyright © 2016 SoftDev. All rights reserved.
//

import UIKit

open class IntroPageView: UIView {
    
    open fileprivate(set) lazy var logoImageView : UIImageView = self.makeLogoImageView()
    open fileprivate(set) lazy var titleView : UILabel = self.makeTitleView()
    open fileprivate(set) lazy var textView : UILabel = self.makeTextView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSubviews()
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupSubviews()
        setupConstraints()
    }
    
    func setupSubviews() {
        addSubview(logoImageView)
        addSubview(titleView)
        addSubview(textView)
    }
    
    func setupConstraints() {
        let views : [String : AnyObject] = [
            "titleView" : titleView,
            "textView" : textView,
            "logoImageView" : logoImageView
        ]
        
        let formatArray : [String] = [
            "V:|-10-[logoImageView]-10-[titleView]-10-[textView]-(>=10)-|",
            "H:|-30-[titleView]-30-|",
            "H:|-30-[textView]-30-|",
            "H:|-30-[logoImageView]-30-|"
        ]
        
        var constraints :[NSLayoutConstraint] = NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        constraints.append(NSLayoutConstraint.constraintsCenterX(textView, toItem: self,
            multiplier: 1.0, constant: 1.0))
        
        let imageMult : CGFloat = 0.43
        
        NSLayoutConstraint(item: logoImageView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: imageMult, constant: 0).isActive = true
        
        NSLayoutConstraint.activate(constraints)
    }

    
    // MARK: - Private methods
    fileprivate func makeLogoImageView() -> UIImageView {
        let v = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.image = UIImage(named: "intro_state_1_image")
        v.contentMode = .scaleAspectFit
        
        return v;
    }
    
    fileprivate func makeTitleView() -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        view.font = UIFont.boldSystemFont(ofSize: 22)
        view.textColor = UIColor.black
        
        view.text = "screen_intro_title_1".localized
        
        return view;
    }
    
    fileprivate func makeTextView() -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        view.textAlignment = .center
        view.font = UIFont.systemFont(ofSize: 17)
        view.textColor = UIColor.black
        
        view.text = "screen_intro_desc_1".localized
        
        return view;
    }
    
}
