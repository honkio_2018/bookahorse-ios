//
//  HorseMatchingHelper.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

open class HorseMatchingHelper {
    
    private static let CompetenceItems = ["id6", "id5", "id4", "id3", "id2", "id1"]

    public static func isHorseSuitableForRider(_ riderRole: UserRole, _ horse: HKAsset) -> Bool {
        let isAgeSuitable = ((horse.horseRiderAgeMin ?? 0) ... (horse.horseRiderAgeMax ?? 0)).contains(riderRole.riderAge)
        let isWeightSuitable = ((horse.horseRiderWeightMin ?? 0) ... (horse.horseRiderWeightMax ?? 0)).contains(riderRole.riderWeight ?? 0)
        let isLengthSuitable = ((horse.horseRiderLengthMin ?? 0) ... (horse.horseRiderLengthMax ?? 0)).contains(riderRole.riderLength ?? 0)
        let isTemperamentSuitable = ((riderRole.riderHorseTemperamentMin ?? 0) ... (riderRole.riderHorseTemperamentMax ?? 0)).contains(horse.horseTemperament ?? 0)
        
        
        var isCompetenceLevelSuitable = HorseMatchingHelper.isCompetenceLevelSuitable(riderRole.riderCompetenceLevel, horse.horseCompetenceLevel)
        
        return isAgeSuitable &&
            isWeightSuitable &&
            isLengthSuitable &&
            isTemperamentSuitable &&
            isCompetenceLevelSuitable
    }
    
    public static func buildSuitableFilter(_ rider: UserRole?, _ reuseFilter: HKPropertiesFilter?) -> HKPropertiesFilter {
        let propertiesFilter = reuseFilter ?? HKPropertiesFilter()
        propertiesFilter.removeAll()
        
        let riderAge = NSNumber(integerLiteral: rider?.riderAge ?? 0)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: nil, end: riderAge), forKey: HKAsset.Fields.horseRiderAgeMin.rawValue)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: riderAge, end: nil), forKey: HKAsset.Fields.horseRiderAgeMax.rawValue)
        
        let riderWeight = NSNumber(integerLiteral: rider?.riderWeight ?? 0)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: nil, end: riderWeight), forKey: HKAsset.Fields.horseRiderWeightMin.rawValue)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: riderWeight, end: nil), forKey: HKAsset.Fields.horseRiderWeightMax.rawValue)
        
        let riderLength = NSNumber(integerLiteral: rider?.riderLength ?? 0)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: nil, end: riderLength), forKey: HKAsset.Fields.horseRiderLengthMin.rawValue)
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: riderLength, end: nil), forKey: HKAsset.Fields.horseRiderLengthMax.rawValue)
        
        propertiesFilter.add(HKPropertiesFilterRange<NSNumber>(start: NSNumber(integerLiteral: rider?.riderHorseTemperamentMin ?? 0), end: NSNumber(integerLiteral: rider?.riderHorseTemperamentMax ?? 0)), forKey: HKAsset.Fields.horseTemperament.rawValue)
        
        let riderCompetenceLevel = rider?.riderCompetenceLevel
        if riderCompetenceLevel?.count ?? 0 > 0 {
            propertiesFilter.add(HKPropertiesFilterInList<NSString>(list: riderItems(riderCompetenceLevel).compactMap({ (string) -> NSString in
                return NSString(string: string)
            })), forKey: HKAsset.Fields.horseCompetenceLevel.rawValue)
        }
        
        return propertiesFilter
    }
    
    private static func isCompetenceLevelSuitable(_ riderLevels: [String]?, _ horseLevels: [String]?) -> Bool {
        if horseLevels == nil || horseLevels?.count == 0 {
            return true
        }
        
        if riderLevels == nil || riderLevels?.count == 0 {
            return false
        }
        
        let realRiderLevels = riderItems(riderLevels)
        
        for horseLevel in horseLevels! {
            if realRiderLevels.contains(horseLevel) {
                return true
            }
        }
        
        return false
    }
    
    private static func riderItems(_ riderCompetenceLevel: [String]?) -> [String] {
        var filter: [String] = []
        var i = 0;
        
        while i < CompetenceItems.count && filter.isEmpty {
            if riderCompetenceLevel?.contains(CompetenceItems[i]) == true {
                filter.append(CompetenceItems[i])
                i += 1
                while i < CompetenceItems.count {
                    filter.append(CompetenceItems[i])
                    i += 1
                }
            }
            i += 1
        }
        
        return filter
    }
    
    private static func isEnumValueSuitable(_ riderValue: String, _ horseValue: String ) -> Bool {
        if riderValue != KEY_DOES_NOT_MATTER {
            return horseValue == KEY_DOES_NOT_MATTER || horseValue == riderValue
        }
        return true
    }
    
    private static func lessonTypeSuitable(_ riderValue: String, _ horseValue: String ) -> Bool {
        if riderValue == KEY_DOES_NOT_MATTER {
            return horseValue == KEY_DOES_NOT_MATTER ||
                horseValue == "id2" || horseValue == "id3"
        }
        
        if horseValue == KEY_DOES_NOT_MATTER {
            return riderValue == KEY_DOES_NOT_MATTER ||
                riderValue == "id2" || riderValue == "id3"
        }
        return horseValue == riderValue
    }
    
    private static func isListValueSuitable(_ riderList: [String], _ horseList: [String]) -> Bool {
        for horseItem in horseList {
            for riderItem in riderList {
                if riderItem == horseItem {
                    return true
                }
            }
        }
        return false
    }
}
