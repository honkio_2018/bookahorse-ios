//
//  AppApi.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

open class AppApi {

    public static let ROLE_RIDER = "5a3ced84284059d1fef73fcc"
    public static let ROLE_OWNER = "5a3ced6c284059d1fef73fcb"
    public static let TAG = "BookaAHorse.HonkioApi"
    
    public static let HORSE_STRUCTURE = "5a55f61d28405908e2681337"
    public static let PRODUCT_ID_TYPE = "5a7c6f8a28405935d5c1d923"
    
    public static let FEE_PRODUCT_ID = "5c75497f618412131eb47266"
    
    public static let REPORT_USER_SEND_REPORT = "5c77d39e6c1c6a27b150a5cf";
    
    public static let OBJECT_TYPE_USER_ROLE = "user_role"
    public static let LINK_TYPE_USER_ROLE = "user_role"
    public static let OBJECT_TYPE_ASSET = "asset"
    public static let LINK_TYPE_ASSET = "asset"
    public static let OBJECT_TYPE_SHOP = "shop"
    public static let LINK_TYPE_SHOP = "shop"
    
    public static let MEDIA_URL_PREFIX = "https://media1.honkio.com/bookahorse/getfile/"
//    public static let MEDIA_URL_PREFIX = "https://media1.honkio.com/bookahorse_beta/getfile/"
    
    public static let MEDIA_URL_ASSET = MEDIA_URL_PREFIX + OBJECT_TYPE_ASSET + "/"
    public static let MEDIA_URL_USER_PHOTO = MEDIA_URL_PREFIX + OBJECT_TYPE_USER_ROLE + "/" + ROLE_RIDER + "/"
    public static let MEDIA_URL_SHOP_PHOTO = MEDIA_URL_PREFIX + OBJECT_TYPE_SHOP + "/"
    
    public fileprivate(set) static var apiInstance : AppApiInstance!
    
    public static func initInstance(instance: AppApiInstance) {
        apiInstance = instance
    }
    
    public static func ownerRole() -> UserRole? {
        return apiInstance.roles[AppApi.ROLE_OWNER]
    }
    
    public static func riderRole() -> UserRole? {
        return apiInstance.roles[AppApi.ROLE_RIDER]
    }
    
    public static func isRegisteredAsOwner() -> Bool {
        return (ownerRole()?.ownerRegistered ?? false) && checkOwnerAccess() && getOwnerShop() != nil
    }
    
    public static func isRegisteredAsRider() -> Bool {
        return riderRole() != nil
    }
    
    public static func isHasAnyRole() -> Bool {
        return isRegisteredAsOwner() || isRegisteredAsRider()
    }
    
    public static func userResetRoles() {
        apiInstance.resetRoles();
    }
    
    /**
     * Force set user roles. Use it if you need to save roles in App state immediately.
     */
    public static func onRolesGet(roles: [UserRole]) {
        apiInstance.onRolesGet(roles: roles);
    }
    
    /**
     * Force set user role. Use it if you need to save role in App state immediately.
     */
    public static func onRoleGet(role: UserRole) {
        apiInstance.onRoleGet(role: role);
    }

    //getRiderRole -> riderRole()
    //getOwnerRole -> ownerRole()
    
    public static  func getHorseStructure() -> HKStructure? {
        return apiInstance.assetStructures[HORSE_STRUCTURE]
    }
    
    public static func getRiderStructure() -> HKUserRoleStructure? {
        return apiInstance.roleStructures[ROLE_RIDER]
    }
    
    public static func getOwnerStructure() -> HKUserRoleStructure? {
        return apiInstance.roleStructures[ROLE_OWNER]
    }
    
    public static func getOwnerMerchant() -> Merchant? {
        return apiInstance.ownerMerchant
    }
    
    public static func getOwnerShop() -> MerchantShop? {
        return apiInstance.ownerShop
    }
    
    private static func checkOwnerAccess() -> Bool {
        let role = ownerRole()
        let access = apiInstance.accessList
        
        return role != nil && access != nil
            && access!.isHasAccess(role!.ownerMerchantId, access: ["asset_edit"])
    }
    
    static func userGetOrderById(_ orderId: String!, callBack: @escaping (_ order: Order?, _ error: ApiError?) -> Void) {
        
        let filter = OrderFilter()
        filter.queryId = orderId
        filter.queryUnreadMessages = true
        filter.queryProductDetails = true
        
        HonkioApi.userGetOrders(filter, flags: 0) { (response) in
            
            if response.isStatusAccept() {
                
                if let ordersList = response.result as? OrdersList {
                    
                    if ordersList.list.count > 0 {
                        let item = ordersList.list[0]
                        callBack(item, nil)
                    }
                    else {
                        callBack(nil, ApiError(code: ErrUserOrder.notFound.rawValue))
                    }
                }
            }
            else {
                
                callBack(nil, response.anyError())
            }
        }
    }
    
    static func merchantGetOrderById(_ orderId: String!, callBack: @escaping (_ order: Order?, _ error: ApiError?) -> Void) {
        
        let filter = OrderFilter()
        filter.queryId = orderId
        filter.queryUnreadMessages = true
        filter.queryProductDetails = true
        
        HonkioMerchantApi.merchantGetOrders(filter, flags: 0) { (response) in
            
            if response.isStatusAccept() {
                
                if let ordersList = response.result as? OrdersList {
                    
                    if ordersList.list.count > 0 {
                        let item = ordersList.list[0]
                        callBack(item, nil)
                    }
                    else {
                        callBack(nil, ApiError(code: ErrUserOrder.notFound.rawValue))
                    }
                }
            }
            else {
                
                callBack(nil, response.anyError())
            }
        }
    }
}
