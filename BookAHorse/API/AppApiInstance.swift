//
//  AppApiInstance.swift
//  BookAHorse
//
//  Created by Mikhail Li on 16/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

public class AppApiInstance {
    
    public var ownerRole: UserRole? {
        return self.roles[AppApi.ROLE_OWNER]
    }
    public var riderRole: UserRole? {
        return self.roles[AppApi.ROLE_RIDER]
    }
    
    public private(set) var roles = [String: UserRole]()
    public private(set) var assetStructures = [String: HKStructure]()
    public private(set) var roleStructures = [String: HKUserRoleStructure]()
    public private(set) var accessList: UserAccessList?
    public private(set) var ownerMerchant: Merchant?
    public private(set) var ownerShop: MerchantShop?
    
    public init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userRoleGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_GET_ROLE)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userRoleGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ROLE)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userRolesGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_GET_ROLES)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userAccessList(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_ACCESS_LIST)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userMerchantGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_GET)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userMerchantGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userShopGet(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_GET_SHOP)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.userStructureList(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_ROLES_DESCRIPTIONS)),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.assetStructureList(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_ASSET_STRUCTURE)),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AppApiInstance.logout(_:)),
                                               name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_LOGOUT)),
                                               object: nil)
    }
    
    private func getResponseFromNotification(_ notification: Notification) -> Response? {
        guard let userInfo = notification.userInfo as? [String: AnyObject?] else { return nil }
        guard let result = userInfo[NOTIFY_EXTRA_RESULT] as? Response else { return nil }
        
        return result
    }
    
    @objc public func userRoleGet(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let role = result.result as? UserRole else { return }
        
        onRoleGet(role: role)
    }
    
    @objc public func userRolesGet(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let rolesList = result.result as? UserRolesList else { return }
        
        onRolesGet(roles: rolesList.list)
    }
    
    @objc public func userAccessList(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let userAccessList = result.result as? UserAccessList else { return }
        
        self.accessList = userAccessList
    }
    
    @objc public func userMerchantGet(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let merchant = result.result as? Merchant else { return }
        
        onMerchantGet(merchant: merchant)
    }
    
    @objc public func userShopGet(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let shopInfo = result.result as? MerchantShopInfo else { return }
        
        onShopGet(shop: shopInfo.shop)
    }
    
    @objc public func userStructureList(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let structureList = result.result as? HKUserRoleStructureList else { return }
        
        onStructureGet(structures: structureList)
    }
    
    @objc public func assetStructureList(_ notification: Notification) {
        guard let result = getResponseFromNotification(notification) else { return }
        guard let structureList = result.result as? HKStructureList else { return }
        
        onStructureGet(structures: structureList)
    }
    
    @objc public func logout(_ notification: Notification) {
        resetRoles()
        accessList = nil
        ownerMerchant = nil
        ownerShop = nil
    }
    
    public func resetRoles() {
        roles.removeAll()
    }
    
    public func onRolesGet(roles: [Any]?) {
        guard let values = roles else { return }
        guard let user = HonkioApi.activeUser() else { return }

        for role in values as! [UserRole] {
            if user.userId == role.userId {
                onRoleGet(role: role)
            }
        }
    }
    
    public func onRoleGet(role: UserRole) {
        if role.userId != HonkioApi.activeUser()?.userId {
            return
        }
        
        if role.isActive {
            roles[role.roleId] = role
        } else {
            roles.removeValue(forKey: role.roleId)
        }
    }
    
    public func onMerchantGet(merchant: Merchant) {
        if merchant.merchantId == nil || merchant.merchantId.isEmpty {
            return
        }
        
        if let owner  = self.ownerRole {
            if merchant.merchantId == owner.ownerMerchantId {
                ownerMerchant = merchant
            }
        }
    }

    public func onShopGet(shop: MerchantShop) {
        guard !shop.identityId.isEmpty else { return }
        
        if let owner  = self.ownerRole {
            if shop.identityId == owner.ownerShopId {
                ownerShop = shop
            }
        }
    }
    
    public func onStructureGet(structures: HKUserRoleStructureList) {
        for item in structures.list {
            onStructureGet(structure: item)
        }
    }
    
    public func onStructureGet(structures: HKStructureList) {
        for item in structures.list {
            onStructureGet(structure: item)
        }
    }
 
    public func onStructureGet(structure: HKStructure) {
        assetStructures[structure.objectId] = structure
    }
    
    public func onStructureGet(structure: HKUserRoleStructure) {
        roleStructures[structure.objectId] = structure
    }
    
    public func getUserAccessList() -> UserAccessList? {
        return accessList;
    }
    
    public func getOwnerMerchant() -> Merchant? {
        return ownerMerchant;
    }
    
    public func getOwnerShop() -> MerchantShop? {
        return ownerShop;
    }
}
