//
//  AppSettings.swift
//  BookAHorse
//
//  Created by Mikhail Li on 01/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

public let CHILD_MAX_AGE = 14

public let ROLE_NONE = 0;
public let ROLE_RIDER = 1;
public let ROLE_OWNER = 2;

public let DEFAULT_MARK = 3;

public let SPLIT_SCHEDULE_ROUND_MIN = 10
public let PRODUCT_DURATION_MIN_VALUE = 15 //in min

public let KEY_DOES_NOT_MATTER = "any" //this is a key in horse structure that means that the parameter doesn't matter when comparing horse properties

#if DEBUG
//    let UrlHost = "https://"
    let UrlHost = "https://beta-"
    let UrlScheme = "honkio.com"
#else
 //   let UrlHost = "https://beta-"
    let UrlHost = "https://"
    let UrlScheme = "honkio.com"
#endif

public class AppSettings {
    
    static let AppURL = "\(UrlHost)api.\(UrlScheme)"
    static let ShareURL = "http://www.bookahorse.com/share/"
    static let AppIdentity : Identity = Identity(id: "com/honkio/bookahorse", andPassword: "v9VWO6DtomsuO9r5frj99RxLFCZ7aPfAKqpz88I3")
    
    private static let SAVED_THEME = "AppSettings.SAVED_THEME"
    
    public static var theme: Int {
        get {
            return (StoreData.load(fromStore: SAVED_THEME) as? Int) ?? ROLE_NONE
        }
        set {
            StoreData.save(toStore: newValue, forKey: SAVED_THEME)
        }
    }
    
    public static func switchTheme() {
        switch AppSettings.theme {
        case ROLE_RIDER:
            AppSettings.theme = ROLE_OWNER
            break
        case ROLE_OWNER:
            AppSettings.theme = ROLE_RIDER
            break
        default:
            // Do nothing
            break
        }
    }
    
}
