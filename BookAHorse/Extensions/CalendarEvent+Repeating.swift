//
//  CalendarEvent+Repeating.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension CalendarEvent {
    
    public enum EventRepeating: String {
        case once
        case daily
        case weekly
        case monthly
    }
    
    public static func getRepeatingNameStringId(_ status: String) -> String {
        switch status {
        case EventRepeating.once.rawValue:
            return Str.calendar_event_repeating_once
        case EventRepeating.daily.rawValue:
            return Str.calendar_event_repeating_daily
        case EventRepeating.weekly.rawValue:
            return Str.calendar_event_repeating_weekly
        case EventRepeating.monthly.rawValue:
            return Str.calendar_event_repeating_monthly
        default:
            return Str.calendar_event_repeating_unknown
        }
    }
    
    public static func repeatingValues() -> [String:String] {
        var result = [String:String]()
        result[EventRepeating.once.rawValue] = getRepeatingNameStringId(EventRepeating.once.rawValue)
        result[EventRepeating.daily.rawValue] = getRepeatingNameStringId(EventRepeating.daily.rawValue)
        result[EventRepeating.weekly.rawValue] = getRepeatingNameStringId(EventRepeating.weekly.rawValue)
        result[EventRepeating.monthly.rawValue] = getRepeatingNameStringId(EventRepeating.monthly.rawValue)
        return result
    }
}
