//
//  HkAddress+Extension.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 2/1/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import HonkioAppTemplate

extension HKAddress {
    
    func shortAddress() -> String {
        var result: String? = self.city
        
        if result != nil {
            if self.countryName != nil {
                result = result! + ", " + self.countryName
            }
        }
        else {
            result = self.countryName
        }
        
        return result ?? "-"
    }
    
}
