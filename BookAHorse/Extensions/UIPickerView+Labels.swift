//
//  UIPickerView+Labels.swift
//  BookAHorse
//
//  Created by Mikhail Li on 18/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit


extension UIPickerView {
    
    func setPickerLabels(labels: [Int:UILabel], containedView: UIView) { // [component number:label]
        
        let fontSize:CGFloat = 17
//        let labelWidth:CGFloat = containedView.bounds.width / CGFloat(self.numberOfComponents)
        let labelWidth:CGFloat = self.bounds.width / CGFloat(self.numberOfComponents)
        let x:CGFloat = self.frame.origin.x
        let y:CGFloat = (self.frame.size.height / 2) - (fontSize / 2)
        
        for i in 0...self.numberOfComponents {
            
            if let label = labels[i] {
                
                if self.subviews.contains(label) {
                    label.removeFromSuperview()
                }
                
                
                label.frame = CGRect(x: x + labelWidth * CGFloat(i), y: y, width: labelWidth, height: fontSize)
                label.font = UIFont.systemFont(ofSize: fontSize)
                label.backgroundColor = .clear
                label.textAlignment = NSTextAlignment.left
                
                self.addSubview(label)
            }
        }
    }
}
