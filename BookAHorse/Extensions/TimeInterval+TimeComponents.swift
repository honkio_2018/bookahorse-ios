//
//  TimeInterval+TimeComponents.swift
//  BookAHorse
//
//  Created by Mikhail Li on 12/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    var day: Int {
        set {
            self += Double(newValue) * 86_400
        }
        get {
            return Int((self/86_400).truncatingRemainder(dividingBy: 86_400))
        }
    }
    var hour: Int {
        set {
            self += Double(newValue) * 3600
        }
        get {
            return Int((self/3600).truncatingRemainder(dividingBy: 3600))
        }
    }
    var minute: Int {
        set {
            self += Double(newValue) * 60
        }
        get {
            return Int((self/60).truncatingRemainder(dividingBy: 60))
        }
    }
    var second: Int {
        set {
            self += Double(newValue)
        }
        get {
            return Int(self)
        }
    }
    var millisecond: Int {
        set {
            self += Double(newValue) / 1000
        }
        get {
            return Int(self*1000)
        }
    }
}

extension Int {
    var msToSeconds: Double {
        return Double(self) / 1000
    }
}
