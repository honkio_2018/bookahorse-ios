//
//  UserRole+Base.swift
//  BookAHorse
//
//  Created by Mikhail Li on 17/05/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension UserRole {
    // new functionality to add to UserRole
    enum Fields: String {
        
        // Base
        case email
        case phone
        case presentation
        case skillLevel = "own_competence_level"
        case is_registered = "is_registered"
        
        // Owner
        //for saving to shop
        case strOwnCompetenceLevel = "str_own_competence_level"
        case listCompetenceLevel = "list_competence_level"
        //from owner structure
        case ownCompetenceLevel = "ownCompetenceLevel"
        case competenceLevel = "competenceLevel"
        
        case companyId = "company_id"
        case companyName = "company_name"
        case merchantId = "merchant_id"
        case shopId = "shop_id"

        case bankAccount = "bank_account"
        case bankIban = "bank_iban"
        case bankSwift = "bank_swift"
        case vat = "vat_amount"
        case vatNumber = "vat_number"
        
        case insured = "insured"
        
        // Rider
//        case competenceLevel = "competenceLevel"
        case address
        case eduLevel = "eduLevel"
        case competition = "competitionField"
        case horseType = "type"
        
        case photo = "default_picture"
        case name = "name"
        case referenceName = "reference_name"
        case referencePhone = "reference_phone"
        
        case age = "age"
        case parentEmail = "parent"
        
        case length = "riding_length"
        case weight = "weight"
        case rideFrequencyWeek = "ride_frequency_week"
        case rideFrequencyYear = "ride_frequency_year"
        case yearsTotal = "number_of_years_ridden"
        case yearsSince = "years_since_you_rode"
        
        case numberOfHorses = "number_of_horses"
        case yearsOwnedHourse = "years_owned_horse"
        case yearsSinceOwnedHourse = "years_since_owned_horse"
        case stableOwner = "stable_owner"
        
        case privateLesson = "private_lesson"
        case indoorRiding = "indoor_riding"
        case outdoorRiding = "outdoor_riding"
        case groupLesson = "tour_ride"
        case coRider = "co_rider"
        case temperamentMin = "temperament_min"
        case temperamentMax = "temperament_max"
        
        case coRiderFrom = "co_rider_from"
        case coRiderTo = "co_rider_to"
        case availableFrom = "available_from"
        case availableTo = "available_to"
        
        case lessonType = "lessonType"
        case typeOfRiding = "typeOfRiding"
        
        static func getRawValue(_ field: Fields) -> String {
            return field.rawValue
        }
    }
    
    func applyPublicProperty(_ key: String, _ value: Any?) {
        if self.publicProperties == nil {
            self.publicProperties = [:]
        }
        
        self.publicProperties[key] = value
    }
    
    func applyPrivateProperty(_ key: String, _ value: Any?) {
        if self.privateProperties == nil {
            self.privateProperties = [:]
        }
        
        self.privateProperties[key] = value
    }
    
    // Base
    
    var email: String? {
        set {
            self.applyPublicProperty(Fields.email.rawValue, newValue!)
        }
        get {
            return self.publicProperties?[Fields.email.rawValue] as? String
        }
    }
    
    var phone: String? {
        set {
            self.applyPublicProperty(Fields.getRawValue(.phone), newValue)
        }
        get {
            return self.publicProperties?[Fields.phone.rawValue] as? String
        }
    }
    
    var presentation: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.presentation), newValue)
        }
        get {
            return self.publicProperties?[Fields.presentation.rawValue] as? String
        }
    }
    
    // Owner
    
    var ownerAddress: String? {
        set {
            return self.applyPublicProperty(Fields.address.rawValue, newValue)
        }
        get {
            return self.publicProperties?[Fields.address.rawValue] as? String
        }
    }
    
    var ownerOwnCompetenceLevel: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.ownCompetenceLevel), newValue)
        }
        get {
            return self.publicProperties?[Fields.ownCompetenceLevel.rawValue] as? String
        }
    }
    
    var ownerCompetenceLevelGroups: [String]? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.competenceLevel), newValue)
        }
        get {
            return self.publicProperties?[Fields.competenceLevel.rawValue] as? [String]
        }
    }
    
    var ownerCompanyId: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.companyId), newValue)
        }
        get {
            return self.publicProperties?[Fields.companyId.rawValue] as? String
        }
    }
    
    var ownerCompanyName: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.companyName), newValue)
        }
        get {
            return self.publicProperties?[Fields.companyName.rawValue] as? String
        }
    }
 
    var ownerMerchantId: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.merchantId), newValue)
        }
        get {
            return self.publicProperties?[Fields.merchantId.rawValue] as? String
        }
    }
    
    var ownerShopId: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.shopId), newValue)
        }
        get {
            return self.publicProperties?[Fields.shopId.rawValue] as? String
        }
    }
 
    var ownerBankAccount: String? {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.bankAccount), newValue)
        }
        get {
            return self.privateProperties?[Fields.bankAccount.rawValue] as? String
        }
    }
    
    var ownerBankIban: String? {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.bankIban), newValue)
        }
        get {
            return self.privateProperties?[Fields.bankIban.rawValue] as? String
        }
    }
    
    var ownerBankSwift: String? {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.bankSwift), newValue)
        }
        get {
            return self.privateProperties?[Fields.bankSwift.rawValue] as? String
        }
    }
    
    var ownerVat: Double? {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.vat), newValue)
        }
        get {
            return self.privateProperties?[Fields.vat.rawValue] as? Double
        }
    }
    
    var ownerVatNumber: String? {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.vatNumber), newValue)
        }
        get {
            return self.privateProperties?[Fields.vatNumber.rawValue] as? String
        }
    }
    
    var ownerRegistered: Bool {
        set {
            return self.applyPrivateProperty(Fields.getRawValue(.is_registered), newValue)
        }
        get {
            return self.privateProperties?[Fields.is_registered.rawValue] as? Bool ?? false
        }
    }
    
    // Rider
    
    var riderAddress: HKAddress? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.address), newValue?.toDict())
        }
        get {
            if let value = self.publicProperties?[Fields.address.rawValue] as? [String: String]  {
                return HKAddress.init(fromDict: value)
            } else {
                return nil
            }
        }
    }
    
    var riderCompetenceLevel: [String]? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.competenceLevel), newValue)
        }
        get {
            return self.publicProperties?[Fields.competenceLevel.rawValue] as? [String]
        }
    }
    
    var riderEduLevel: [String]? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.eduLevel), newValue)
        }
        get {
            return self.publicProperties?[Fields.eduLevel.rawValue] as? [String]
        }
    }
    
    var riderCompetition: [String]? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.competition), newValue)
        }
        get {
            return self.publicProperties?[Fields.competition.rawValue] as? [String]
        }
    }
    
    var riderHorseType: [String]? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.horseType), newValue)
        }
        get {
            return self.publicProperties?[Fields.horseType.rawValue] as? [String]
        }
    }

    var riderName: String? {
        set {
            self.applyPublicProperty(Fields.getRawValue(.name), newValue)
        }
        get {
            return self.publicProperties?[Fields.name.rawValue] as? String
        }
    }
    
    var riderPhoto: String? {
        set {
            self.applyPublicProperty(Fields.getRawValue(.photo), newValue)
        }
        get {
            return self.publicProperties?[Fields.photo.rawValue] as? String
        }
    }
    
    var riderReferenceName: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.referenceName), newValue)
        }
        get {
            return self.publicProperties?[Fields.referenceName.rawValue] as? String
        }
    }
    
    var riderReferencePhone: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.referencePhone), newValue)
        }
        get {
            return self.publicProperties?[Fields.referencePhone.rawValue] as? String
        }
    }
    
    var riderParentEmail: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.parentEmail), newValue)
        }
        get {
            return self.publicProperties?[Fields.parentEmail.rawValue] as? String
        }
    }
    
    var riderBirthDate: Date? {
        set {
            if newValue != nil {
                self.applyPublicProperty(Fields.getRawValue(.age), Int((newValue!.timeIntervalSince1970 * 1000.0).rounded()))
            }
            else {
                self.applyPublicProperty(Fields.getRawValue(.age), nil)
            }
        }
        get {
            if let value = self.publicProperties?[Fields.age.rawValue] as? Int {
                return Date(timeIntervalSince1970: TimeInterval(value / 1000))
            }
            return nil
        }
    }
    
    var riderAge: Int {
        get {
            if self.riderBirthDate == nil {
                return 0
            }
            let ageComponents = Calendar.current.dateComponents(Set<Calendar.Component>(arrayLiteral: .year), from: self.riderBirthDate!, to: Date())
            return ageComponents.year ?? 0
        }
    }
    
    var riderWeight: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.weight), newValue)
        }
        get {
            return self.publicProperties?[Fields.weight.rawValue] as? Int
        }
    }
    
    var riderLength: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.length), newValue)
        }
        get {
            return self.publicProperties?[Fields.length.rawValue] as? Int
        }
    }
    
    var riderRideFrequencyWeek: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.rideFrequencyWeek), newValue)
        }
        get {
            return self.publicProperties?[Fields.rideFrequencyWeek.rawValue] as? Int
        }
    }
    
    var riderRideFrequencyYear: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.rideFrequencyYear), newValue)
        }
        get {
            return self.publicProperties?[Fields.rideFrequencyYear.rawValue] as? Int
        }
    }
    
    var riderYearsTotal: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.yearsTotal), newValue)
        }
        get {
            return self.publicProperties?[Fields.yearsTotal.rawValue] as? Int
        }
    }
    
    var riderYearsSince: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.yearsSince), newValue)
        }
        get {
            return self.publicProperties?[Fields.yearsSince.rawValue] as? Int
        }
    }
    
    var riderNumberHorses: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.numberOfHorses), newValue)
        }
        get {
            return self.publicProperties?[Fields.numberOfHorses.rawValue] as? Int
        }
    }
    
    var riderYearsOwnedHorse: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.yearsOwnedHourse), newValue)
        }
        get {
            return self.publicProperties?[Fields.yearsOwnedHourse.rawValue] as? Int
        }
    }
    
    var riderYearsSinceOwnedHorse: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.yearsSinceOwnedHourse), newValue)
        }
        get {
            return self.publicProperties?[Fields.yearsSinceOwnedHourse.rawValue] as? Int
        }
    }
    
    var riderStableOwner: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.stableOwner), newValue)
        }
        get {
            return self.publicProperties?[Fields.stableOwner.rawValue] as? Bool
        }
    }
    
    var riderPrivateLesson: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.privateLesson), newValue)
        }
        get {
            return self.publicProperties?[Fields.privateLesson.rawValue] as? Bool
        }
    }
    
    var riderIndoorRiding: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.indoorRiding), newValue)
        }
        get {
            return self.publicProperties?[Fields.indoorRiding.rawValue] as? Bool
        }
    }
    
    var riderOutdoorRiding: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.outdoorRiding), newValue)
        }
        get {
            return self.publicProperties?[Fields.outdoorRiding.rawValue] as? Bool
        }
    }
    
    var riderGroupLesson: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.groupLesson), newValue)
        }
        get {
            return self.publicProperties?[Fields.groupLesson.rawValue] as? Bool
        }
    }
    
    var riderCoRider: Bool? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.coRider), newValue)
        }
        get {
            return self.publicProperties?[Fields.coRider.rawValue] as? Bool
        }
    }
    
    var riderHorseTemperamentMin: Int? {
        set {
            return self.applyPublicProperty(Fields.temperamentMin.rawValue, newValue)
        }
        get {
            return self.publicProperties?[Fields.temperamentMin.rawValue] as? Int
        }
    }
    
    var riderHorseTemperamentMax: Int? {
        set {
            return self.applyPublicProperty(Fields.temperamentMax.rawValue, newValue)
        }
        get {
            return self.publicProperties?[Fields.temperamentMax.rawValue] as? Int
        }
    }
    
    var riderCoRiderFrom: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.coRiderFrom), newValue)
        }
        get {
            return self.publicProperties?[Fields.coRiderFrom.rawValue] as? Int
        }
    }
    
    var riderCoRiderTo: Int? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.coRiderTo), newValue)
        }
        get {
            return self.publicProperties?[Fields.coRiderTo.rawValue] as? Int
        }
    }
    
    var riderAvailableFrom: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.availableFrom), newValue)
        }
        get {
            return self.publicProperties?[Fields.availableFrom.rawValue] as? String
        }
    }
    
    var riderAvailableTo: String? {
        set {
            return self.applyPublicProperty(Fields.getRawValue(.availableTo), newValue)
        }
        get {
            return self.publicProperties?[Fields.availableTo.rawValue] as? String
        }
    }
    
    
    var riderLessonType: String? {
        set {
            return self.applyPublicProperty(Fields.lessonType.rawValue, newValue)
        }
        get {
            return self.publicProperties?[Fields.lessonType.rawValue] as? String
        }
    }
    
    var riderTypeOfRiding: String? {
        set {
            return self.applyPublicProperty(Fields.typeOfRiding.rawValue, newValue)
        }
        get {
            return self.publicProperties?[Fields.typeOfRiding.rawValue] as? String
        }
    }
    
    var isChild: Bool {
        return self.riderAge <= CHILD_MAX_AGE
    }
}
