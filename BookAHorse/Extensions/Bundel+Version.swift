//
//  Bundel+Version.swift
//  BookAHorse
//
//  Created by Mikhail Li on 14/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
