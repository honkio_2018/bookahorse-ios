//
//  UIImageView+StylizedBorder.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/22/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

extension UIImageView {
    
    enum BorderSize {
        case small
        case medium
        case large
    }
    
    func setBorder(_ size: BorderSize) {
        self.layer.borderColor = UIColor(netHex: AppColors.appOrange).cgColor
        switch size {
        case .small:
            self.layer.borderWidth = 2.0
        case .medium:
            self.layer.borderWidth = 3.0
        case .large:
            self.layer.borderWidth = 4.0
        }
    }
    
}
