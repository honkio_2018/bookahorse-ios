//
//  BookedProduct+Info.swift
//  BookAHorse
//
//  Created by Mikhail Li on 23/08/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension Sequence where Element == BookedProduct {
    
    var names: String {
        get {
            guard self.underestimatedCount != 0 else { return "" }
            
            var result: [String] = []

            for booking in self {
                var bookingItem = "\(booking.product.name ?? "null")"
                
                if booking.count > 1 {
                    bookingItem += "(\(booking.count))"
                }
                result.append(bookingItem)
            }
            
            return result.joined(separator: ", ")
        }
    }
    
    var desc: String {
        guard self.underestimatedCount != 0 else { return "" }
        
        return self.first(where: { (product) -> Bool in
            return true
        })?.product.desc ?? "-"
    }
    
    var price: String {
        guard self.underestimatedCount != 0 else { return "" }
        
        var price = 0.0
        var currency: String!
        
        for booking in self {
            price += booking.getPrice()
            
            if currency == nil {
                currency = booking.product.currency
            }
        }
        
        return String(format: "%.2f %@", price, currency ?? "")
    }
}
