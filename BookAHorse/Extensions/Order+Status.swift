//
//  Order+Status.swift
//  BookAHorse
//
//  Created by Mikhail Li on 01/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension Order {
    
    enum Model: String {
        case model = "bookahorse"
        case modelRequest = "bookahorse_request"
        case modelInterest = "bookahorse_interest"
    }
    
    enum Status: String {
        case bookRequest = "book_request"
        case booked = "booked"
        case canceledByOwner = "cancelled_by_owner"
        case canceldByRider = "cancelled_by_rider"
        case rideCompleted = "ride_completed"
    }
    
    enum StatusRequest: String {
        case new = "new"
        case approved = "approved"
        case rejected = "rejected"
    }
    
    enum StatusInterest: String {
        case requested = "requested"
        case rejected = "rejected"
        case approved = "approved"
    }
    
    enum Custom: String {
        case CustomHorse = "horse"
        case CustomRequestDuration = "duration"
        case CustomComment = "comment"
        case CustomParentEmail = "parent_email"
    }
    
    var statusName: String {
        get {
            switch (self.status) {
            case Status.bookRequest.rawValue:
                return Str.order_status_book_request
            case Status.booked.rawValue:
                return Str.order_status_booked
            case Status.canceledByOwner.rawValue:
                return Str.order_status_cancelled_by_owner
            case Status.canceldByRider.rawValue:
                return Str.order_status_cancelled_by_rider
            case Status.rideCompleted.rawValue:
                return Str.order_status_completed
            case StatusRequest.new.rawValue:
                return Str.order_status_request_new
            case StatusRequest.approved.rawValue:
                return Str.order_status_request_approved
            case StatusRequest.rejected.rawValue:
                return Str.order_status_request_rejected
            default:
                return Str.order_status_unknown
            }
        }
    }
    
    func getHorseName() -> String {
        let horse = self.getCustom(Custom.CustomHorse.rawValue) as! HKAsset
        return horse.name
    }
    
    var comment: String? {
        set {
            self.setCustom(Custom.CustomComment.rawValue, value: newValue!)
        }
        get {
            return getCustom(Custom.CustomComment.rawValue) as? String
        }
    }
    
    var requestedDuration: Int? {
        set {
            self.setCustom(Custom.CustomRequestDuration.rawValue, value: newValue!)
        }
        get {
            let value = getCustom(Custom.CustomRequestDuration.rawValue) as? Int
            return value ?? 0
        }
    }
    
    var parentEmail: String? {
        set {
            if newValue == nil {
                self.removeCustom(Custom.CustomParentEmail.rawValue)
            }
            else {
                self.setCustom(Custom.CustomParentEmail.rawValue, value: newValue!)
            }
        }
        get {
            return getCustom(Custom.CustomRequestDuration.rawValue) as? String
        }
    }
}
