//
//  HKAsset+Horse.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension HKAsset {
    
    enum Fields: String {
        case horseBreed = "horse_breed"
        case horseType = "horse_type"
        case lessonType = "lessonType"
        case typeOfRiding = "typeOfRiding"
        case horseAddress = "horse_address"
        case horseYearsBeenRiding = "yearsRidden"
        case horseCompetitionField = "competitionField"
        case horseAfraid = "afraid_of"
        case horseCompetenceLevel = "competenceLevel"
        case horseRiderLengthMin = "riderHeight_min"
        case horseRiderLengthMax = "riderHeight_max"
        case horseRiderWeightMin = "riderWeight_min"
        case horseRiderWeightMax = "riderWeight_max"
        case horseRiderAgeMin = "riderAge_min"
        case horseRiderAgeMax = "riderAge_max"
        case horseTemperament = "temperament"
        case horseInsured = "insured"
        //dateOfBirth
        case horseBirthDate = "horse_birth_date"
        //not found in structure
        case horseOtherFear = "horse_other_fear"
        // description
        case horseDescription = "horse_description"
        case horsePhoto = "default_picture"
        //not found in structure
        case horseOwner = "horse_owner"
        //not found in structure
        case horseShop = "shop_id"
        //this is a key in horse structure that means that the parameter doesn't matter when comparing horse properties
        case keyDoesNotMatter = "any"
    }
    
    func applyProperty(_ key: String, _ value: Any?) {
        if self.properties == nil {
            self.properties = [:]
        }
        
        self.properties[key] = value
    }
    
    // Base
    
    var horseName: String? {
        set {
            if let value = newValue {
                self.name = value
            } else {
                self.name = ""
            }
        }
        get { return self.name }
    }
    
    var horseOwner: String? {
        set {
            self.applyProperty(Fields.horseOwner.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseOwner.rawValue] as? String
        }
    }
    
    var horseBreed: String? {
        set {
            self.applyProperty(Fields.horseBreed.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseBreed.rawValue] as? String
        }
    }
    
    var horseType: [String]? {
        set {
            self.applyProperty(Fields.horseType.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseType.rawValue] as? [String]
        }
    }
    
    var lessonType: String? {
        set {
            self.applyProperty(Fields.lessonType.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.lessonType.rawValue] as? String
        }
    }
    
    var typeOfRiding: String? {
        set {
            self.applyProperty(Fields.typeOfRiding.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.typeOfRiding.rawValue] as? String
        }
    }
    
    var horseCompeted: [String]? {
        set {
            self.applyProperty(Fields.horseCompetitionField.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseCompetitionField.rawValue] as? [String]
        }
    }
    
    var horseYearsBeenRiding: String? {
        set {
            self.applyProperty(Fields.horseYearsBeenRiding.rawValue, newValue)
        }
        get {
            return self.properties[Fields.horseYearsBeenRiding.rawValue] as? String
        }
    }
    
    var address: HKAddress? {
        set {
            self.applyProperty(Fields.horseAddress.rawValue, newValue?.toDict())
        }
        get {
            if let value = self.properties?[Fields.horseAddress.rawValue] as? [String: String]  {
                return HKAddress.init(fromDict: value)
            } else {
                return nil
            }
        }
    }
    
    var horseBirthDate: Date? {
        set {
            if let seconds = newValue?.timeIntervalSince1970 {
                self.applyProperty(Fields.horseBirthDate.rawValue, seconds * 1000)
            }
        }
        get {
            if let milliseconds = self.properties?[Fields.horseBirthDate.rawValue] as? Double {
                return Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
            }
            return nil
        }
    }
    
    var horseRiderWeightMin: Int? {
        set {
            self.applyProperty(Fields.horseRiderWeightMin.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderWeightMin.rawValue] as? Int
        }
    }
    
    var horseRiderWeightMax: Int? {
        set {
            self.applyProperty(Fields.horseRiderWeightMax.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderWeightMax.rawValue] as? Int
        }
    }
    
    var horseRiderLengthMin: Int? {
        set {
            self.applyProperty(Fields.horseRiderLengthMin.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderLengthMin.rawValue] as? Int
        }
    }
    
    var horseRiderLengthMax: Int? {
        set {
            self.applyProperty(Fields.horseRiderLengthMax.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderLengthMax.rawValue] as? Int
        }
    }
    
    var horseRiderAgeMin: Int? {
        set {
            self.applyProperty(Fields.horseRiderAgeMin.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderAgeMin.rawValue] as? Int
        }
    }
    
    var horseRiderAgeMax: Int? {
        set {
            self.applyProperty(Fields.horseRiderAgeMax.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseRiderAgeMax.rawValue] as? Int
        }
    }
    
    var horseOtherFear: String? {
        set {
            self.applyProperty(Fields.horseOtherFear.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseOtherFear.rawValue] as? String
        }
    }
    
    var horseAfraid: [String]? {
        set {
            self.applyProperty(Fields.horseAfraid.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseAfraid.rawValue] as? [String]
        }
    }
    
    var horseTemperament: Int? {
        set {
            self.applyProperty(Fields.horseTemperament.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseTemperament.rawValue] as? Int
        }
    }
    
    var horseInsured: Bool? {
        set {
            self.applyProperty(Fields.horseInsured.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseInsured.rawValue] as? Bool
        }
    }
    
    var horseCompetenceLevel: [String]? {
        set {
            self.applyProperty(Fields.horseCompetenceLevel.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseCompetenceLevel.rawValue] as? [String]
        }
    }
    
    var horseDescription: String? {
        set {
            self.applyProperty(Fields.horseDescription.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseDescription.rawValue] as? String
        }
    }
    
    var horsePhoto: String? {
        set {
            self.applyProperty(Fields.horsePhoto.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horsePhoto.rawValue] as? String
        }
    }
    
    var horseShop: String? {
        set {
            self.applyProperty(Fields.horseShop.rawValue, newValue)
        }
        get {
            return self.properties?[Fields.horseShop.rawValue] as? String
        }
    }
}
