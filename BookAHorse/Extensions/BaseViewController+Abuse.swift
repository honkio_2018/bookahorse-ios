//
//  HKBaseTableViewController+Abuse.swift
//  BookAHorse
//
//  Created by Mikhail Li on 13/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import Foundation
import HonkioAppTemplate

extension HKBaseViewController {
    
    public enum linkType: String {
        case asset
        case shop
        case userRole = "user_role"
        case order
    }
    
    private func sendAbuse(withComment: String, and okMessage: String, _ linkId: String!, _ linkType: String!) {
        HonkioApi.userSendAbuse(HonkioApi.mainShopInfo()?.merchant.merchantId,
                                linkId: linkId,
                                linkType: linkType,
                                comment: withComment,
                                flags: 0) { (response) in
                                    CustomAlertView(okTitle: nil,
                                                    okMessage: okMessage,
                                                    okAction: {
                                                        print(okMessage)
                                    }).show()
        }
    }

    open func showAbuseCommentDialog(_ title: String,
                                     _ message: String,
                                     _ hint: String,
                                     _ okMessage: String,
                                     _ linkId: String!,
                                     _ linkType: linkType) {
        CustomInputAlertView(title: title,
                             message: message,
                             action: { (input) in
                                // send abuse
                                self.sendAbuse(withComment: input, and: okMessage, linkId, linkType.rawValue)

        }).cancelAction({
            print("user canceled action in showAbuseCommentDialog")
        }).hint(hint).show()
    }
}
