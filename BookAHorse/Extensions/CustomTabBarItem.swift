//
//  UIBarButtonItem+InspectableScale.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

// class used for set scale pdf image for icon.

@IBDesignable
class CustomTabBarItem: UITabBarItem {
    
    @IBInspectable
    var scaledHeight: CGFloat = 0 {
        didSet {
            if !scaledWidth.isZero {
                self.image = self.image?.scaleTo(CGSize(width: self.scaledHeight, height: self.scaledWidth))
            }
        }
    }

    @IBInspectable
    var scaledWidth: CGFloat = 0 {
        didSet {
            if !scaledHeight.isZero {
                self.image = self.image?.scaleTo(CGSize(width: self.scaledHeight, height: self.scaledWidth))
            }
        }
    }
}
