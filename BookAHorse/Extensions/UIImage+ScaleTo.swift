//
//  UIImage+ScaleTo.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

// MARK: - Used to scale UIImages
extension UIImage {
    func scaleTo(_ newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage != nil ? newImage! : self
    }
}
