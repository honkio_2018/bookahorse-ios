//
//  CalendarEvent+Extension.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 10/2/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import HonkioAppTemplate

extension CalendarEvent {
    
    public var maxProductDuration: Double {
        get {
            if self.products == nil || self.products.count == 0 {
                return 0.0
            }
            
            var duration: Int = 0
            for product in self.products {
                if product.duration > duration {
                    duration = product.duration
                }
            }
            
            return Double(duration) / 1000
        }
    }

}
