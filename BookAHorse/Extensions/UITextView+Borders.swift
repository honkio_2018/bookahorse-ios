//
//  UITextView+Borders.swift
//  BookAHorse
//
//  Created by Dash on 26.09.2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

extension UITextView {
    
    private static let borderCollor: CGColor = UIColor(netHex: 0xeeeeee).cgColor
    
    @IBInspectable public var isHasBorders: Bool {
        get {
            return self.layer.borderWidth > 0
        }
        set {
            if newValue {
                self.clipsToBounds = true
                self.layer.borderWidth = 1
                self.layer.borderColor = UITextView.borderCollor
                self.layer.cornerRadius = 5
            }
            else {
                self.clipsToBounds = false
                self.layer.borderWidth = 0
            }
        }
    }
}
