//
//  AppColors.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/22/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class AppColors {

    public static let appGray = 0x9d9d9c
    public static let appOrange = 0xeb6d21
}
