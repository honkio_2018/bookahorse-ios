//
//  ShareUtils.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 9/1/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

class ShareUtils: NSObject {

    static func buildOrderShareUrl(_ orderId: String) -> String {
        return "\(AppSettings.ShareURL)/order/\(orderId)"
    }
    
    static func shareOrder(_ orderId: String, viewController: UIViewController) {
        ShareUtils.shareText(ShareUtils.buildOrderShareUrl(orderId), viewController: viewController)
    }
    
    static func buildAssetShareUrl(_ assetId: String) -> String {
        return "\(AppSettings.ShareURL)/asset/\(assetId)"
    }
    
    static func shareAsset(_ assetId: String, viewController: UIViewController) {
        ShareUtils.shareText(ShareUtils.buildAssetShareUrl(assetId), viewController: viewController)
    }
    
    static func shareText(_ text: String, viewController: UIViewController) {
        var item : Any = text
        if let url = NSURL(string: text) {
            item = url
        }
        let activityViewController = UIActivityViewController(activityItems: [ item ], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = viewController.view
        activityViewController.view.tintColor = UIColor.init(hex: AppColors.appOrange, alpha: 1)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]

        viewController.present(activityViewController, animated: true, completion: nil)
    }
}
