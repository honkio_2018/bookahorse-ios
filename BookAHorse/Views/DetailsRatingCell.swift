//
//  DetailsRatingCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class DetailsRatingCell: UITableViewCell {

    @IBOutlet weak var ratingView: UIStackView!
    let ratingImage = UIImage(named: "ic_ratings")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func showRating(_ ratings: RateList?) {
        let averageRating = ratings?.getAverageRate()
        for index in 0..<ratingView.arrangedSubviews.count {
            let imageView = ratingView.arrangedSubviews[Int(index)] as! UIImageView
            imageView.image = ratingImage
            if !(index < averageRating ?? 3) {
                imageView.image = UIImage(named: "ic_ratings_gray")
            }
            else {
                imageView.image = UIImage(named: "ic_ratings")
            }
        }
    }
    
}
