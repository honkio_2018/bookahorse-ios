//
//  PushItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/5/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class PushItemCell: UITableViewCell {
    
    @IBOutlet var labelTitle1 : UILabel!
    @IBOutlet var labelTitle2 : UILabel!
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelDesc : UILabel!
    @IBOutlet var photoView : UIImageView!
    @IBOutlet var unreadCheckView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.photoView.clipsToBounds = true
        self.photoView.layer.cornerRadius = 30
    }
}
