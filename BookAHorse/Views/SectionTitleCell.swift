//
//  SectionTitleCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/28/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class SectionTitleCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public static func buildBinder() -> ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)? {
        return { (viewController, cell, item) in
            let section = item as? (title: String, desc: String?)
            let sectionCell = cell as? SectionTitleCell
            
            sectionCell?.titleLabel.text = section?.title
            sectionCell?.descLabel?.text = section?.desc
            sectionCell?.separatorInset = UIEdgeInsetsMake(0, 0, 0, UIScreen.main.bounds.width)
        }
    }
}
