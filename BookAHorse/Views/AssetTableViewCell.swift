//
//  AssetTableViewCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 23/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class AssetTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundPhotoView: UIImageView!
    @IBOutlet weak var visibilityIconView: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scheduleIconView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
