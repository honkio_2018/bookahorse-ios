//
//  DetailRiderVideoCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol DetailRiderVideoCellDelegate: class {
    
    func actionRuderVideoEdit(_ cell: DetailRiderVideoCell)
    
}

class DetailRiderVideoCell: UITableViewCell {

    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    public weak var delegate: DetailRiderVideoCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tabRecogniser = UITapGestureRecognizer(target: self,
                                                   action: #selector(DetailRiderVideoCell.cellTap(_:)))
        self.contentView.addGestureRecognizer(tabRecogniser)
    }
    
    @objc public func cellTap(_ sender: Any?) {
        self.delegate?.actionRuderVideoEdit(self)
    }
}
