//
//  DetailsPhotoCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/9/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol DetailsPhotoCellMainPhotoDelegate: class {
    
    func mainPhotoDidClick(_ cell: DetailsPhotoCell, view: UIView)
    
}

class DetailsPhotoCell: UITableViewCell {

    @IBOutlet weak var mainPhotoView: UIImageView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var heighConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    
    public weak var mainPhotoDelegate: DetailsPhotoCellMainPhotoDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainPhotoView.clipsToBounds = true
        mainPhotoView.layer.cornerRadius = self.mainPhotoView.frame.height / 2
        
        mainPhotoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DetailsPhotoCell.mainPhotoDidClick(_:))))
        mainPhotoView.isUserInteractionEnabled = true
    }
    
    public func setLargeImage() {
        widthConstraint.constant = 120
        heighConstraint.constant = 120
        cellHeightConstraint.constant = 136
        
        sizeToFit()
        mainPhotoView.layer.cornerRadius = widthConstraint.constant / 2
    }
    
    
    @objc func mainPhotoDidClick(_ sender: Any?) {
        self.mainPhotoDelegate?.mainPhotoDidClick(self, view: self.mainPhotoView)
    }
}
