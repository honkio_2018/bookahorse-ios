//
//  OrderRateCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 18/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol OrderRateCellDelegate {
    func ratingDidSet(_ sender: OrderRateCell, _ ratingValue: Int)
}

class OrderRateCell: UITableViewCell {

    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var commentTextView: CustomUITextView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    
    public var delegate: OrderRateCellDelegate?
    
    private var selection: Int = 0;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        for index in 0..<ratingStackView.arrangedSubviews.count {
            let imageView = ratingStackView.arrangedSubviews[Int(index)] as! UIImageView
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleRateTap(_:)))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            imageView.addGestureRecognizer(tapGesture)
            imageView.isUserInteractionEnabled = true
            imageView.tag = index + 1
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func handleRateTap(_ sender: UITapGestureRecognizer) {
        if let tappedImageView = sender.view as? UIImageView {
            resetRating()
            setRating(of: tappedImageView)
        }
    }
    
    public func showRating(_ rating: Int?) {
        for index in 0..<ratingStackView.arrangedSubviews.count {
            let imageView = ratingStackView.arrangedSubviews[Int(index)] as! UIImageView
            if !(index < rating ?? 3) {
                imageView.image = UIImage(named: "ic_ratings_gray")
            }
            else {
                imageView.image = UIImage(named: "ic_ratings")
            }
        }
    }
    
    func setRating(of imageView: UIImageView) {
        if let index = ratingStackView.arrangedSubviews.index(of: imageView) {
            let ratingViews = ratingStackView.arrangedSubviews[...index]
            for rateImage in ratingViews {
                (rateImage as! UIImageView).image = UIImage(named: "ic_ratings")
            }
            self.selection = imageView.tag
            delegate?.ratingDidSet(self, self.selection)
        }
    }
    
    func resetRating() {
        ratingStackView.arrangedSubviews.forEach { rateImage in
            (rateImage as! UIImageView).image = UIImage(named: "ic_ratings_gray")
        }
    }

}
