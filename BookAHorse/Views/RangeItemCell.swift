//
//  RangeItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class RangeItemCell: UITableViewCell {
    
    @IBOutlet weak var slider: MARKRangeSlider!
    
    public var object: AnyObject?
    public var slideDelegate: ((_ object: AnyObject?, _ minValue: Double, _ maxValue: Double) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.slider.addTarget(self, action: #selector(onSlide(object:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func onSlide(object: AnyObject?) {
        if slideDelegate != nil {
            slideDelegate!(self.object, Double(self.slider?.leftValue ?? 0.0), Double(self.slider?.rightValue ?? 0.0))
        }
    }
}
