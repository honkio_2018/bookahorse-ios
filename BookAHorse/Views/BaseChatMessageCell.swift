//
//  BaseChatMessageCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/10/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
 The table view cell representing the use accoount item.
 */
open class BaseChatMessageCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIImageView!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
}
