//
//  CheckableItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/28/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class CheckableItemCell: UITableViewCell {
    
    @IBOutlet weak var switchView: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    
    public var object: AnyObject?
    public var switchDelegate: ((_ object: AnyObject?, _ isOn: Bool) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.switchView.addTarget(self, action: #selector(onSwitcChecked(object:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func onSwitcChecked(object: AnyObject?) {
        if switchDelegate != nil {
            switchDelegate!(self.object, self.switchView.isOn)
        }
    }
}
