//
//  DetailsEditItemCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 26/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class DetailsEditItemCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
