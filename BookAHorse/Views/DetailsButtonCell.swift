//
//  DetailsButtonCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 18/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class DetailsButtonCell: UITableViewCell {

    @IBOutlet weak var detailButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
