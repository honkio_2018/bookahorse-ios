//
//  MarkAllAsReadedCell.swift
//  BookAHorse
//
//  Created by Dash on 25.09.2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class MarkAllAsReadedCell: UITableViewCell {

    @IBOutlet var button : UIButton!
    
    public var isSetup = false
    
}
