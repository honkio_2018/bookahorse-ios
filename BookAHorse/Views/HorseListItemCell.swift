//
//  HorseListItemCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 04/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class HorseListItemCell: UITableViewCell {

    // MARK: - IBOutlets
    
    @IBOutlet weak var horseImage: UIImageView!
    @IBOutlet weak var horseName: UILabel!
    @IBOutlet weak var horseInfo: UILabel!
    @IBOutlet weak var sheduleImage: UIImageView!
    @IBOutlet weak var scheduleImageWidthConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        sheduleImage.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
}
