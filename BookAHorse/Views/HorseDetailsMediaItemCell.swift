//
//  HorseDetailsMediaItemCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

protocol HorseDetailsMediaItemCellDelegate: class {
    
    func onMediaItemClick(_ cell: HorseDetailsMediaItemCell, _ item: HKMediaFile?)
    
}

class HorseDetailsMediaItemCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var contextButton: UIView!
    
    public weak var delegate: HorseDetailsMediaItemCellDelegate?
    public weak var item: HKMediaFile?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HorseDetailsMediaItemCell.imageClick(_:))))
        self.imageView.isUserInteractionEnabled = true
        
        self.contextButton.tintColor = UIColor.white
        self.contextButton.tintColorDidChange()
    }

    @objc func imageClick(_ sender: Any?) {
        self.delegate?.onMediaItemClick(self, self.item)
    }
}
