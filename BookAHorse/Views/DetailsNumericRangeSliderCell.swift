//
//  DetailsNumericRangeSliderCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class DetailsNumericRangeSliderCell: UITableViewCell {

    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var minValueLabel: UILabel!
    @IBOutlet weak var maxValueLabel: UILabel!
    @IBOutlet weak var valueRangeSlider: MARKRangeSlider!
    
    public var object: AnyObject?
    public var slideDelegate: ((_ object: AnyObject?, _ minValue: Double, _ maxValue: Double) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.valueRangeSlider.addTarget(self, action: #selector(onSlide(object:)), for: UIControlEvents.valueChanged)
        self.valueRangeSlider.addTarget(self, action: #selector(onSlideEnd(object:)), for: UIControlEvents.touchDragExit)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func onSlide(object: AnyObject?) {
        let leftValue = round(Double(self.valueRangeSlider?.leftValue ?? 0.0))
        let rightValue = round(Double(self.valueRangeSlider?.rightValue ?? 0.0))
        
        if slideDelegate != nil {
            slideDelegate!(self.object, leftValue, rightValue)
        }
    }
    
    @objc func onSlideEnd(object: AnyObject?) {
        let leftValue = round(Double(self.valueRangeSlider?.leftValue ?? 0.0))
        let rightValue = round(Double(self.valueRangeSlider?.rightValue ?? 0.0))
        
        self.valueRangeSlider.setValue(CGFloat(leftValue), right: CGFloat(rightValue))
    }
    
}
