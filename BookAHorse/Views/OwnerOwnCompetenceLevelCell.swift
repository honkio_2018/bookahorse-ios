//
//  OwnerOwnCompetenceLevelCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 5/31/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class OwnerOwnCompetenceLevelCell: UITableViewCell {
    
    @IBOutlet weak var labelButton: UILabel!
    
    public var object: AnyObject?
    public var buttonDelegate: ((_ cell: OwnerOwnCompetenceLevelCell, _ object: AnyObject?) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.labelButton.isUserInteractionEnabled = true
        self.labelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onButtonTap(object:))))
    }
    
    @objc func onButtonTap(object: AnyObject?) {
        if buttonDelegate != nil {
            buttonDelegate!(self, self.object)
        }
    }
}
