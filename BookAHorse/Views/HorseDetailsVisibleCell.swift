//
//  HorseDetailsVisibleCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol HorseDetailsVisibleCellDelegate: class {
    func visibleButtonClick(_ cell: HorseDetailsVisibleCell)
}

class HorseDetailsVisibleCell: UITableViewCell {

    @IBOutlet weak var visibleLabel: UILabel!
    @IBOutlet weak var publishButton: UIButton!
    
    public weak var delegate: HorseDetailsVisibleCellDelegate?
    
    @IBAction
    func visibleButtonClick(_ sender: Any?) {
        self.delegate?.visibleButtonClick(self)
    }
}
