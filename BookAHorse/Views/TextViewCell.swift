//
//  TextViewCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 09/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell {

    @IBOutlet weak var textView: CustomUITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
