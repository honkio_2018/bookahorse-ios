//
//  TwoDetailItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/21/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class TwoDetailItemCell: UITableViewCell {
    
    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var detailInfo1Label: UILabel!
    @IBOutlet weak var detailInfo2Label: UILabel!
    
    private static let binder: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void) = { (viewController, cell, item) in
        let detailCell = cell as! TwoDetailItemCell
        let data = item as? (title: String, value1: String?, value2: String?)
        
        detailCell.detailNameLabel.text = data?.title
        detailCell.detailInfo1Label.text = data?.value1 ?? "-"
        detailCell.detailInfo2Label.text = data?.value2 ?? "-"
    }
    
    public static func buildSourceItem(_ title: String, _ value1: String?, _ value2: String?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return ("TwoDetailItemCell", (title: title, value1: value1, value2: value2), TwoDetailItemCell.binder)
    }
}
