//
//  ListItemProductSelectCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 02/07/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class ListItemProductSelectCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
