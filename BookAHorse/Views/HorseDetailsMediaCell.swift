//
//  HorseDetailsMediaCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class HorseDetailsMediaCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, HorseDetailsMediaItemCellDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    public var mediaFiles: HKMediaFileList? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    public var isEditable = false;
    public weak var delegate: HorseDetailsMediaItemCellDelegate?
    
    private let ItemCellIdentifier = "HorseDetailsMediaItemCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.register(UINib(nibName: ItemCellIdentifier, bundle: nil), forCellWithReuseIdentifier: ItemCellIdentifier)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.mediaFiles?.list.count ?? 0) + (isEditable ? 1 : 0)
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: HorseDetailsMediaItemCell? = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCellIdentifier, for: indexPath) as? HorseDetailsMediaItemCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(ItemCellIdentifier, owner: self, options: nil)!.last as? HorseDetailsMediaItemCell
        }
    
        if indexPath.row >= (self.mediaFiles?.list.count ?? 0) {
            cell?.imageView.image = UIImage(named: "ic_add_photo_video")
            cell?.imageView.contentMode = .scaleAspectFit
            cell?.contextButton.isHidden = true
            cell?.item = nil
        }
        else {
            cell?.contextButton.isHidden = !self.isEditable
            
            let item = self.mediaFiles?.list[indexPath.row]
            if item?.type == "video" {
                cell?.imageView.image = UIImage(named: "ic_play_movie")
                cell?.imageView.contentMode = .scaleAspectFit
            } else {
                cell?.imageView.imageFromURL(link: item?.url, contentMode: nil, fallAction: {})
            }
            cell?.item = item
        }
        
        cell?.delegate = self
            
        return cell!
    }
    
    func onMediaItemClick(_ cell: HorseDetailsMediaItemCell, _ item: HKMediaFile?) {
        self.delegate?.onMediaItemClick(cell, item)
    }
    
}
