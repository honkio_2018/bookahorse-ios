//
//  ReportItemTableCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 3/1/19.
//  Copyright © 2019 Honkio. All rights reserved.
//

import UIKit

class ReportItemTableCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelCurrency: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setBold(_ isBold: Bool) {
        if isBold {
            self.labelTitle.font = UIFont.boldSystemFont(ofSize: self.labelTitle.font.pointSize)
            self.labelAmount.font = UIFont.boldSystemFont(ofSize: self.labelAmount.font.pointSize)
            self.labelCurrency.font = UIFont.boldSystemFont(ofSize: self.labelCurrency.font.pointSize)
        }
        else {
            self.labelTitle.font = UIFont.systemFont(ofSize: self.labelTitle.font.pointSize)
            self.labelAmount.font = UIFont.systemFont(ofSize: self.labelAmount.font.pointSize)
            self.labelCurrency.font = UIFont.systemFont(ofSize: self.labelCurrency.font.pointSize)
        }
    }
    
}
