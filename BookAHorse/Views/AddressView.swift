//
//  AddressView.swift
//
//
//  Created by Shurygin Denis on 8/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import AddressBookUI
import HonkioAppTemplate

class AddressView: UIView, UITextFieldDelegate {
    
    public private(set) var address1Text : UITextField!
    public private(set) var address2Text : UITextField!
    public private(set) var cityText : UITextField!
    public private(set) var adminText : UITextField!
    public private(set) var countryValueButton : UIButton!
    public private(set) var postCodeText : UITextField!
    
    public var textDelegate: UITextFieldDelegate?
    
    public var location: CLLocation?
    public private(set) var countryIso: String?
    public private(set) var countryName: String?
    public var isLocationRequired = false
    public var isLockCountry = false
    public weak var parentViewController: UIViewController!
    
    public var requiredFields: [UITextField?]?
    
    private let emptyTextValidator = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    private var isAddressTextChanged = false
    
    private var _geocoder: GeocoderProtocol!
    
    public var geocoder: GeocoderProtocol {
        get {
            if _geocoder == nil {
                _geocoder = GoogleGeocoder()
            }
            return _geocoder
        }
        
        set {
            _geocoder = newValue
        }
    }
    
    public func setAddress(_ address: HKAddress?) {
        if address != nil {
            self.address1Text.text = address?.address1 ?? ""
            self.address2Text.text = address?.address2 ?? ""
            self.cityText.text = address?.city ?? ""
            self.adminText.text = address?.admin ?? ""
            self.postCodeText.text = address?.postalCode ?? ""
            self.setCountry(address?.countryIso, name: address?.countryName)
        }
    }
    
    public func setCountry(_ iso: String?, name: String?) {
        self.countryIso = iso
        self.countryName = name
        
        if self.countryIso != nil && (self.countryIso?.count ?? 0) > 0 {
            var country = HKCountries.findCounry(byIso: self.countryIso!)
            if country == nil {
                country = HKCountries.findCounry(byIso: "FIN")
            }
            
            self.countryIso = country?.iso
            self.countryName = country?.name
            
            let locale = NSLocale(localeIdentifier: NSLocale.current.identifier)
            let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue : self.countryIso!])
            self.countryName = locale.displayName(forKey: NSLocale.Key.identifier, value: identifier) ?? country?.name
        }
        
        if self.countryValueButton != nil {
            self.countryValueButton.setTitle(self.countryName ?? "-", for: .normal)
        }
        isAddressTextChanged = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.makeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.makeView()
    }
    
    func actionSetLocation(_ locationName: String?) {
        if locationName == nil {
            return
        }
        self.geocoder.fromLocationName(locationName!, callback: {[unowned self] (place, location) in
            if place != nil {
                self.placeDidSelected(place, location, false)
            }
            else {
                CustomAlertView(okTitle: nil, okMessage: Str.maps_search_address_not_found, okAction: nil).show()
            }
        })
    }
    
    @objc func actionSetCurrentLocation() {
        if let location = HonkioApi.deviceLocation() {
            BusyIndicator.shared.showProgressView(self.parentViewController.view)
            self.geocoder.fromLocation(location, callback: {[unowned self] (place, location) in
                BusyIndicator.shared.hideProgressView()
                if place != nil {
                    self.placeDidSelected(place, location, self.isLockCountry)
                }
            })
        }
        else {
            // TODO failed to find location
        }
    }
    
    @objc func actionOpenMap() {
        if let viewController = AppController.instance.getViewControllerByIdentifier("MapViewController", viewController: self.parentViewController) as? MapViewController {
            
            viewController.readOnly = false
            viewController.location = self.location
            
            if self.countryIso != nil && self.countryIso!.count > 0 {
                viewController.userCountry = self.countryIso!
            }
            
            viewController.completeAction = { [unowned self] (viewController: MapViewController, place: HKAddress?, location: CLLocation?) in
                self.placeDidSelected(place, location, self.isLockCountry)
                viewController.dismiss(animated: true, completion: nil)
            }
            
            let navi = UINavigationController(rootViewController: viewController)
            
            parentViewController.present(navi, animated: true, completion: nil)
        }
    }
    
    @objc func actionSelectCountry() {
        
        if let controller = AppController.instance.getViewControllerByIdentifier("CountriesTableViewController", viewController: self.parentViewController) as? CountriesTableViewController {
            controller.completionAction = {[unowned self] (country) in
                self.setCountry(country.iso, name: country.name)
            }
            
            self.parentViewController.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
        }
        
    }
    
    func isValid() -> Bool {
        if self.isLocationRequired && self.location == nil {
            return false
        }
        
        var result = true
        if self.requiredFields != nil {
            for field in self.requiredFields! {
                if field != nil {
                    result = self.emptyTextValidator.validate(field) && result
                }
            }
        }
        return result
    }
    
    func syncAddress(_ completionAction: @escaping ()-> Void) {
        if self.isAddressTextChanged {
            self.geocoder.fromLocationName(self.fullAddress(), callback: {[unowned self] (place, location) in
                if place != nil
                    && (place!.address1?.count ?? 0) > 0
                    && (place!.postalCode?.count ?? 0) > 0 {
                    self.location = location
                }
                else {
                    self.location = nil
                }
                self.isAddressTextChanged = false
                completionAction()
            })
        }
        else {
            completionAction()
        }
    }
    
    func getAddress() -> HKAddress {
        let address = HKAddress()!
        address.address1 = self.address1Text.text
        address.address2 = self.address2Text.text
        address.city = self.cityText.text
        address.admin = self.adminText.text
        address.postalCode = self.postCodeText.text
        address.setCountry(self.countryIso, name: self.countryName)
        return address
    }
    
    private func fullAddress() -> String {
        return String(format: "%@, %@, %@, %@, %@, %@",
                      self.postCodeText.text ?? "",
                      self.countryName ?? "",
                      self.adminText.text ?? "",
                      self.cityText.text ?? "",
                      self.address1Text.text ?? "",
                      self.address2Text.text ?? "")
    }
    
    fileprivate func placeDidSelected(_ place: HKAddress?, _ location: CLLocation?, _ isLockCountry: Bool) {
        if isLockCountry && HKCountries.toIso3(self.countryIso) != HKCountries.toIso3(place?.countryIso) {
            CustomAlertView(okTitle: nil, okMessage: "registration_p_dlg_country_not_same_message".localized, okAction: nil).show()
        }
        else if place != nil {
            self.location = location
            self.address1Text.text = place?.address1
            self.address2Text.text = ""
            self.cityText.text = place?.city
            //            self.adminText.text = place?.admin
            self.postCodeText.text = place?.postalCode
            self.setCountry(place?.countryIso, name: place?.countryName)
            
            isAddressTextChanged = false
        }
    }
    
    fileprivate func makeView() {
        // ============================
        // Create views
        let buttons = makeButtons()
        
        address1Text = self.makeText("view_address_address_1".localized)
        address1Text.delegate = self
        
        address2Text = self.makeText("view_address_address_2".localized)
        address2Text.delegate = self
        
        cityText = self.makeText("view_address_address_city".localized)
        cityText.delegate = self
        
        adminText = self.makeText("view_address_address_admin".localized)
        adminText.delegate = self
        
        countryValueButton = self.makeButton(self.countryName ?? "settings_addres_country_title".localized)
        countryValueButton.addTarget(self, action: #selector(actionSelectCountry), for: .touchUpInside)
        
        postCodeText = self.makeText("view_address_postal_code".localized)
        postCodeText.delegate = self
        
        // ============================
        // Add views
        self.addSubview(buttons)
        
        self.addSubview(address1Text)
        self.addSubview(address2Text)
        self.addSubview(cityText)
        self.addSubview(adminText)
        self.addSubview(countryValueButton)
        self.addSubview(postCodeText)
        
        // ============================
        // Constraints
        let views: [String : AnyObject] = [
            "buttons" : buttons,
            
            "address1Text" : address1Text,
            "address2Text" : address2Text,
            "cityText" : cityText,
            "adminText" : adminText,
            "postCodeText" : postCodeText,
            
            "countryValueButton" : countryValueButton
        ]
        
        let formatArray = [
            "H:|-0-[address1Text]-0-|",
            "H:|-0-[address2Text]-0-|",
            "H:|-0-[cityText]-0-|",
            "H:|-0-[adminText]-0-|",
            "H:|-0-[postCodeText]-0-|",
            
            "H:|-0-[countryValueButton]-0-|",
            
            "V:|-8-[buttons]-16-[address1Text]" +
                "-(space)-[address2Text]" +
                "-(space)-[cityText]" +
                "-(space)-[adminText]" +
                "-(space)-[postCodeText]" +
            "-(space)-[countryValueButton]-8-|"
        ]
        
        let layoutMetrics: [String : AnyObject] = [
            "space": CGFloat(12.0) as AnyObject,
            ]
        
        let formatOptions = NSLayoutFormatOptions(rawValue:0)
        
        var constraints = NSLayoutConstraint.constraintsWithFormatArray(formatArray, options:formatOptions, metrics: layoutMetrics, views: views)
        constraints.append(NSLayoutConstraint(item: buttons, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    fileprivate func makeButtons() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let button1 = makeButton(
            title: "view_address_btn_current_location".localized,
            selector: #selector(actionSetCurrentLocation))
        
        let button2 = makeButton(
            title: "view_address_btn_select_on_map".localized,
            selector: #selector(actionOpenMap))
        
        view.addSubview(button1)
        view.addSubview(button2)
        
        let views = ["button1" : button1,
                     "button2" : button2]
        
        let formatArray = [
            "H:|-0-[button1(<=150)]-8-[button2(<=150)]-0-|",
            "H:[button1(==button2)]",
            "V:|-0-[button1]-0-|",
            "V:|-0-[button2]-0-|"
        ]
        
        let formatOptions = NSLayoutFormatOptions(rawValue:0)
        
        let constraints = NSLayoutConstraint.constraintsWithFormatArray(formatArray, options:formatOptions, metrics: nil, views: views)
        NSLayoutConstraint.activate(constraints)
        
        return view
    }
    
    fileprivate func makeButton(title: String, selector: Selector) -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor =  UIColor(netHex: AppColors.appOrange)
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        
        let titleLabel = makeLabel2(title)
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.white
        
        let actionButton = UIButton()
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.addTarget(self, action: selector, for: .touchUpInside)
        
        view.addSubview(titleLabel)
        view.addSubview(actionButton)
        
        // Constraints
        let views = [
            "titleLabel" : titleLabel,
            "actionButton" : actionButton
        ]
        
        let formatArray = [
            "H:|-8-[titleLabel]-8-|",
            "V:|-8-[titleLabel]-8-|",
            
            "H:|-0-[actionButton]-0-|",
            "V:|-0-[actionButton(==80)]-0-|"
        ]
        
        let formatOptions = NSLayoutFormatOptions(rawValue:0)
        
        let constraints = NSLayoutConstraint.constraintsWithFormatArray(formatArray, options:formatOptions, metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        return view
    }
    
    fileprivate func makeLabel2(_ text: String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.numberOfLines = 0
        
        label.text = text
        
        return label
    }
    
    fileprivate func makeLabel(_ text: String) -> UIView {
        let label = UIView()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }
    
    fileprivate func makeText(_ hint: String) -> UITextField {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        textField.placeholder = hint
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.borderStyle = UITextBorderStyle.roundedRect
        
        return textField
    }
    
    fileprivate func makeButton(_ title: String) -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor(netHex: AppColors.appOrange), for: .normal)
        button.contentHorizontalAlignment = .left
        
        button.setTitle(title, for: .normal)
        
        return button
    }
    
    // =================================================================
    // Protocol UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        isAddressTextChanged = true
        return self.textDelegate?.textField?(textField, shouldChangeCharactersIn:range, replacementString:string) ?? true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.textDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return self.textDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.textDelegate?.textFieldDidEndEditing?(textField)
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return self.textDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.textDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
}
