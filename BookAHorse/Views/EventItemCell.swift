//
//  EventsItemCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class EventItemCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bookButton.layer.cornerRadius = 5
    }
}
