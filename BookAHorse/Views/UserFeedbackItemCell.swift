//
//  UserFeedbackItemCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 25/09/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol UserFeedbackItemCellDelegate: class {
    
    func feedbackDidtap(_ cell: UserFeedbackItemCell, view: UIView)
    
}

class UserFeedbackItemCell: UITableViewCell {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var rateRecipientLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        photoView.clipsToBounds = true
        photoView.layer.cornerRadius = self.photoView.frame.height / 2
        
        cardView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func showRating(_ rating: Int?) {
        for index in 0..<ratingStackView.arrangedSubviews.count {
            let imageView = ratingStackView.arrangedSubviews[Int(index)] as! UIImageView
            if !(index < rating ?? 3) {
                imageView.image = UIImage(named: "ic_ratings_gray")
            }
            else {
                imageView.image = UIImage(named: "ic_ratings")
            }
        }
    }
}
