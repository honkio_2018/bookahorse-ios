//
//  ListItemEventCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

protocol ListItemEventCellDelegate: class {
    
    func actionBook(_ cell: ListItemEventCell)
    
}

class ListItemEventCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var productsLabel: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    public weak var delegate: ListItemEventCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionBook(_ sender: UIButton) {
        if (delegate != nil) {
            delegate?.actionBook(self)
        }
    }
}
