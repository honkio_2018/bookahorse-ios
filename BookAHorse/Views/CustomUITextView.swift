//
//  CustomUITextView.swift
//  BookAHorse
//
//  Created by Dash on 27.09.2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class CustomUITextView: UITextView, UITextViewDelegate, NSTextStorageDelegate {
    
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    private weak var _delegate: UITextViewDelegate?
    override weak open var delegate: UITextViewDelegate? {
        set {
            if newValue === self {
                self._delegate = nil
            }
            else {
                self._delegate = newValue
            }
        }
        get {
            return self
        }
    }
    
    @IBInspectable public var localizedPlaceholder : String? {
        set {
            if newValue != nil {
                self.placeholder = newValue?.localized
            }
            else {
                self.placeholder = nil
            }
        }
        
        get {
            return self.placeholder
        }
    }
    
    @IBInspectable public var placeholder: String? {
        get {
            return placeholderLabel?.text
        }
        set {
            if let placeholderLbl = placeholderLabel {
                placeholderLbl.text = newValue
                placeholderLbl.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    private var placeholderLabel: UILabel?
    
    // support programmatic change text property
    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorageEditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if self.delegate?.responds(to: #selector(UITextViewDelegate.textViewDidChange(_:))) ?? false {
            self.delegate?.textViewDidChange!(self)
        }
    }
    
    private func resizePlaceholder() {
        if let placeholderLbl = placeholderLabel {
            let x = self.textContainer.lineFragmentPadding
            let y = self.textContainerInset.top - 2
            let width = self.frame.width - (x * 2)
            let height = placeholderLbl.frame.height
            
            placeholderLbl.frame = CGRect(x: x, y: y, width: width, height: height)
        }
    }
    
    private func addPlaceholder(_ placeholderText: String) {
        if placeholderLabel == nil {
            placeholderLabel = UILabel()
            placeholderLabel?.numberOfLines = 0
            placeholderLabel?.lineBreakMode = .byWordWrapping
            placeholderLabel?.font = self.font
            placeholderLabel?.textColor = UIColor.lightGray
            self.addSubview(placeholderLabel!)
        }
        placeholderLabel?.text = placeholderText
        placeholderLabel?.sizeToFit()
        placeholderLabel?.isHidden = !self.text.isEmpty
        
        self.resizePlaceholder()
        
        // TODO write wrapper for delegate
        super.delegate = self
        self.textStorage.delegate = self
    }
}

extension CustomUITextView {
    
    open func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewShouldBeginEditing(_:))) ?? false {
            return self._delegate!.textViewShouldBeginEditing!(self)
        }
        return true
    }
    
    open func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewShouldEndEditing(_:))) ?? false {
            return self._delegate!.textViewShouldEndEditing!(self)
        }
        return true
    }
    
    
    open func textViewDidBeginEditing(_ textView: UITextView) {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewDidBeginEditing(_:))) ?? false {
            self._delegate!.textViewDidBeginEditing!(self)
        }
    }
    
    open func textViewDidEndEditing(_ textView: UITextView) {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewDidEndEditing(_:))) ?? false {
            self._delegate!.textViewDidEndEditing!(self)
        }
    }
    
    
    open func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textView(_:shouldChangeTextIn:replacementText:))) ?? false {
            return self._delegate!.textView!(textView, shouldChangeTextIn: range, replacementText: text)
        }
        return true
    }
    
    open func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel?.isHidden = !self.text.isEmpty
        self.resizePlaceholder()
        
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewDidChange(_:))) ?? false {
            self._delegate!.textViewDidChange!(self)
        }
    }
    
    open func textViewDidChangeSelection(_ textView: UITextView) {
        if self._delegate?.responds(to: #selector(UITextViewDelegate.textViewDidChangeSelection(_:))) ?? false {
            self._delegate!.textViewDidChangeSelection!(self)
        }
    }
    
    
//    open func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        if self._delegate?.responds(to:
//            #selector(
//                UITextViewDelegate.textView(_:shouldInteractWith:in:interaction:) as (UITextViewDelegate) -> (UITextView) -> (URL) -> (NSRange) -> (UITextItemInteraction) -> (Bool)
//            )) ?? false {
//            return self._delegate!.textView!(textView, shouldInteractWith: URL, in: characterRange, interaction: interaction)
//        }
//
//        return true
//    }
//
//    open func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        if self._delegate?.responds(to: #selector(UITextViewDelegate.textView(_:shouldInteractWith:in:interaction:))) ?? false {
//            return self._delegate!.textView!(textView, shouldInteractWith: textAttachment, in: characterRange, interaction: interaction)
//        }
//        return true
//    }
}
