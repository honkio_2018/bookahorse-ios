//
//  BackgroundImageView.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/6/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class BackgroundImageView: UIImageView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.alpha = 0.2
    }

}
