//
//  OrderCommentCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 7/4/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class OrderCommentCell: UITableViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelComment: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var ratingView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func showRating(_ rating: Int?) {
        for index in 0..<ratingView.arrangedSubviews.count {
            let imageView = ratingView.arrangedSubviews[Int(index)] as! UIImageView
            if !(index < rating ?? 3) {
                imageView.image = UIImage(named: "ic_ratings_gray")
            }
            else {
                imageView.image = UIImage(named: "ic_ratings")
            }
        }
    }
}
