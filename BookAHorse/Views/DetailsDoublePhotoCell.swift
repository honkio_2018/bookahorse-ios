//
//  HorseDetailsProfileCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol DetailsPhotoCellChildPhotoDelegate: class {
    
    func childPhotoDidClick(_ cell: DetailsDoublePhotoCell, view: UIView)
    
}

class DetailsDoublePhotoCell: DetailsPhotoCell {

    @IBOutlet weak var childPhotoView: UIImageView!
    
    public weak var childPhotoDelegate: DetailsPhotoCellChildPhotoDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.childPhotoView.clipsToBounds = true
        self.childPhotoView.layer.cornerRadius = 40
        
        self.childPhotoView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DetailsDoublePhotoCell.childPhotoDidClick(_:))))
        self.childPhotoView.isUserInteractionEnabled = true
    }
    
    @objc func childPhotoDidClick(_ sender: Any?) {
        self.childPhotoDelegate?.childPhotoDidClick(self, view: self.childPhotoView)
    }
    
}
