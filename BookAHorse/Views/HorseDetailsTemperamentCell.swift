//
//  HorseDetailsTemperamentCell.swift
//  
//
//  Created by Mikhail Li on 09/06/2018.
//

import UIKit

class HorseDetailsTemperamentCell: UITableViewCell {

    @IBOutlet var temperametSlider: UISlider!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public var slideDelegate: ((_ object: AnyObject?, _ value: Float) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.temperametSlider.addTarget(self, action: #selector(onSlide(object:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func onSlide(object: AnyObject?) {
        let value = round(temperametSlider.value)
        temperametSlider.setValue(value, animated: true)
        if slideDelegate != nil {
            slideDelegate!(self, value)
        }
    }
}
