//
//  HorseDetailsCell.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/6/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

protocol HorseDetailsCellDelegate: class {
    
    func horseImageClick(_ cell: HorseDetailsCell)
    func horseAddressClick(_ cell: HorseDetailsCell)
    func ownerImageClick(_ cell: HorseDetailsCell)
    
}

class HorseDetailsCell: UITableViewCell {

    @IBOutlet weak var imgHorse: UIImageView!
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var labelHorseName: UILabel!
    @IBOutlet weak var labelHorseLocation: UILabel!
    @IBOutlet weak var imgLocationPin: UIImageView!
    @IBOutlet weak var labelOwnerName: UILabel!
    
    public weak var delegate: HorseDetailsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let horseImageTap = UITapGestureRecognizer(target: self, action: #selector(HorseDetailsCell.horseImageClick(_:)))
        let horseAddressTap = UITapGestureRecognizer(target: self, action: #selector(HorseDetailsCell.horseAddressClick(_:)))
        let ownerImageTap = UITapGestureRecognizer(target: self, action: #selector(HorseDetailsCell.ownerImageClick(_:)))
        
        self.imgHorse.addGestureRecognizer(horseImageTap)
        self.labelHorseLocation.addGestureRecognizer(horseAddressTap)
        self.imgOwner.addGestureRecognizer(ownerImageTap)
        
        self.imgHorse.clipsToBounds = true
        self.imgHorse.layer.cornerRadius = 45
        self.imgHorse.setBorder(.medium)
        self.imgOwner.clipsToBounds = true
        self.imgOwner.layer.cornerRadius = 27
        self.imgOwner.setBorder(.small)
        self.imgLocationPin.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
    @objc func horseImageClick(_ sender: Any?) {
        self.delegate?.horseImageClick(self)
    }
    
    @objc func horseAddressClick(_ sender: Any?) {
        self.delegate?.horseAddressClick(self)
    }
    
    @objc func ownerImageClick(_ sender: Any?) {
        self.delegate?.ownerImageClick(self)
    }
}
