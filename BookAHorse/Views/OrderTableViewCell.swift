//
//  OrderTableViewCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 19/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var horseNameLabel: UILabel!
    @IBOutlet weak var calendarIcon: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var topOrderTitle: NSLayoutConstraint!
    
    @IBOutlet public var imgHasMessages : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.photoView.clipsToBounds = true
        self.photoView.layer.cornerRadius = 30
        
        self.imgHasMessages.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
}
