//
//  DetailItemCell.swift
//  BookAHorse
//
//  Created by Mikhail Li on 06/06/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

class DetailItemCell: UITableViewCell {

    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var detailInfoLabel: UILabel!
    
    private static let binder: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void) = { (viewController, cell, item) in
        let detailCell = cell as! DetailItemCell
        let data = item as? (title: String, value: String?)
        
        detailCell.detailNameLabel.text = data?.title
        detailCell.detailInfoLabel.text = data?.value ?? "-"
    }
    
    public static func buildSourceItem(_ title: String, _ value: Double?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailItemCell.buildSourceItem(title, value != nil ? String(value!) : nil)
    }
    
    public static func buildSourceItem(_ title: String, _ value: Int?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailItemCell.buildSourceItem(title, value != nil ? String(value!) : nil)
    }
    
    public static func buildSourceItem(_ title: String, _ value: Bool?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        return DetailItemCell.buildSourceItem(title, (value ?? false) ? Str.screen_rider_details_yes : Str.screen_rider_details_no)
    }
    
    public static func buildSourceItem(_ title: String, _ value: String?) -> (identifierOrNibName: String, item: Any?, initializer: ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)?) {
        // for empty string fields check empty (ex. bankSwift)
        return ("DetailItemCell", (title: title, value: value != nil && value!.isEmpty ? nil : value), DetailItemCell.binder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
