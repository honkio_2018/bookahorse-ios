//
//  AppDelegate.swift
//  BookAHorse
//
//  Created by Mikhail Li on 11/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: HKBaseAppDelegate {
    
//    #if DEBUG
//        private let appUrl = "http://api.honkio2.webdev.softdev.com/"
//    #else
        private let appUrl = "https://beta-api.honkio.com/"
//    #endif
    private let appIdentity : Identity = Identity(id: "com/honkio/bookahorse",
                                         andPassword: "v9VWO6DtomsuO9r5frj99RxLFCZ7aPfAKqpz88I3")
    
    // MARK: - HonkioSdk
    override func applicationShouldInit() {
        HonkioApi.initInstance(withUrl: AppSettings.AppURL, app: AppSettings.AppIdentity)
        AppController.initInstance(AppViewControllerFactory(storyboards: ["Main", "MainRider", "MainOwner", "Registration"]))
        AppErrors.initInstance(BaseAppErrors())
        AppApi.initInstance(instance: AppApiInstance())
    }

    // MARK: - Lifecycle
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if super.application(application, didFinishLaunchingWithOptions: launchOptions) {
            
            Fabric.with([Crashlytics.self])
            
            // Remove all cached responses to avoid obsolete images
            URLCache.shared.removeAllCachedResponses()
            
            // Create MainWindow
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = HKLaunchViewController()
            self.window?.backgroundColor = UIColor.white
            self.window?.makeKeyAndVisible()
            
            application.applicationIconBadgeNumber = 0;

            // Appearance
            UITabBar.appearance().isTranslucent = false
            
            UISwitch.appearance().onTintColor = UIColor(netHex: AppColors.appOrange)
            
            let titleAttributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: CGFloat(20)),
                              NSAttributedStringKey.foregroundColor: UIColor(netHex: AppColors.appOrange)]
            
            UINavigationBar.appearance().titleTextAttributes = titleAttributes
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().tintColor = UIColor(netHex: AppColors.appOrange)

//            setupAppearanceForWindow()
            setupAppearanceForSettingsCell()
            setupAppearanceForAlertController()
            setupAppearanceForToolbar()
            setupAppearanceForBarButtonItem()
            
            return true
        }
        
        return false
    }
    
    private func setupAppearanceForAlertController() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self])
        view.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
    private func setupAppearanceForSettingsCell() {
        let view = UIView.appearance(whenContainedInInstancesOf: [HKSettingsTableViewCell.self])
        view.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
    private func setupAppearanceForWindow() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIWindow.self])
        view.tintColor = UIColor(netHex: AppColors.appOrange)
    }
    
    private func setupAppearanceForToolbar() {
        let view = UIView.appearance(whenContainedInInstancesOf: [UIToolbar.self])
        view.backgroundColor = UIColor(netHex: AppColors.appOrange)
    }
    
    private func setupAppearanceForBarButtonItem() {
        let attribute = [NSAttributedStringKey.foregroundColor: UIColor(netHex: AppColors.appOrange)]
        UIBarButtonItem.appearance().setBackButtonBackgroundImage(#imageLiteral(resourceName: "arrow_down_white"), for: .normal, barMetrics: .default)
        UIBarButtonItem.appearance().setTitleTextAttributes(attribute, for: .normal)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UIToolbar.self as UIAppearanceContainer.Type]).setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
    }
}

