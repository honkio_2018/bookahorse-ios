//
//  AppViewControllerFactory..swift
//  BookAHorse
//
//  Created by Mikhail Li on 24/04/2018.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import HonkioAppTemplate

class AppViewControllerFactory: HKBaseAppViewControllerFactory {
    
    public static let RiderHorsesList = AppController.Custom + AppController.Vc + 20
    
    override func getStoryboardIdentifierByViewControllerId(_ identifier: Int) -> String? {
        switch identifier {
        case AppController.OrderDetails:
            return "OrderDetailsViewController"
        case AppController.UserDetails:
            return "RiderDetailsViewController"
        case AppViewControllerFactory.RiderHorsesList:
            return "AssetsListViewController"
        case AppController.PushTable:
            return "PushesTableViewController"
        case AppController.Asset:
            return "HorseDetailsViewController"
        case AppController.MediaFiles:
            return "MediaPagesViewController"
        default:
            return super.getStoryboardIdentifierByViewControllerId(identifier)
        }
    }
    
    override func getViewControllerById(_ controllerId: Int, viewController: UIViewController) -> UIViewController? {
        switch controllerId {
        case AppController.Main:
            if HonkioApi.isConnected() {
                StoreData.save(toStore: true, forKey: IntroViewController.INTRO_DID_SHOWN)
            }
            
            let introDidShown = StoreData.load(fromStore: IntroViewController.INTRO_DID_SHOWN) as? Bool
            if introDidShown == nil || !introDidShown! {
                return getViewControllerByIdentifier("IntroNavigationViewController", viewController: viewController)
            }
            return super.getViewControllerById(controllerId, viewController: viewController)
        case AppController.Shop:
            return ShopViewController()
        default:
            break
        }
        return super.getViewControllerById(controllerId, viewController: viewController)
    }
    
    override func isGuestModeAvailable() -> Bool {
        return true
    }
    
}
