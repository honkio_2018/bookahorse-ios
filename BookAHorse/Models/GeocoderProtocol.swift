//
//  GeocoderProtocol.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 8/25/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

protocol GeocoderProtocol {
    
    func fromLocationName(_ locationName: String, callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void))
    func fromLocation(_ location: CLLocation, callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void))
    
    func cancel()
    
}
